
var date_settings = {
    format: 'yyyy/mm/dd',
    today: 'Сьогодні',
    clear: 'Очистити',
    close: 'Закрити',
    monthsFull: ['Січень', 'Лютий', 'Березень', 'Квітень', 'Травень', 'Червень', 'Липень', 'Серпень', 'Вересень', 'Жовтень', 'Листопад', 'Грудень'],
    monthsShort: ['Січ', 'Лют', 'Берез', 'Квіт', 'Трав', 'Черв', 'Лип', 'Серп', 'Верес', 'Жовт', 'Листоп', 'Груд'],
    weekdaysFull: ['Неділя', 'Понеділок', 'Вівторок', 'Середа', 'Четвер', "П'ятниця", 'Субота'],
    weekdaysShort: ['Нд', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
    showMonthsShort: undefined,
    showWeekdaysFull: undefined
}

$( document ).ready(function() {

    $('body')
        .off('click', 'form button')
        .on('click', 'form button', function (event) {
            event.preventDefault();
        });
    
    $('body')
        .off('click', '#logout')
        .on('click', '#logout', function (event) {
            
            var response = http_post('/logout');

            if (response.status == 200) {
                window.location.href = '/login';
            }

        });
    
    $('body')
        .off('keyup', 'input.required')
        .on('keyup', 'input.required', function (event) {
            var text = $(this).val();
            if (empty(text)) {
                $(this).closest( ".form-group" ).addClass('has-danger');
            } else {
                $(this).closest( ".form-group" ).removeClass('has-danger');
            }
        });

    $.fn.collect_input = function (options) {
        var settings = $.extend({
            stringify : false
        }, options);
        
        var json = {};
        $.each(this.serializeArray(), function() {
            if (json[this.name]) {
                if (!json[this.name].push)
                    json[this.name] = [json[this.name]];
                json[this.name].push(this.value || '');
            } else
                json[this.name] = this.value || '';
        });

        if(settings.stringify)
            return JSON.stringify(json);
        else
            return json;
    };

});

function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test( $email );
}

function verifyData (data) {

    var result = true;

    $.each(data , function (index, value){
        if(empty(value)) {
            $('[name="' + index + '"]').addClass('is-invalid');
            result = false;
        }
    });

    return result;

}

function insertFormData (form, data) {

    $.each(data, function(key, value) {

        var input = $('[name="' + key + '"]');

        if (input.prop('tagName') != undefined) {

            if (input.prop('tagName') == 'INPUT') {

                if (input.attr('type') == 'text') {

                    if (input.hasClass('pickadate')) {

                        var date = new Date(value);
                        input.pickadate('picker').set('select', date);

                    } else {
                        input.val(value);
                    }

                } else if (input.attr('type') == 'checkbox') {
                    input.prop('checked', value).trigger('change');
                }

            } else if (input.prop('tagName') == 'SELECT') {
                $('[name="' + key + '"]').val(value).trigger('change');
            } else if (input.prop('tagName') == 'TEXTAREA') {
                $('[name="' + key + '"]').val(value).text(value);
            }

        }

    });

    $.uniform.update('input[type="checkbox"]');

}

function empty (e) {

    if (e != null && typeof e == 'object') {
        if (Object.keys(e).length > 0) {
            return false;
        } else {
            return true;
        }
    }

    switch(e) {
        case "":
        case 0:
        case "0":
        case null:
        case false:
        case undefined:
            return true;
        default :
            return false;
    }
}

function http_get (url, data) {
    var result;

    $.ajax({
        type: 'get',
        async: false,
        url: url,
        data: data,
        dataType: 'json',
        statusCode: {
            200: function (response) {
                result = {
                    status: 200,
                    response: response
                };
            },
            // No Content
            204: function (response) {
                result = {
                    status: 409,
                    response: response
                };
            },
            403: function (response) {
                result = {
                    status: 403,
                    response: response
                };
            },
            404: function (response) {
                result = {
                    status: 404,
                    response: response
                };
            },
            405: function (response) {
                result = {
                    status: 405,
                    response: response
                };
            },
            406: function (response) {
                result = {
                    status: 406,
                    response: JSON.parse(response.responseText)
                };
            },
            409: function (response) {
                result = {
                    status: 409,
                    response: response
                };
            },
            418: function (response) {
                result = {
                    status: 418,
                    response: response
                };
            },
            // Internal server error
            500: function (response) {
                result = {
                    status: 500,
                    response: response
                };
            },
            // Bad Gateway
            502: function (response) {
                result = {
                    status: 502,
                    response: response
                };
            }
        }
    });

    return result;
}

var token = '';

function selectNewToken (update = true) {

    if (update == false && !empty(token)) {

        return token;

    }

    token = http_get('/refresh/csrf_token').response.token;

    return token;

}

function http_post (url, data) {

    var result;

    $.ajax({
        type: 'POST',
        async: false,
        contentType: 'application/json',
        headers: {
            'X-CSRF-TOKEN': selectNewToken()
        },
        url: url,
        dataType: 'json',
        data: JSON.stringify(data),
        statusCode: {
            // OK
            200: function (response) {
                result = {
                    status: 200,
                    response: response
                };
            },
            // No Content
            204: function (response) {
                result = {
                    status: 409,
                    response: response
                };
            },
            // Bad request
            400: function (response) {
                result = {
                    status: 400,
                    response: JSON.parse(response.responseText)
                };
            },
            // Forbidden
            403: function (response) {
                result = {
                    status: 403,
                    response: response
                };
            },
            // Not found
            404: function (response) {
                result = {
                    status: 404,
                    response: response
                };
            },
            405: function (response) {
                result = {
                    status: 405,
                    response: response
                };
            },
            // Not Acceptable
            406: function (response) {
                result = {
                    status: 406,
                    response: JSON.parse(response.responseText)
                };
            },
            // Conflict
            409: function (response) {
                result = {
                    status: 409,
                    response: JSON.parse(response.responseText)
                };
            },
            418: function (response) {
                result = {
                    status: 418,
                    response: response
                };
            },
            420: function (response) {
                result = {
                    status: 420,
                    response: response
                };
            },
            // Internal server error
            500: function (response) {
                result = {
                    status: 500,
                    response: response
                };
            },
            // Bad Gateway
            502: function (response) {
                result = {
                    status: 502,
                    response: response
                };
            }
        }
    });

    return result;
}