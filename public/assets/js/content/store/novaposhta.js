$( document ).ready(function() {

    $('.form-check-input-styled').uniform();

    novaposhtaAccountComplete();

    $('[name="search_account"]').autocomplete({
        minLength: 1,
        source: function( request, response ) {

            var complete = http_get('/integration/novaposhta/account/complete', {
                term: request.term
            }).response;

            response(complete);

        },
        select: function( event, ui ) {

            var account = ui.item;

            $('#accounts_table tbody').append('\
                <tr class="novaposhta_account" key="' + account.Key + '" sender_Ref="' + account.sender_Ref + '" contact_Ref="' + account.contact_Ref + '">\
                    <td>' + account.Key + '</td>\
                    <td>' + account.Description + '</td>\
                    <td>' + account.Phones + '</td>\
                    <td><input type="text" name="city" class="form-control" Ref="" placeholder="Місто"></td>\
                    <td><input type="text" name="warehouse" class="form-control" Ref="" placeholder="Відділення"></td>\
                    <td><input type="number" name="month_limit" class="form-control" value="0"></td>\
                    <td><input type="checkbox" name="active" class="form-check-input-styled" checked data-fouc></td>\
                </tr>\
            ');

            $('.form-check-input-styled').uniform();

            novaposhtaAccountComplete();

            $(this).val(''); return false; 
        }
    });

    $('body')
        .off('click', '#save_integration')
        .on('click', '#save_integration', function () {

            var data = {};

            // Select Statuses

            var statuses = [];

            $('#statuses_table tbody tr').each(function(key, value) {

                var status = {
                    StateId : $(value).attr('StateId'),
                    limit_debiting : $('[name="limit_debiting"]', value).is(":checked"),
                };

                statuses.push(status);
        
            });

            data.statuses = statuses;

            // Select Accounts

            var accounts = [];

            $('#accounts_table tbody tr').each(function(key, value) {

                var account = {
                    Key : $(value).attr('key'),
                    sender_Ref : $(value).attr('sender_Ref'),
                    contact_Ref : $(value).attr('contact_Ref'),
                    city : $('[name="city"]', value).val(),
                    city_Ref : $('[name="city"]', value).attr('Ref'),
                    warehouse : $('[name="warehouse"]', value).val(),
                    warehouse_Ref : $('[name="warehouse"]', value).attr('Ref'),
                    month_limit : $('[name="month_limit"]', value).val(),
                    active : $('[name="active"]', value).is(":checked"),
                };

                accounts.push(account);
        
            });

            data.accounts = accounts;

            var response = http_post('/integration/novaposhta/save', data);

            if (response.status == 200) {
    
                new PNotify( {
                    title: 'Виконано',
                    text: response.response.message, 
                    type: 'success'
                });
    
            } else if (response.status == 409) {
    
                new PNotify( {
                    title: 'Помилка',
                    text: response.response.message, 
                    type: 'error'
                });
    
            } else if (response.status == 406) {

                $.each( response.response.errors, function( key, value ) {
    
                    $('[name="' + value.input  + '"]').closest('.form-group').append('\
                        <label class="validation-invalid-label" for="default_select">Заповніть ' + value.message  + '</label>\
                    ');
    
                });
    
                new PNotify( {
                    title: 'Помилка',
                    text: response.response.message, 
                    type: 'error'
                });
    
            } else {
    
                new PNotify( {
                    title: 'Помилка',
                    text: 'Не визначена помилка', 
                    type: 'error'
                });
            }

        });

    $('body')
        .off('click', '#update_novaposhta_data')
        .on('click', '#update_novaposhta_data', function () {

            var response = http_get('/integration/novaposhta/import/data');

            if (response.status == 200) {

                areas_table.ajax.reload();

                cities_table.ajax.reload();

                warehouses_table.ajax.reload();
    
                new PNotify( {
                    title: 'Виконано',
                    text: response.response.message, 
                    type: 'success'
                });
    
            } else if (response.status == 409) {
    
                new PNotify( {
                    title: 'Помилка',
                    text: response.response.message, 
                    type: 'error'
                });
    
            } else if (response.status == 406) {

                console.log(response.response);

                $.each( response.response.errors, function( key, value ) {
    
                    $('[name="' + value.input  + '"]').closest('.form-group').append('\
                        <label class="validation-invalid-label" for="default_select">Заповніть ' + value.message  + '</label>\
                    ');
    
                });
    
                new PNotify( {
                    title: 'Помилка',
                    text: response.response.message, 
                    type: 'error'
                });
    
            } else {
    
                new PNotify( {
                    title: 'Помилка',
                    text: 'Не визначена помилка', 
                    type: 'error'
                });
            }

        });
    
});

function novaposhtaAccountComplete () {

    $('.novaposhta_account [name="city"]').autocomplete({
        minLength: 1,
        source: function( request, response ) {

            var complete = http_get('/integration/novaposhta/city/complete', {
                term: request.term,
                api_key: $(this.element).closest('.novaposhta_account').attr('Key')
            }).response;

            response(complete);

        },
        select: function( event, ui ) {
            $(this).attr('Ref', ui.item.Ref); 
        }
    });

    $('.novaposhta_account [name="warehouse"]').autocomplete({
        minLength: 1,
        source: function( request, response ) {

            var tr = $(this.element).closest('.novaposhta_account'),
                city_ref = $('[name="city"]', tr).attr('ref');

            if (empty(city_ref)) {

                new PNotify( {
                    title: 'Помилка',
                    text: 'Не можливо вибрати відділення без вказаного міста', 
                    type: 'error'
                });

                return false;

            }

            var complete = http_get('/integration/novaposhta/warehouse/complete', {
                term: request.term,
                api_key: $(this.element).closest('.novaposhta_account').attr('Key'),
                city_ref : city_ref
            }).response;

            response(complete);

        },
        select: function( event, ui ) {
            $(this).attr('Ref', ui.item.Ref); 
        }
    });

}