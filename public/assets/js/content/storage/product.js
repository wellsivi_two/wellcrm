$( document ).ready(function() {

    $('.select').select2({
        minimumResultsForSearch: Infinity
    });
    
    var validator = $('#product_form').validate({
        errorClass: 'validation-invalid-label',
        validClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        errorPlacement: function(error, element) {
            error.appendTo( element.parent().parent() );
        },
        validClass: 'validation-valid-label',
        rules : {
            article : { required : true },
            name : { required : true },
            cost : { required : true},
            price : { required : true },
        },
        messages : {
            article : { required : "Обов'язкове поле для заповнення" },
            name : { required : "Обов'язкове поле для заповнення" },
            cost : { required : "Обов'язкове поле для заповнення" },
            price : { required : "Обов'язкове поле для заповнення" }
        },
        submitHandler: function (form) {
            
            var product_id = $('#save_product').attr('product_id');

            var response = http_post('/product/' + product_id, $(form).collect_input());

            if (response.status == 200) {
                
                if (empty(product_id)) {
                    location.replace('/product/' + response.response.id);
                } else {
                    new PNotify( {
                        title: 'Виконано',
                        text: response.response.message, 
                        type: 'success'
                    });
                }

            } else if (response.status == 409) {

                new PNotify( {
                    title: 'Помилка',
                    text: response.response.message, 
                    type: 'error'
                });

            } else if (response.status == 406) {

                $.each( response.response.errors, function( key, value ) {
    
                    $('[name="' + value.input  + '"]').closest('.form-group').append('\
                        <label class="validation-invalid-label" for="default_select">Заповніть ' + value.message  + '</label>\
                    ');
    
                });
    
                new PNotify( {
                    title: 'Помилка',
                    text: response.response.message, 
                    type: 'error'
                });
    
            }  else {

                new PNotify( {
                    title: 'Помилка',
                    text: 'Не визначена помилка', 
                    type: 'error'
                });
            }
           
         }
    });
    
    $('#delete_product').click(function () {
        
        var product_id = $(this).attr('product_id')
        
        var response = http_post('/product/' + product_id + '/delete');
        
        if (response.status == 200) {
            
            location.replace('/products');
            
        } else {
            
            new PNotify( {
                title: 'Помилка',
                text: 'Не визначена помилка', 
                type: 'error'
            });
            
        }
        
    });

    if (product.id != 0) {

        $('[name="store_id"]').val(product.store_id).trigger('change');

    } else {


    }
});