$( document ).ready(function() {

    var users_table = $('#products_table').DataTable({
        autoWidth: false,
        responsive: true,
        stateSave: true,
        order: [[0, 'desc']],
        ajax: {
            url : "/products",
            type : "POST",
            async : false,
            headers : {
                'X-CSRF-TOKEN': selectNewToken()
            }
        },
        columns: [
            {"data" : "id"},
            {"data" : "article"},
            {
                'data': 'name',
                'render': function(data, type, full, meta) {
                    return '<a href="product/' + full.id + '">' + full.name + '</a>';
                }
            },
            {"data" : "price"},
            {"data" : "created_at"},
            {"data" : "updated_at"},
        ],
        columnDefs: [
            { "width": "5%", "targets": 0 },
            { "width": "10%", "targets": 1 },
            { "width": "30%", "targets": 2 },
            { "width": "15%", "targets": 3 },
            { "width": "15%", "targets": 4 },
            { "width": "15%", "targets": 5 }
        ],
        dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Пошук:</span> _INPUT_',
            lengthMenu: '<span>Показати:</span> _MENU_',
            paginate: { 'first': 'Перша', 'last': 'Остання', 'next': '&rarr;', 'previous': '&larr;' },
            info: 'Записи з _START_ до _END_ . Всього:  _TOTAL_ записів',
            infoEmpty: 'Записів 0',
            zeroRecords: 'Записи відсутні'
        },
        drawCallback: function () {}
    });

});