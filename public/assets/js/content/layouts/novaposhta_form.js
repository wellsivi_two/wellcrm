$( document ).ready(function() {

    $('.select').select2({
        minimumResultsForSearch: Infinity
    });

    $('input[type="checkbox"]').uniform();

    $('[name="phone"]').inputmask({"mask": "38 (999) 999-99-99"});

    $('.multiselect-select-all-filtering').multiselect({
        nonSelectedText:'Не вибрано',
        allSelectedText: 'Вибрано все',
        selectAllText: 'Вибрати все',
        nSelectedText: 'вибрано',
        numberDisplayed: 6,
        includeSelectAllOption: true,
        enableCaseInsensitiveFiltering: true,
    });

    checkRedeliveryCargo();

    $('body')
        .off('change', '[name="redelivery_cargo"]')
        .on('change', '[name="redelivery_cargo"]', function () {

            checkRedeliveryCargo();

        });

    $('body')
        .off('keyup', '[name="length"], [name="width"], [name="height"]')
        .on('keyup', '[name="length"], [name="width"], [name="height"]', function () {

            var position_block = $(this).closest('#novaposhta_card');

            var length = (empty($('[name="length"]', position_block).val())) ? 1 : parseFloat($('[name="length"]', position_block).val()),
                width = (empty($('[name="width"]', position_block).val())) ? 1 : parseFloat($('[name="width"]', position_block).val()),
                height = (empty($('[name="height"]', position_block).val())) ? 1 : parseFloat($('[name="height"]', position_block).val()),
                volume = length * width * height,
                volumetric_weight = volume / 4000;

            $('[name="volumetric_weight"]', position_block).val(volumetric_weight);

        });

    $('#novaposhta_form [name="area"]').autocomplete({
        minLength: 1,
        source: function( request, response ) {

            var complete = http_get('/integration/novaposhta/area/complete', {
                term: request.term
            }).response;

            response(complete);

        },
        select: function( event, ui ) {
            $('#novaposhta_form [name="area"]').attr('Ref', ui.item.Ref); 
        }
    });

    $('#novaposhta_form [name="city"]').autocomplete({
        minLength: 1,
        source: function( request, response ) {

            var area_ref = $('[name="area"]').attr('ref');

            if (empty(area_ref)) {

                new PNotify( {
                    title: 'Помилка',
                    text: 'Не можливо вибрати місто без вказаної області', 
                    type: 'error'
                });

                return false;

            }

            var complete = http_get('/integration/novaposhta/city/complete', {
                term: request.term,
                area_ref : area_ref
            }).response;

            response(complete);

        },
        select: function( event, ui ) {
            $('#novaposhta_form [name="city"]').attr('Ref', ui.item.Ref); 
        }
    });

    $('#novaposhta_form [name="warehouse"]').autocomplete({
        minLength: 1,
        source: function( request, response ) {

            var city_ref = $('[name="city"]').attr('ref');

            if (empty(city_ref)) {

                new PNotify( {
                    title: 'Помилка',
                    text: 'Не можливо вибрати відділення без вказаного міста', 
                    type: 'error'
                });

                return false;

            }

            var complete = http_get('/integration/novaposhta/warehouse/complete', {
                term: request.term,
                city_ref : city_ref
            }).response;

            response(complete);

        },
        select: function( event, ui ) {
            $('#novaposhta_form [name="warehouse"]').val(ui.item.label).attr('Ref', ui.item.Ref); 
        }
    });

    $('#novaposhta_form [name="delivery_description"]').autocomplete({
        minLength: 1,
        source: function( request, response ) {

            var complete = http_get('/integration/novaposhta/delivery_description/complete', {
                term: request.term
            }).response;

            response(complete);

        },
        select: function( event, ui ) {
            $('#novaposhta_form [name="delivery_description"]').val(ui.item.label).attr('Ref', ui.item.Ref); 
        }
    });

    $('body')
        .off('click', '#novaposhta_card #create_ttn')
        .on('click', '#novaposhta_card #create_ttn', function () {

            var id = $(this).attr('novaposhta_id');

            var response = http_post('/integration/novaposhta/ttn/create', {
                'id' : id
            });
        
            if (response.status == 200) {

                renderDeliveryForm();

                new PNotify( {
                    title: 'Виконано',
                    text: response.response.message, 
                    type: 'success'
                });
                
            } else if (response.status == 409) {

                new PNotify( {
                    title: 'Помилка',
                    text: response.response.message, 
                    type: 'error'
                });
    
            } else if (response.status == 406) {
    
                $.each( response.response.errors, function( key, value ) {
    
                    $('[name="' + value.input  + '"]').closest('.form-group').append('\
                        <label class="validation-invalid-label" for="default_select">Заповніть ' + value.message  + '</label>\
                    ');
    
                });
    
                new PNotify( {
                    title: 'Помилка',
                    text: response.response.message, 
                    type: 'error'
                });
    
            } else {
    
                new PNotify( {
                    title: 'Помилка',
                    text: 'Не визначена помилка', 
                    type: 'error'
                });
            }

        });

    $('body')
        .off('click', '#novaposhta_card #delete_ttn')
        .on('click', '#novaposhta_card #delete_ttn', function () {

            var id = $(this).attr('novaposhta_id');

            var response = http_post('/integration/novaposhta/ttn/delete', {
                'id' : id
            });
        
            if (response.status == 200) {

                renderDeliveryForm();

                new PNotify( {
                    title: 'Виконано',
                    text: response.response.message, 
                    type: 'success'
                });
                
            } else if (response.status == 409) {

                new PNotify( {
                    title: 'Помилка',
                    text: response.response.message, 
                    type: 'error'
                });
    
            } else if (response.status == 406) {
    
                $.each( response.response.errors, function( key, value ) {
    
                    $('[name="' + value.input  + '"]').closest('.form-group').append('\
                        <label class="validation-invalid-label" for="default_select">Заповніть ' + value.message  + '</label>\
                    ');
    
                });
    
                new PNotify( {
                    title: 'Помилка',
                    text: response.response.message, 
                    type: 'error'
                });
    
            } else {
    
                new PNotify( {
                    title: 'Помилка',
                    text: 'Не визначена помилка', 
                    type: 'error'
                });
            }

        });

});

function checkRedeliveryCargo () {

    var redelivery_cargo = $('#novaposhta_card [name="redelivery_cargo"]').val(),
        redelivery_additional_string = $('#novaposhta_card [name="redelivery_additional_string"]'),
        redelivery_payer = $('#novaposhta_card [name="redelivery_payer"]');

    if (empty(redelivery_cargo)) {
        redelivery_additional_string.attr('disabled', true);
        redelivery_payer.attr('disabled', true);
    } else {
        redelivery_additional_string.attr('disabled', false);
        redelivery_payer.attr('disabled', false);
    }

}