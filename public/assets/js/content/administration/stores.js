$( document ).ready(function() {

    var roles_table = $('#roles_table').DataTable({
        autoWidth: false,
        responsive: true,
        stateSave: true,
        order: [[0, 'asc']],
        ajax: {
            url : "/stores",
            type : "POST",
            async : false,
            headers : {
                'X-CSRF-TOKEN': selectNewToken()
            }
        },
        columns: [
            {"data" : "id"},
            {
                'data': 'name',
                'render': function(data, type, full, meta) {
                    return '<a href="store/' + full.id + '">' + data + '</a>';
                }
            }
        ],
        columnDefs: [
            { "width": "5%", "targets": 0 },
            { "width": "20%", "targets": 1 }
        ],
        dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Пошук:</span> _INPUT_',
            lengthMenu: '<span>Показати:</span> _MENU_',
            paginate: { 'first': 'Перша', 'last': 'Остання', 'next': '&rarr;', 'previous': '&larr;' },
            info: 'Записи з _START_ до _END_ . Всього:  _TOTAL_ записів',
            infoEmpty: 'Записів 0',
            zeroRecords: 'Записи відсутні'
        },
        drawCallback: function () {}
    });

});