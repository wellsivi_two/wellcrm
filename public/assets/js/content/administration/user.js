$( document ).ready(function() {

    $('[name="phone"]').inputmask({"mask": "38 (999) 999-99-99"});

    $('.multiselect-select-all-filtering').multiselect({
        nonSelectedText:'Не вибрано',
        allSelectedText: 'Вибрано все',
        selectAllText: 'Вибрати все',
        nSelectedText: 'вибрано',
        numberDisplayed: 6,
        includeSelectAllOption: true,
        enableCaseInsensitiveFiltering: true,
    });
    
    var validator = $('#user_form').validate({
        errorClass: 'validation-invalid-label',
        validClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        errorPlacement: function(error, element) {
            error.appendTo( element.parent().parent() );
        },
        validClass: 'validation-valid-label',
        rules : {
            name : { required : true },
            surname : { required : true },
            email : { required : true, email : true},
            phone : { required : true },
            roles : { required : true }
        },
        messages : {
            name : { required : "Ім'я обов'язкове для заповнення" },
            surname : { required : "Прізвище обов'язкове для заповнення" },
            email : { required : "Email обов'язковий для заповнення" },
            phone : { required : "Номер телефону обов'язковий для заповнення" },
            roles : { required : "Ролі обов'язкові для заповлення" },
        },
        submitHandler: function (form) {
            
            var user_id = $('#save_user').attr('user_id');

            var response = http_post('/user/' + user_id, $(form).collect_input());

            if (response.status == 200) {
                
                if (empty(user_id)) {
                    location.replace('/user/' + response.response.id);
                } else {
                    new PNotify( {
                        title: 'Виконано',
                        text: response.response.message, 
                        type: 'success'
                    });
                }

            } else if (response.status == 409) {

                new PNotify( {
                    title: 'Помилка',
                    text: response.response.message, 
                    type: 'error'
                });

            } else {

                new PNotify( {
                    title: 'Помилка',
                    text: 'Не визначена помилка', 
                    type: 'error'
                });
            }
           
         }
    });
    
    $('#delete_user').click(function () {
        
        var user_id = $(this).attr('user_id')
        
        var response = http_post('/user/' + user_id + '/delete');
        
        if (response.status == 200) {
            
            location.replace('/users');
            
        } else {
            
            new PNotify( {
                title: 'Помилка',
                text: 'Не визначена помилка', 
                type: 'error'
            });
            
        }
        
    });
});