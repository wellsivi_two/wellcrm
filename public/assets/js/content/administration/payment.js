$( document ).ready(function() {
    
    $('#save_payment').click(function () {

        var data = $('#payment_form').collect_input();

        var response = http_post('/payment/' + payment.id, data);

        $('.form-group .validation-invalid-label').remove();

        if (response.status == 200) {
            
            if (empty(payment.id)) {

                location.replace('/payment/' + response.response.id);

            } else {

                new PNotify( {
                    title: 'Виконано',
                    text: response.response.message, 
                    type: 'success'
                });

            }

        } else if (response.status == 409) {

            new PNotify( {
                title: 'Помилка',
                text: response.response.message, 
                type: 'error'
            });

        } else if (response.status == 406) {

            $.each( response.response.errors, function( key, value ) {

                $('[name="' + value.input  + '"]').closest('.form-group').append('\
                    <label class="validation-invalid-label" for="default_select">Заповніть ' + value.message  + '</label>\
                ');

            });

            new PNotify( {
                title: 'Помилка',
                text: response.response.message, 
                type: 'error'
            });

        } else {

            new PNotify( {
                title: 'Помилка',
                text: 'Не визначена помилка', 
                type: 'error'
            });
        }

    });

    $('#delete_payment').click(function () {
        
        var response = http_post('/payment/' + payment.id + '/delete');
        
        if (response.status == 200) {
            
            location.replace('/payments');
            
        } else {
            
            new PNotify( {
                title: 'Помилка',
                text: 'Не визначена помилка', 
                type: 'error'
            });
            
        }
        
    });

    if (delivery.id != 0) {


    } else {
        
        $('#delete_payment').remove();

    }
    
});