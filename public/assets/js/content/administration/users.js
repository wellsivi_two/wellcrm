$( document ).ready(function() {

    var users_table = $('#users_table').DataTable({
        autoWidth: false,
        responsive: true,
        stateSave: true,
        order: [[3, 'desc']],
        ajax: {
            url : "/users",
            type : "POST",
            async : false,
            headers : {
                'X-CSRF-TOKEN': selectNewToken()
            }
        },
        columns: [
            {"data" : "id"},
            {
                'data': 'name',
                'render': function(data, type, full, meta) {
                    return '<a href="user/' + full.id + '">' + full.name + ' ' + full.surname + '</a>';
                }
            },
            {"data" : "roles_string"},
            {"data" : "created_at"},
            {"data" : "updated_at"},
        ],
        columnDefs: [
            { "width": "5%", "targets": 0 },
            { "width": "20%", "targets": 1 },
            { "width": "35%", "targets": 2 },
            { "width": "15%", "targets": 3 },
            { "width": "15%", "targets": 4 }
        ],
        dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Пошук:</span> _INPUT_',
            lengthMenu: '<span>Показати:</span> _MENU_',
            paginate: { 'first': 'Перша', 'last': 'Остання', 'next': '&rarr;', 'previous': '&larr;' },
            info: 'Записи з _START_ до _END_ . Всього:  _TOTAL_ записів',
            infoEmpty: 'Записів 0',
            zeroRecords: 'Записи відсутні'
        },
        drawCallback: function () {}
    });

});