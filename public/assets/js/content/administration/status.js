$( document ).ready(function() {

    $('.select').select2({
        placeholder: "Не выбрано"
    });
    
    $('#save_status').click(function () {

        var data = $('#status_form').collect_input();

        var response = http_post('/status/' + status_data.id, data);

        $('.form-group .validation-invalid-label').remove();

        if (response.status == 200) {
            
            if (empty(status_data.id)) {

                location.replace('/status/' + response.response.id);

            } else {

                new PNotify( {
                    title: 'Виконано',
                    text: response.response.message, 
                    type: 'success'
                });

            }

        } else if (response.status == 409) {

            new PNotify( {
                title: 'Помилка',
                text: response.response.message, 
                type: 'error'
            });

        } else if (response.status == 406) {

            $.each( response.response.errors, function( key, value ) {

                $('[name="' + value.input  + '"]').closest('.form-group').append('\
                    <label class="validation-invalid-label" for="default_select">Заповніть ' + value.message  + '</label>\
                ');

            });

            new PNotify( {
                title: 'Помилка',
                text: response.response.message, 
                type: 'error'
            });

        } else {

            new PNotify( {
                title: 'Помилка',
                text: 'Не визначена помилка', 
                type: 'error'
            });
        }

    });

    $('#delete_status').click(function () {
        
        var response = http_post('/status/' + status_data.id + '/delete');
        
        if (response.status == 200) {
            
            location.replace('/statuses');
            
        } else {
            
            new PNotify( {
                title: 'Помилка',
                text: 'Не визначена помилка', 
                type: 'error'
            });
            
        }
        
    });

    if (status_data.id != 0) {

        $('[name="essence_id"]').val(status_data.essence_id).trigger('change');


    } else {
        
        $('#delete_status').remove();

    }
    
});