$( document ).ready(function() {

    $('.select').select2({
        minimumResultsForSearch: Infinity
    });
    
    $('#save_delivery').click(function () {

        var data = $('#delivery_form').collect_input();

        var response = http_post('/delivery/' + delivery.id, data);

        $('.form-group .validation-invalid-label').remove();

        if (response.status == 200) {
            
            if (empty(delivery.id)) {

                location.replace('/delivery/' + response.response.id);

            } else {

                new PNotify( {
                    title: 'Виконано',
                    text: response.response.message, 
                    type: 'success'
                });

            }

        } else if (response.status == 409) {

            new PNotify( {
                title: 'Помилка',
                text: response.response.message, 
                type: 'error'
            });

        } else if (response.status == 406) {

            $.each( response.response.errors, function( key, value ) {

                $('[name="' + value.input  + '"]').closest('.form-group').append('\
                    <label class="validation-invalid-label" for="default_select">Заповніть ' + value.message  + '</label>\
                ');

            });

            new PNotify( {
                title: 'Помилка',
                text: response.response.message, 
                type: 'error'
            });

        } else {

            new PNotify( {
                title: 'Помилка',
                text: 'Не визначена помилка', 
                type: 'error'
            });
        }

    });

    $('#delete_delivery').click(function () {
        
        var response = http_post('/delivery/' + delivery.id + '/delete');
        
        if (response.status == 200) {
            
            location.replace('/deliveries');
            
        } else {
            
            new PNotify( {
                title: 'Помилка',
                text: 'Не визначена помилка', 
                type: 'error'
            });
            
        }
        
    });

    if (delivery.id != 0) {


    } else {
        
        $('#delete_delivery').remove();

    }
    
});