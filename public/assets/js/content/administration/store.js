$( document ).ready(function() {

    $('.multiselect-select-all-filtering').multiselect({
        nonSelectedText:'Не вибрано',
        allSelectedText: 'Вибрано все',
        selectAllText: 'Вибрати все',
        nSelectedText: 'вибрано',
        numberDisplayed: 6,
        includeSelectAllOption: true,
        enableCaseInsensitiveFiltering: true,
    });
    
    $('#save_store').click(function () {

        var data = $('#store_form').collect_input();

        if (store.id > 0 ) {

            data.orders_status_matrix = $('#orders_statuses_table .status_matrix').collect_input();
            data.start_order_status_id = $('[name="start_order_status_id"]:checked').val();

        }

        var response = http_post('/store/' + store.id, data);

        $('.form-group .validation-invalid-label').remove();

        if (response.status == 200) {
            
            if (empty(store.id)) {

                location.replace('/store/' + response.response.id);

            } else {

                renderOrdersStatusMatrix();

                new PNotify( {
                    title: 'Виконано',
                    text: response.response.message, 
                    type: 'success'
                });

            }

        } else if (response.status == 409) {

            new PNotify( {
                title: 'Помилка',
                text: response.response.message, 
                type: 'error'
            });

        } else if (response.status == 406) {

            $.each( response.response.errors, function( key, value ) {

                $('[name="' + value.input  + '"]').closest('.form-group').append('\
                    <label class="validation-invalid-label" for="default_select">Заповніть ' + value.message  + '</label>\
                ');

            });

            new PNotify( {
                title: 'Помилка',
                text: response.response.message, 
                type: 'error'
            });

        } else {

            new PNotify( {
                title: 'Помилка',
                text: 'Не визначена помилка', 
                type: 'error'
            });
        }

    });

    $('#delete_store').click(function () {
        
        var response = http_post('/store/' + store.id + '/delete');
        
        if (response.status == 200) {
            
            location.replace('/stores');
            
        } else {
            
            new PNotify( {
                title: 'Помилка',
                text: 'Не визначена помилка', 
                type: 'error'
            });
            
        }
        
    });

    if (store.id != 0) {




    } else {
        
        $('#delete_store').remove();

    }

    renderMultiselect();

    renderOrdersStatusMatrix();
    
});

function renderOrdersStatusMatrix () {

    $('#orders_statuses_matrix').html('');

    if (store.id > 0) {

        var response = http_post('/store/' + store.id + '/orders/status/matrix').response;

        $('#orders_statuses_matrix').html(response.orders_statuses_matrix).show();

        renderMultiselect();

        $('.form-input-styled').uniform();

    } else {

        $('#orders_statuses_matrix').hide();

    }

}

function renderMultiselect () {

    $('.multiselect-select-all-filtering').multiselect({
        nonSelectedText:'Не вибрано',
        allSelectedText: 'Вибрано все',
        selectAllText: 'Вибрати все',
        nSelectedText: 'вибрано',
        numberDisplayed: 6,
        includeSelectAllOption: true,
        enableCaseInsensitiveFiltering: true,
    });

}