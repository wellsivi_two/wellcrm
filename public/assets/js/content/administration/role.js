$( document ).ready(function() {
    
    $('.select-search').select2({
        placeholder: "Не выбрано"
    });
    
    $('.form-check-input-styled').uniform();
    
    $('#user_search').autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "/user/complete",
                type: "GET",
                headers : {
                    'X-CSRF-TOKEN': selectNewToken(true)
                },
                dataType: "json",
                data: {
                    term: request.term
                },
                success: function( data ) {
                    
                    response( $.map( data, function( item ) {
                        return {
                            value: item.id,
                            label: item.name + ' ' + item.surname,
                            id: item.id,
                        }
                    }));
                }
            });
        },
        minLength: 1,
        select: function( event, ui ) {

            var is_member = false;

            $('#users tbody tr').each(function( index, value ) {

                if ($(value).attr('id') == ui.item.id) {

                    is_member = true;

                }

            });

            if (is_member == false) {

                $('#users tbody').append('\
                   <tr id="' + ui.item.value + '">\
                        <td>' + ui.item.value + '</td>\
                        <td>' + ui.item.label + '</td>\
                        <td>\
                            <div class="list-icons">\
                                <button class="btn list-icons-item text-danger-600 delete_user"><i class="icon-trash"></i></button>\
                            </div>\
                        </td>\
                    </tr>\
                ');
                
                var newOption = new Option(ui.item.label, ui.item.value, false, false);
                $('[name="responsible"]').append(newOption).trigger('change');

            } else {

                new PNotify( {
                    title: 'Помилка',
                    text: 'Даний користувач вже являється учасником даної ролі', 
                    type: 'error'
                });

            }

            return false;

        }
    });
    
    var validator = $('#role_form').validate({
        errorClass: 'validation-invalid-label',
        validClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        errorPlacement: function(error, element) {
            error.appendTo( element.parent().parent() );
        },
        validClass: 'validation-valid-label',
        rules : {
            name : { required : true },
            responsible : { required : true }
        },
        messages : {
            name : { required : "Ім'я обов'язкове для заповнення" },
            responsible : { required : "Відповідальний обов'язкове для заповнення" }
        },
        submitHandler: function (form) {
            
            var data = $(form).collect_input();
            
            data.users = {};
            
            $('#users tbody tr').each(function( index, value ) {
                
                var user_id = $(value).attr('id');
                
                data.users[user_id] = user_id;

            });
            
            data.permissions =  $('#role_permissions :input').collect_input();
            
            var role_id = $('#save_role').attr('role_id');

            var response = http_post('/role/' + role_id, data);

            if (response.status == 200) {
                
                if (empty(role_id)) {
                    location.replace('/role/' + response.response.id);
                } else {
                    new PNotify( {
                        title: 'Виконано',
                        text: response.response.message, 
                        type: 'success'
                    });
                }

            } else if (response.status == 409) {

                new PNotify( {
                    title: 'Помилка',
                    text: response.response.message, 
                    type: 'error'
                });

            } else {

                new PNotify( {
                    title: 'Помилка',
                    text: 'Не визначена помилка', 
                    type: 'error'
                });
            }
           
         }
    });
    
    $('#delete_role').click(function () {
        
        var role_id = $(this).attr('role_id')
        
        var response = http_post('/role/' + role_id + '/delete');
        
        if (response.status == 200) {
            
            location.replace('/roles');
            
        } else {
            
            new PNotify( {
                title: 'Помилка',
                text: 'Не визначена помилка', 
                type: 'error'
            });
            
        }
        
    });
    
    $('.delete_user').click(function () {
        
        $(this).closest('tr').remove();
        
    });
    
    
});