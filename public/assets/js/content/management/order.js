$( document ).ready(function() {
    
    $('input[type="checkbox"]').uniform();
    
    $('[name="phone"], [name="new_phone"]').inputmask({"mask": "38 (999) 999-99-99"});
    
    $('.pickadate').pickadate(date_settings);

    $('.select').select2({
        minimumResultsForSearch: Infinity
    });

    $('[name="new_phone"]').autocomplete({
        minLength: 1,
        source: function( request, response ) {

            var complete = http_get('/client/phone/complete', {
                term: request.term.replace(/[^\d]/g, '')
            }).response;

            response(complete);

        },
        select: function( event, ui ) {

            insertFormData('#client_form', ui.item);

            $('#phones_block').empty();

            $.each(ui.item.phones, function(key, value) {

                $('#phones_block').append('\
                    <div class="input-group mb-3">\
                        <span class="input-group-prepend">\
                            <button class="btn btn-light btn-icon legitRipple delete_phone_button"><i class="icon-cross2"></i></button>\
                        </span>\
                        <input type="text" name="phone" class="form-control" readonly placeholder="Телефон" value="' + value.phone  + '">\
                    </div>\
                ');

                $('[name="phone"]').inputmask({"mask": "38 (999) 999-99-99"});

            });

            $('#emails_block').empty();

            $.each(ui.item.emails, function(key, value) {

                $('#emails_block').append('\
                    <div class="input-group mb-3">\
                        <span class="input-group-prepend">\
                            <button class="btn btn-light btn-icon legitRipple delete_email_button" type="button"><i class="icon-cross2"></i></button>\
                        </span>\
                        <input type="text" name="email" class="form-control" readonly placeholder="Телефон" value="' + value.email  + '">\
                    </div>\
                ');

            });

            order.client.id = ui.item.id;

            $(this).val('');
        }
    });
    
    $('#new_phone_button').click(function () {
        
        var phone = $('[name="new_phone"]').val().replace(/[^\d]/g, '');
        
        if (phone.length == 12) {

            $('#phones_block').append('\
                <div class="input-group mb-3">\
                    <span class="input-group-prepend">\
                        <button class="btn btn-light btn-icon legitRipple delete_phone_button"><i class="icon-cross2"></i></button>\
                    </span>\
                    <input type="text" name="phone" class="form-control" readonly placeholder="Телефон" value="' + phone  + '">\
                </div>\
            ');

            $('[name="phone"]').inputmask({"mask": "38 (999) 999-99-99"});

            $('[name="new_phone"]').val('');
            
        } else {

            new PNotify( {
                title: 'Помилка',
                text: 'Введіть коректний номер телефону', 
                type: 'error'
            });

        }
        
    });

    $('body')
        .off('click', '.delete_phone_button')
        .on('click', '.delete_phone_button', function () {

            $(this).closest('.input-group').remove();

        });

    $('[name="new_email"]').autocomplete({
        minLength: 1,
        source: function( request, response ) {

            var complete = http_get('/client/email/complete', {
                term: request.term
            }).response;

            response(complete);

        },
        select: function( event, ui ) {

            insertFormData('#client_form', ui.item);

            $('#phones_block').empty();

            $.each(ui.item.phones, function(key, value) {

                $('#phones_block').append('\
                    <div class="input-group mb-3">\
                        <span class="input-group-prepend">\
                            <button class="btn btn-light btn-icon legitRipple delete_phone_button"><i class="icon-cross2"></i></button>\
                        </span>\
                        <input type="text" name="phone" class="form-control" readonly placeholder="Телефон" value="' + value.phone  + '">\
                    </div>\
                ');

                $('[name="phone"]').inputmask({"mask": "38 (999) 999-99-99"});

            });

            $('#emails_block').empty();

            $.each(ui.item.emails, function(key, value) {

                $('#emails_block').append('\
                    <div class="input-group mb-3">\
                        <span class="input-group-prepend">\
                            <button class="btn btn-light btn-icon legitRipple delete_email_button" type="button"><i class="icon-cross2"></i></button>\
                        </span>\
                        <input type="text" name="email" class="form-control" readonly placeholder="Телефон" value="' + value.email  + '">\
                    </div>\
                ');

            });

            order.client.id = ui.item.id;

            $(this).val('');
        }
    });

    $('#new_email_button').click(function () {
        
        var email = $('[name="new_email"]').val();

        if (validateEmail(email)) {

            $('#emails_block').append('\
                <div class="input-group mb-3">\
                    <span class="input-group-prepend">\
                        <button class="btn btn-light btn-icon legitRipple delete_email_button" type="button"><i class="icon-cross2"></i></button>\
                    </span>\
                    <input type="text" name="email" class="form-control" readonly placeholder="Телефон" value="' + email  + '">\
                </div>\
            ');

            $('[name="new_email"]').val('');

        } else {

            new PNotify( {
                title: 'Помилка',
                text: 'Введіть коректний email', 
                type: 'error'
            });

        }
        
    });

    $('body')
        .off('click', '.delete_email_button')
        .on('click', '.delete_email_button', function () {

            $(this).closest('.input-group').remove();

        });

    $('[name="search_product"]').autocomplete({
        appendTo: "#modal_add_product",
        minLength: 1,
        source: function( request, response ) {

            var complete = http_get('/product/complete', {
                term: request.term,
                store_id: order.store_id
            }).response;

            response(complete);

        },
        select: function( event, ui ) {

            $('body')
                .off('click', '#add_product_button')
                .on('click', '#add_product_button', function () {

                    createOrderProduct(ui.item);

                    $('#modal_add_product').modal('hide');

                    $('[name="search_product"]').val('');

                });
            
        }
    });

    $('body')
        .off('click', '.delete_product_button')
        .on('click', '.delete_product_button', function () {

            $(this).closest('tr').remove();

            countTotalOrder();

        });

    $('body')
        .off('change', '[name="delivery_id"]')
        .on('change', '[name="delivery_id"]', function () {

            renderDeliveryForm();

        });
    
    $('[name="store_id"]').change(function () {

        var store_id = $(this).val();
        order.store_id = store_id;

        if (store_id == 0) {

            order.store_id = 0;

            $('.store_info select').prop('disabled', true).empty();

            var newOption = new Option('Не вибрано', 0, false, false);
            $('.store_info select').append(newOption);

            $('#product_card').hide();

            return false;

        }

        // Statuses

        changeStatusesOptions(store_id);

        // Deliveries

        $('[name="delivery_id"]').empty();

        $.each(stores[store_id].deliveries, function(key, value) {

            var newOption = new Option(value.name, value.id, false, false);
            $('[name="delivery_id"]').append(newOption);

        });

        $('[name="delivery_id"]').prop('disabled', false);

        // Payments

        $('[name="payment_id"]').empty();

        $.each(stores[store_id].payments, function(key, value) {

            var newOption = new Option(value.name, value.id, false, false);
            $('[name="payment_id"]').append(newOption);

        });

        $('[name="payment_id"]').prop('disabled', false);
        
        // Show Product Vard

        $('#product_card').show();
        
    });

    $('#save_order').click(function () {

        var data = $('#order_form').collect_input();
        data.client_id = order.client.id;
        data.discount = $('#product_table tfoot [name="discount"]').val();
        data.total = $('#product_table tfoot [name="total"]').val();

        if (empty(data.store_id)) {
            data.store_id = order.store_id;
        }


        data.client = $('#client_form').collect_input();
        data.products = [];

        if ($('#novaposhta_card').length === 1) {

            var form = $('#novaposhta_form');

            data.novaposhta = $(form).collect_input();

            data.novaposhta.delivery_description_Ref = $('[name="delivery_description"]', form).attr('Ref');
            data.novaposhta.area_Ref = $('[name="area"]', form).attr('Ref');
            data.novaposhta.city_Ref = $('[name="city"]', form).attr('Ref');
            data.novaposhta.warehouse_Ref = $('[name="warehouse"]', form).attr('Ref');

        } else {

            data.delivery_integration_id = 0;

        }

        $('#product_table tbody tr').each(function( index, value ) {
            
            data.products.push({
                id : parseInt($(value).attr('id')),
                price : $(value).find('[name="price"]').val(),
                discount : $(value).find('[name="discount"]').val(),
                amount : $(value).find('[name="amount"]').val(),
                sum : $(value).find('[name="sum"]').val(),
            });

        });

        var response = http_post('/order/' + order.id, data);

        $('.form-group .validation-invalid-label').remove();

        if (response.status == 200) {
            
            if (empty(order.id)) {

                location.replace('/order/' + response.response.id);

            } else {

                changeStatusesOptions(order.store_id);

                new PNotify( {
                    title: 'Виконано',
                    text: response.response.message, 
                    type: 'success'
                });

            }

        } else if (response.status == 409) {

            new PNotify( {
                title: 'Помилка',
                text: response.response.message, 
                type: 'error'
            });

        } else if (response.status == 406) {

            $.each( response.response.errors, function( key, value ) {

                $('[name="' + value.input  + '"]').closest('.form-group').append('\
                    <label class="validation-invalid-label" for="default_select">Заповніть ' + value.message  + '</label>\
                ');

            });

            new PNotify( {
                title: 'Помилка',
                text: response.response.message, 
                type: 'error'
            });

        } else {

            new PNotify( {
                title: 'Помилка',
                text: 'Не визначена помилка', 
                type: 'error'
            });
        }

    });

    $('#delete_order').click(function () {
        
        var response = http_post('/order/' + order.id + '/delete');
        
        if (response.status == 200) {
            
            location.replace('/orders');
            
        } else {
            
            new PNotify( {
                title: 'Помилка',
                text: 'Не визначена помилка', 
                type: 'error'
            });
            
        }
        
    });

    $('body')
        .off('click', '#clear_client_form')
        .on('click', '#clear_client_form', function () {

            $('#client_form').find('input').val('');

            $('#client_form').find('select').val(0).trigger('change');

            $('#client_form input[type="checkbox"]').prop('checked', false);
            $.uniform.update('#client_form input[type="checkbox"]');

            $('#emails_block').empty();
            $('#phones_block').empty();

            order.client.id = 0;

        });

    $('body')
        .off('keyup', '#product_table tbody [name="discount"], #product_table tbody [name="amount"]')
        .on('keyup', '#product_table tbody [name="discount"], #product_table tbody [name="amount"]', function () {

            var product_block = $(this).closest('tr');

            var price = parseFloat(product_block.find('[name="price"]').val()),
                amount = parseInt(product_block.find('[name="amount"]').val()),
                discount = parseFloat(product_block.find('[name="discount"]').val());

            var sum = (price - discount) * amount;

            product_block.find('[name="sum"]').val(sum);

            countTotalOrder();

        });

    if (order.id != 0) {

        $('[name="sex"]').val(order.client.sex).trigger('change');

        $('[name="store_id"]').prop('disabled', true);

        insertFormData('#order_form', order);

    } else {

        $('.store_info select').prop('disabled', true);

        $('#product_card').hide();

        $('#order_form [name="discount"]').val(0);
        
        $('#delete_order').remove();

    }

    countTotalOrder();
    
});

function changeStatusesOptions (store_id = 1) {

    var response = http_post('/order/' + order.id + '/change/status', {
        store_id : store_id
    }).response;

    $('[name="status_id"]').empty();

    $.each(response.statuses, function(key, value) {

        var newOption = new Option(value.name, value.id, false, false);
        $('[name="status_id"]').append(newOption);

    });

    $('[name="status_id"]').prop('disabled', false);

}

function renderDeliveryForm () {

    var delivery_id = $('[name="delivery_id"]').val();

    if (delivery_id > 0) {

        var delivery = http_post('/order/' + order.id + '/change/delivery', {
            delivery_id : delivery_id
        }).response;

        if (delivery.integration_id > 0) {

            $('#delivery_block').html(delivery.integration.form);

        }

    }

}

function createOrderProduct (data) {

    var in_order = false;

    $('#product_table tbody tr').each(function( index, value ) {

        if ($(value).attr('id') == data.id) {

            in_order = true;

        }

    });

    if (in_order == false) {

        $('#product_table tbody').append('\
            <tr class="product" id="' + data.id + '">\
                <td><button type="button" class="btn btn-light btn-icon mr-2 delete_product_button"><i class="icon-cross2"></i></button></td>\
                <td>' + data.id + '</td>\
                <td>' + data.name + '</td>\
                <td><input type="number" readonly step="0.01" name="price" class="form-control" value="' + data.price + '"></td>\
                <td><input type="number" readonly name="discount" class="form-control" value="' + data.discount + '"></td>\
                <td><input type="number" name="amount" class="form-control" value="1"></td>\
                <td><input type="number" readonly step="0.01" name="sum" class="form-control" value="' + data.price + '"></td>\
            </tr>\
        ');

        countTotalOrder();

    } else {

        new PNotify( {
            title: 'Помилка',
            text: 'Даний товар вже добавлений в замовлення', 
            type: 'error'
        });

    }

}

function countTotalOrder () {

    var total = 0;

    if ($('#product_table .product').length > 0) {

        $('#product_table').show();

        $('#product_table .product [name="sum"]').each(function( index ) {

            var sum = parseFloat($(this).val());
    
            total += sum;
    
        });

        var discount = $('#product_table tfoot [name="discount"]').val();

        if (empty(discount)) {
            discount = 0;
            $('#product_table tfoot [name="discount"]').val(discount);
        }

        total -= discount;

        $('#product_table tfoot [name="total"]').val(total);
        
    } else {

        $('#product_table').hide();

    }

}