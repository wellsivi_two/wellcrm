$( document ).ready(function() {

    var users_table = $('#clients_table').DataTable({
        autoWidth: false,
        responsive: true,
        stateSave: true,
        order: [[0, 'desc']],
        ajax: {
            url : "/clients",
            type : "POST",
            async : false,
            headers : {
                'X-CSRF-TOKEN': selectNewToken()
            }
        },
        columns: [
            {"data" : "id"},
            {
                'data': 'name',
                'render': function(data, type, full, meta) {
                    return '<a href="client/' + full.id + '">' + full.name + ' ' + full.surname +  '</a>';
                }
            },
            {"data" : "created_at"},
            {"data" : "updated_at"},
        ],
        columnDefs: [
            { "width": "5%", "targets": 0 },
            { "width": "30%", "targets": 1 },
            { "width": "15%", "targets": 2 },
            { "width": "15%", "targets": 3 }
        ],
        dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Пошук:</span> _INPUT_',
            lengthMenu: '<span>Показати:</span> _MENU_',
            paginate: { 'first': 'Перша', 'last': 'Остання', 'next': '&rarr;', 'previous': '&larr;' },
            info: 'Записи з _START_ до _END_ . Всього:  _TOTAL_ записів',
            infoEmpty: 'Записів 0',
            zeroRecords: 'Записи відсутні'
        },
        drawCallback: function () {}
    });

});