$( document ).ready(function() {

    var orders_table = $('#orders_table').DataTable({
        autoWidth: false,
        responsive: true,
        stateSave: true,
        order: [[0, 'desc']],
        ajax: {
            url : "/orders",
            type : "POST",
            async : false,
            headers : {
                'X-CSRF-TOKEN': selectNewToken()
            }
        },
        columns: [
            {"data" : "id"},
            {
                'data': 'client_fio',
                'render': function(data, type, full, meta) {
                    console.log(full);
                    return '<a href="order/' + full.id + '">' + data +  '</a>';
                }
            },
            {"data" : "created_at"},
            {"data" : "updated_at"},
        ],
        columnDefs: [
            { "width": "50px", "targets": 0 },
            { "width": "200px", "targets": 2 },
            { "width": "200px", "targets": 3 }
        ],
        dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Пошук:</span> _INPUT_',
            lengthMenu: '<span>Показати:</span> _MENU_',
            paginate: { 'first': 'Перша', 'last': 'Остання', 'next': '&rarr;', 'previous': '&larr;' },
            info: 'Записи з _START_ до _END_ . Всього:  _TOTAL_ записів',
            infoEmpty: 'Записів 0',
            zeroRecords: 'Записи відсутні'
        },
        drawCallback: function () {}
    });

});