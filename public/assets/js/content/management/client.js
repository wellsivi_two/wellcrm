$( document ).ready(function() {
    
    $('[name="phone"], [name="new_phone"]').inputmask({"mask": "38 (999) 999-99-99"});

    $('.select').select2({
        minimumResultsForSearch: Infinity
    });
    
    $('#save_client').click(function () {

        var data = $('#client_form').collect_input();

        var response = http_post('/client/' + client.id, data);

        $('.form-group .validation-invalid-label').remove();

        if (response.status == 200) {
            
            if (empty(client.id)) {

                location.replace('/client/' + response.response.id);

            } else {

                new PNotify( {
                    title: 'Виконано',
                    text: response.response.message, 
                    type: 'success'
                });

            }

        } else if (response.status == 409) {

            new PNotify( {
                title: 'Помилка',
                text: response.response.message, 
                type: 'error'
            });

        } else if (response.status == 406) {

            $.each( response.response.errors, function( key, value ) {

                $('[name="' + value.input  + '"]').closest('.form-group').append('\
                    <label class="validation-invalid-label" for="default_select">Заповніть ' + value.message  + '</label>\
                ');

            });

            new PNotify( {
                title: 'Помилка',
                text: response.response.message, 
                type: 'error'
            });

        } else {

            new PNotify( {
                title: 'Помилка',
                text: 'Не визначена помилка', 
                type: 'error'
            });
        }

    });

    $('#delete_client').click(function () {
        
        var response = http_post('/client/' + client.id + '/delete');
        
        if (response.status == 200) {
            
            location.replace('/clients');
            
        } else {
            
            new PNotify( {
                title: 'Помилка',
                text: 'Не визначена помилка', 
                type: 'error'
            });
            
        }
        
    });
    
    $('#new_phone_button').click(function () {
        
        var phone = $('[name="new_phone"]').val().replace(/[^\d]/g, '');
        
        if (phone.length == 12) {

            $('#phones_block').append('\
                <div class="input-group mb-3">\
                    <span class="input-group-prepend">\
                        <button class="btn btn-light btn-icon legitRipple delete_phone_button"><i class="icon-cross2"></i></button>\
                    </span>\
                    <input type="text" name="phone" class="form-control" readonly placeholder="Телефон" value="' + phone  + '">\
                </div>\
            ');

            $('[name="phone"]').inputmask({"mask": "38 (999) 999-99-99"});

            $('[name="new_phone"]').val('');
            
        } else {

            new PNotify( {
                title: 'Помилка',
                text: 'Введіть коректний номер телефону', 
                type: 'error'
            });

        }
        
    });

    $('body')
        .off('click', '.delete_phone_button')
        .on('click', '.delete_phone_button', function () {

            $(this).closest('.input-group').remove();

        });

    $('#new_email_button').click(function () {
        
        var email = $('[name="new_email"]').val();

        if (validateEmail(email)) {

            $('#emails_block').append('\
                <div class="input-group mb-3">\
                    <span class="input-group-prepend">\
                        <button class="btn btn-light btn-icon legitRipple delete_email_button" type="button"><i class="icon-cross2"></i></button>\
                    </span>\
                    <input type="text" name="email" class="form-control" readonly placeholder="Телефон" value="' + email  + '">\
                </div>\
            ');

            $('[name="new_email"]').val('');

        } else {

            new PNotify( {
                title: 'Помилка',
                text: 'Введіть коректний email', 
                type: 'error'
            });

        }
        
    });

    $('body')
        .off('click', '.delete_email_button')
        .on('click', '.delete_email_button', function () {

            $(this).closest('.input-group').remove();

        });

    if (client.id != 0) {

        $('[name="sex"]').val(client.sex).trigger('change');

    } else {
        
        $('#delete_client').remove();

    }
    
});