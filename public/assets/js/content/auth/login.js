$( document ).ready(function() {

    $('.form-input-styled').uniform();

    $('body')
        .off('click', '#login')
        .on('click', '#login', function () {

            var data = {
                all : $('#login_form :input').collect_input(),
                required : $('#login_form :input[required]').collect_input(),
            }

            var result = verifyData(data['required']);

            if (result == true) {
                var response = http_post('/login', data['all']);

                if (response.status == 200) {

                    window.location.href = '/';

                } else if (response.status == 409) {

                    new PNotify( {
                        title: 'Помилка',
                        text: response.response.message,
                        icon: 'icon-blocked',
                        addclass: 'bg-danger border-danger'
                    });

                } else {

                    new PNotify({
                        title: 'Помилка',
                        text: 'Не визначена помилка',
                        icon: 'icon-blocked',
                        addclass: 'bg-danger border-danger'
                    });

                }

            } else {

                new PNotify({
                    title: 'Помилка',
                    text: 'Заповніть всі дані',
                    icon: 'icon-blocked',
                    addclass: 'bg-danger border-danger'
                });

            }

        });

});