<?php
Route::middleware(['auth'])->group(function () {

    Route::get('/', function () {
        return view('home.dashboard');
    });

    // User Routs

    Route::get('/users', [
        'uses'  => 'Administration\User\UsersController@show'
    ])->name('users');
    
    Route::post('/users', [
        'uses'  => 'Administration\User\UsersController@select'
    ]);

    Route::get('/user/{id}', [
        'uses'  => 'Administration\User\UserController@show'
    ])->name('user_edit')->where('id', '[0-9]+');
    
    Route::post('/user/{id}', [
        'uses'  => 'Administration\User\UserController@save'
    ])->where('id', '[0-9]+');
    
    Route::post('/user/{id}/delete', [
        'uses'  => 'Administration\User\UserController@delete'
    ]);
    
    Route::get('/user/complete', [
        'uses'  => 'Administration\User\UserController@complete'
    ]);

    // Role Routs

    Route::get('/roles', [
        'uses'  => 'Administration\Role\RolesController@show'
    ])->name('roles');
    
    Route::post('/roles', [
        'uses'  => 'Administration\Role\RolesController@select'
    ]);

    Route::get('/role/{id}', [
        'uses'  => 'Administration\Role\RoleController@show'
    ])->name('role_edit');
    
    Route::post('/role/{id}', [
        'uses'  => 'Administration\Role\RoleController@save'
    ]);
    
    Route::post('/role/{id}/delete', [
        'uses'  => 'Administration\Role\RoleController@delete'
    ]);

    // Product Routs

    Route::get('/products', [
        'uses'  => 'Storage\Product\ProductsController@show'
    ])->name('roles');
    
    Route::post('/products', [
        'uses'  => 'Storage\Product\ProductsController@select'
    ]);

    Route::get('/product/{id}', [
        'uses'  => 'Storage\Product\ProductController@show'
    ])->name('role_edit')->where('id', '[0-9]+');
    
    Route::post('/product/{id}', [
        'uses'  => 'Storage\Product\ProductController@save'
    ])->where('id', '[0-9]+');
    
    Route::post('/product/{id}/delete', [
        'uses'  => 'Storage\Product\ProductController@delete'
    ]);

    Route::get('/product/complete', [
        'uses'  => 'Storage\Product\ProductController@complete'
    ]);
    
    // Client Routs

    Route::get('/clients', [
        'uses'  => 'Management\Client\ClientsController@show'
    ]);
    
    Route::post('/clients', [
        'uses'  => 'Management\Client\ClientsController@select'
    ]);

    Route::get('/client/{id}', [
        'uses'  => 'Management\Client\ClientController@show'
    ])->where('id', '[0-9]+');
    
    Route::post('/client/{id}', [
        'uses'  => 'Management\Client\ClientController@save'
    ])->where('id', '[0-9]+');
    
    Route::post('/client/{id}/delete', [
        'uses'  => 'Management\Client\ClientController@delete'
    ]);

    Route::get('/client/phone/complete', [
        'uses'  => 'Management\Client\ClientController@completeByPhone'
    ]);

    Route::get('/client/email/complete', [
        'uses'  => 'Management\Client\ClientController@completeByEmail'
    ]);

    // Order Routs

    Route::get('/orders', [
        'uses'  => 'Management\Order\OrdersController@show'
    ]);
    
    Route::post('/orders', [
        'uses'  => 'Management\Order\OrdersController@select'
    ]);

    Route::get('/order/{id}', [
        'uses'  => 'Management\Order\OrderController@show'
    ]);
    
    Route::post('/order/{id}', [
        'uses'  => 'Management\Order\OrderController@save'
    ]);

    Route::post('/order/{id}/change/delivery', [
        'uses'  => 'Management\Order\OrderController@changeDelivery'
    ]);

    Route::post('/order/{id}/change/status', [
        'uses'  => 'Management\Order\OrderController@changeStatus'
    ]);
    
    Route::post('/order/{id}/delete', [
        'uses'  => 'Management\Order\OrderController@delete'
    ]);

    // Store Routs

    Route::get('/stores', [
        'uses'  => 'Administration\Store\StoresController@show'
    ]);
    
    Route::post('/stores', [
        'uses'  => 'Administration\Store\StoresController@select'
    ]);

    Route::get('/store/{id}', [
        'uses'  => 'Administration\Store\StoreController@show'
    ]);
    
    Route::post('/store/{id}', [
        'uses'  => 'Administration\Store\StoreController@save'
    ]);
    
    Route::post('/store/{id}/delete', [
        'uses'  => 'Administration\Store\StoreController@delete'
    ]);

    Route::post('/store/{id}/orders/status/matrix', [
        'uses'  => 'Administration\Store\StoreController@renderOrdersStatusMatrix'
    ]);

    // Status Routs

    Route::get('/statuses', [
        'uses'  => 'Administration\Status\StatusesController@show'
    ]);
    
    Route::post('/statuses', [
        'uses'  => 'Administration\Status\StatusesController@select'
    ]);

    Route::get('/status/{id}', [
        'uses'  => 'Administration\Status\StatusController@show'
    ]);
    
    Route::post('/status/{id}', [
        'uses'  => 'Administration\Status\StatusController@save'
    ]);
    
    Route::post('/status/{id}/delete', [
        'uses'  => 'Administration\Status\StatusController@delete'
    ]);

    // Deliveries Routs

    Route::get('/deliveries', [
        'uses'  => 'Administration\Delivery\DeliveriesController@show'
    ]);
    
    Route::post('/deliveries', [
        'uses'  => 'Administration\Delivery\DeliveriesController@select'
    ]);

    Route::get('/delivery/{id}', [
        'uses'  => 'Administration\Delivery\DeliveryController@show'
    ]);
    
    Route::post('/delivery/{id}', [
        'uses'  => 'Administration\Delivery\DeliveryController@save'
    ]);
    
    Route::post('/delivery/{id}/delete', [
        'uses'  => 'Administration\Delivery\DeliveryController@delete'
    ]);

    // Payments Routs

    Route::get('/payments', [
        'uses'  => 'Administration\Payment\PaymentsController@show'
    ]);
    
    Route::post('/payments', [
        'uses'  => 'Administration\Payment\PaymentsController@select'
    ]);

    Route::get('/payment/{id}', [
        'uses'  => 'Administration\Payment\PaymentController@show'
    ]);
    
    Route::post('/payment/{id}', [
        'uses'  => 'Administration\Payment\PaymentController@save'
    ]);
    
    Route::post('/payment/{id}/delete', [
        'uses'  => 'Administration\Payment\PaymentController@delete'
    ]);

    // Integrations Routs

    Route::get('/integrations', [
        'uses'  => 'Store\Integration\IntegrationsController@show'
    ]);

    Route::get('/integration/novaposhta', [
        'uses'  => 'Store\Integration\NovaPoshtaController@show'
    ]);

    Route::post('/integration/novaposhta/save', [
        'uses'  => 'Store\Integration\NovaPoshtaController@save'
    ]);

    Route::get('/integration/novaposhta/account/complete', [
        'uses'  => 'Store\Integration\NovaPoshtaController@completeAccount'
    ]);

    Route::get('/integration/novaposhta/import/data', [
        'uses'  => 'Store\Integration\NovaPoshtaController@importData'
    ]);

    Route::get('/integration/novaposhta/area/complete', [
        'uses'  => 'Store\Integration\NovaPoshtaController@completeArea'
    ]);

    Route::get('/integration/novaposhta/city/complete', [
        'uses'  => 'Store\Integration\NovaPoshtaController@completeCity'
    ]);

    Route::get('/integration/novaposhta/warehouse/complete', [
        'uses'  => 'Store\Integration\NovaPoshtaController@completeWarehouse'
    ]);

    Route::get('/integration/novaposhta/delivery_description/complete', [
        'uses'  => 'Store\Integration\NovaPoshtaController@completeDeliveryDescription'
    ]);

    Route::get('/integration/novaposhta/ttn/save', [
        'uses'  => 'Store\Integration\NovaPoshtaTtnController@save'
    ]);

    Route::post('/integration/novaposhta/ttn/create', [
        'uses'  => 'Store\Integration\NovaPoshtaTtnController@createTtn'
    ]);

    Route::post('/integration/novaposhta/ttn/delete', [
        'uses'  => 'Store\Integration\NovaPoshtaTtnController@deleteTtn'
    ]);

    // Logout

    Route::post('/logout', [
        'uses'  => 'Auth\LoginController@logout'
    ]);

});

Route::get('/login', [
    'uses'  => 'Auth\LoginController@show'
])->name('login');

Route::post('/login', [
    'uses'  => 'Auth\LoginController@login'
]);

Route::get('/refresh/csrf_token', function(){
    session()->regenerate();
    return response()->json([
        "token"=>csrf_token()],
    200);
});
