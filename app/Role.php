<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{

    use SoftDeletes;

    protected $table = 'roles';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'id', 'name', 'responsible', 'create_at', 'update_at'
    ];

    public function users () {
        return $this->belongsToMany('App\User', 'users_roles', 'role_id', 'user_id');
    }

    public function permissions () {
        return $this->belongsToMany('App\Permission', 'roles_permissions', 'role_id', 'permission_id');
    }
}