<?php

    if (! function_exists('colsFrom2DArray')) {

        function colsFrom2DArray(array $array, $keys) {

            if (!is_array($keys)) $keys = [$keys];
            return array_map(function ($el) use ($keys) {

                if (is_object($el)) {
                    $el = (array) $el;
                }

                $o = [];
                foreach($keys as $key){
                    //  if(isset($el[$key]))$o[$key] = $el[$key]; //you can do it this way if you don't want to set a default for missing keys.
                    $o[$key] = isset($el[$key])?$el[$key]:false;
                }
                return $o;
            }, $array);

            exit();
        }

    }

    if (! function_exists('colsFromArray')) {

        function colsFromArray(array $array, $keys) {

            $result = [];

            foreach($keys as $key){
                //  if(isset($el[$key]))$o[$key] = $el[$key]; //you can do it this way if you don't want to set a default for missing keys.
                $result[$key] = isset($array[$key])?$array[$key]:false;
            }

            return $result;

        }

    }

    if (! function_exists('getDataForDataTable')) {

        function getDataForDataTable(array $array) {

            $data = [
                'name' => $array["columns"][$array["order"][0]["column"]]["data"],
                'order' => $array["order"][0]["dir"],
                'start' => $array["start"],
                'length' => $array["length"],
                'search' => $array["search"]["value"],
                'columns' => array_column($array['columns'], 'data')
            ];

            return $data;

        }

    }

    if (! function_exists('searchInObject')) {

        function searchInObject(array $array, $search_string, $search_limit = 0, $columns_to_search) {
            
            $result = [];

            if (strlen($search_string) > $search_limit) {
                $search_array = explode(" ", $search_string);
                if (is_array($array)) {
                    foreach ($array as $current_key => $current_row) {

                        if (is_object($current_row)) {
                            $current_row = (array) $current_row;
                        }

                        $current_row = colsFromArray($current_row, $columns_to_search);

                        $current_row_string = implode('|', $current_row);
                        $is_found = true;
                        foreach ($search_array as $search_value) {
                            if (strlen($search_value) > 0) {
                                $position = mb_stripos($current_row_string, $search_value, 0, 'UTF-8');
                                if ($position === false) {
                                    $is_found = false;
                                }
                            }
                        }
                        if ($is_found == true) {
                            array_push($result, $array[$current_key]);
                        }
                    }
                }
            } else {
                $result = $array;
            }

            return $result;

        }

    }

?>