<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NovaPoshtaAccount extends Model
{

    use SoftDeletes;

    protected $table = 'novaposhta_accounts';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'id',
        'Key',
        'sender_Ref',
        'contact_Ref',
        'Description',
        'Phones',
        'active'
    ];

}
