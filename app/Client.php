<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;

    protected $table = 'clients';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'id', 'name', 'surname', 'sex'
    ];

    public function phones () {
        return $this->hasMany('App\ClientPhone');
    }

    public function emails () {
        return $this->hasMany('App\ClientEmail');
    }
}
