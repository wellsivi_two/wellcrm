<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Store extends Model
{
    
    use SoftDeletes;

    protected $table = 'stores';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'id', 'name', 'orders_status_matrix', 'start_order_status_id'
    ];

    public function statuses () {
        return $this->belongsToMany('App\Status', 'stores_statuses', 'store_id', 'status_id');
    }

    public function deliveries () {
        return $this->belongsToMany('App\Delivery', 'stores_deliveries', 'store_id', 'delivery_id');
    }

    public function payments () {
        return $this->belongsToMany('App\Payment', 'stores_payments', 'store_id', 'payment_id');
    }

}
