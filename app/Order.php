<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    protected $table = 'orders';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'id',
        'client_id',
        'status_id',
        'delivery_id',
        'payment_id',
        'store_id',
        'discount',
        'total',
    ];

    public function client () {
        return $this->hasOne('App\Client', 'id', 'client_id')->with(['phones']);
    }

    public function products () {
        return $this->belongsToMany('App\Product', 'orders_products', 'order_id', 'product_id')
        ->withPivot('amount', 'price', 'discount', 'sum');
    }
}
