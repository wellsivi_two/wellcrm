<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreStatus extends Model
{

    protected $table = 'stores_statuses';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'id',
        'store_id',
        'status_id'
    ];

}
