<?php

namespace App\Http\Controllers\Store\Integration;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Integration;

use Gate;
use DB;
use LisDev\Delivery\NovaPoshtaApi2;

class NovaPoshtaController extends Controller
{
    
    public function show (Request $request) {
        
        abort_unless(Gate::allows('integration_view'), 403);

        $novaposhta = Integration::where('ref', 'novaposhta')->first();

        $novaposhta->settings = json_decode($novaposhta->settings);

        $novaposhta->accounts = DB::table('novaposhta_accounts')->get();

        $novaposhta->statuses = $this->selectStatuses();

        if (view()->exists('store.novaposhta')) {
            return view('store.novaposhta')->withTitle('Нова Пошта')->with([
                'novaposhta' => $novaposhta
            ]);
        }

    }

    public function save (Request $request) {
        
        abort_unless(Gate::allows('integration_save'), 403);

        $save_accounts = $this->saveAccounts($request);

        if ($save_accounts->status() && $save_accounts->status() != 200) {
            return $save_accounts;
        }

        $save_statuses = $this->saveStatuses($request);

        if ($save_statuses->status() && $save_statuses->status() != 200) {
            return $save_statuses;
        }

        return response()->json([
            'message' => 'Дані збережено'
        ], 200);

    }

    public function saveAccounts (Request $request) {

        $accounts = DB::table('novaposhta_accounts')->get()->toArray();

        $insert = [];
        $update = [];

        if (count($accounts) > 0) {
            
            foreach ($request->accounts as $new_value) {

                $action_find = false;

                $new_value = (array) $new_value;
    
                foreach ($accounts as $old_value) {
    
                    $old_value = (array) $old_value;

                    if ($action_find == true) continue;
    
                    if ($new_value['sender_Ref'] == $old_value['sender_Ref'] && $new_value['contact_Ref'] == $old_value['contact_Ref']) {
                        array_push($update, $new_value);
                        $action_find = true;
                        continue;
                    }
    
                }

                if ($action_find == false) {
                    array_push($insert, $new_value);
                }
    
            }

        } else {

            $insert = $request->accounts;

        }

        // Insert accounts

        if (count($insert) > 0) {

            foreach ($insert as $value) {

                $np = new NovaPoshtaApi2($value['Key']);

                $counterparties = $np->getCounterparties('Sender');

                if ($counterparties['success'] === false) {
                    continue;
                }

                $contacts = $np->getCounterpartyContactPersons($counterparties['data'][0]['Ref']);

                foreach ($contacts['data'] as $contact_key => $contact_value) {

                    if ($contact_value['Ref'] == $value['contact_Ref']) {

                        $value['active'] = ($value['active'] == true) ? 1 : 0;

                        $new_account = colsFromArray($contact_value, [
                            'Description',
                            'Phones',
                        ]);

                        $new_account = array_merge($new_account, [
                            'Key' => $value['Key'],
                            'sender_Ref' => $value['sender_Ref'],
                            'contact_Ref' => $value['contact_Ref'],
                            'month_limit' => $value['month_limit'],
                            'active' => $value['active'],
                            'city' => $value['city'],
                            'city_Ref' => $value['city_Ref'],
                            'warehouse' => $value['warehouse'],
                            'warehouse_Ref' => $value['warehouse_Ref']
                        ]);

                        DB::table('novaposhta_accounts')->insert($new_account);

                    }

                }
            }

        }

        // Update accounts

        if (count($update) > 0) {

            foreach ($update as $value) {

                $value['active'] = ($value['active'] == true) ? 1 : 0;

                DB::table('novaposhta_accounts')->where([
                    'Key' => $value['Key'],
                    'contact_Ref' => $value['contact_Ref']
                ])->update([
                    'month_limit' => $value['month_limit'],
                    'active' => $value['active'],
                    'city' => $value['city'],
                    'city_Ref' => $value['city_Ref'],
                    'warehouse' => $value['warehouse'],
                    'warehouse_Ref' => $value['warehouse_Ref']
                ]);
            }

        }

        return response()->json(200);

    }

    public function saveStatuses (Request $request) {

        $statuses = [];

        foreach ($request->statuses as $status) {

            $statuses[$status['StateId']] = [
                'limit_debiting' => $status['limit_debiting']
            ];

        }

        $novaposhta_statuses_settings = DB::table('json_arrays')->where('key', 'novaposhta_statuses_settings')->first();

        if (is_null($novaposhta_statuses_settings)) {

            DB::table('json_arrays')->insert([
                'key' => 'novaposhta_statuses_settings',
                'value' => json_encode($statuses)
            ]);


        } else {

            DB::table('json_arrays')->where([
                'key' => 'novaposhta_statuses_settings',
            ])->update([
                'value' => json_encode($statuses)
            ]);

        }

        return response()->json(200);

    }

    public function selectStatuses () {

        $statuses = DB::connection('shared_bd')->table('novaposhta_statuses')->get();

        $novaposhta_statuses_array = DB::table('json_arrays')->where('key', 'novaposhta_statuses_settings')->first();

        if (is_null($novaposhta_statuses_array)) {
            $novaposhta_statuses_settings = [];
        } else {
            $novaposhta_statuses_settings = json_decode($novaposhta_statuses_array->value, true);
        }

        foreach ($statuses as $key => $value) {

            if (isset($novaposhta_statuses_settings[$value->StateId])) {
                $statuse_settings = $novaposhta_statuses_settings[$value->StateId];
            } else {
                $statuse_settings = [
                    'limit_debiting' => false
                ];
            }

            $statuses[$key]->limit_debiting = ($statuse_settings['limit_debiting'] === false) ? false : true;

        }

        return $statuses;

    }

    public function firstActiveAccount () {

        $account = DB::table('novaposhta_accounts')
            ->where('active', 1)
            ->orderBy('id', 'desc')
            ->first();

        if (!is_null($account)) {

            return response()->json($account, 200);
            
        } else {

            return response()->json([
                'errors' => [],
                'message' => 'В системі немає жодного активного акаунту Нової пошти'
            ], 406);

        }

    }

    public function completeAccount (Request $request) {
        
        $api_key = $request->get('term');

        $np = new NovaPoshtaApi2($api_key);

        $counterparties = $np->getCounterparties('Sender');

        $contacts = $np->getCounterpartyContactPersons($counterparties['data'][0]['Ref'])['data'] ;

        foreach ($contacts as $key => $value) {

            $contacts[$key]['label'] = $value['Description'] . '(' . $value['Phones']  .  ')';
            $contacts[$key]['value'] = $value['Description'] . '(' . $value['Phones']  .  ')';
            $contacts[$key]['Key'] = $api_key;
            $contacts[$key]['sender_Ref'] = $counterparties['data'][0]['Ref'];
            $contacts[$key]['contact_Ref'] = $value['Ref'];

        }

        return response()->json($contacts, 200);
        
    }

    public function completeArea (Request $request) {
        
        $query = $request->get('term');

        $cities = DB::connection('shared_bd')
            ->table('novaposhta_areas')
                ->where('Description', 'like', '%' . $query . '%')
                    ->orderByRaw('CHAR_LENGTH(Description) ASC')
                        ->get();
        
        $cities = $cities->toArray();

        $cities = colsFrom2DArray($cities, [
            'Description',
            'Ref',
            'SettlementTypeDescription'
        ]);

        foreach ($cities as $key => $value) {

            $cities[$key]['label'] = $value['Description'];
            $cities[$key]['value'] = $value['Description'];

        }

        return response()->json($cities, 200);
        
    }

    public function completeCity (Request $request) {
        
        $query = $request->get('term');
        $area_ref = $request->get('area_ref');

        $where = [
            ['Description', 'like', '%' . $query . '%']
        ];

        if (!is_null($area_ref)) {
            array_push($where, ['Area', $area_ref]);
        }

        $cities = DB::connection('shared_bd')
            ->table('novaposhta_cities')
                ->where($where)
                    ->orderByRaw('CHAR_LENGTH(Description) ASC')
                        ->get();
        
        $cities = $cities->toArray();

        $cities = colsFrom2DArray($cities, [
            'Description',
            'Ref',
            'SettlementTypeDescription'
        ]);

        foreach ($cities as $key => $value) {

            $cities[$key]['label'] = $value['SettlementTypeDescription'] . ' ' . $value['Description'];
            $cities[$key]['value'] = $value['Description'];

        }

        return response()->json($cities, 200);
        
    }

    public function completeWarehouse (Request $request) {
        
        $query = $request->get('term');
        $city_ref = $request->get('city_ref');

        $result = DB::connection('shared_bd')
            ->table('novaposhta_warehouses')
                ->where('Description', 'like', '%' . $query . '%')
                    ->where('CityRef', $city_ref)
                        ->orderByRaw('CHAR_LENGTH(Description) ASC')
                            ->get();
        
        $result = $result->toArray();

        $result = colsFrom2DArray($result, [
            'Description',
            'Ref'
        ]);

        foreach ($result as $key => $value) {

            $result[$key]['label'] = $value['Description'];
            $result[$key]['value'] = $value['Description'];

        }

        return response()->json($result, 200);
        
    }

    public function completeDeliveryDescription (Request $request) {
        
        $query = $request->get('term');
        $city_ref = $request->get('city_ref');

        $result = DB::connection('shared_bd')
            ->table('novaposhta_delivery_description')
                ->where('Description', 'like', '%' . $query . '%')
                    ->orderByRaw('CHAR_LENGTH(Description) ASC')
                        ->get();
        
        $result = $result->toArray();

        $result = colsFrom2DArray($result, [
            'Description',
            'Ref'
        ]);

        foreach ($result as $key => $value) {

            $result[$key]['label'] = $value['Description'];
            $result[$key]['value'] = $value['Description'];

        }

        return response()->json($result, 200);
        
    }

    public function importData () {

        // Import areas

        // $account = $this->firstActiveAccount();

        // if ($account->status() && $account->status() == 200) {
        //     $account = $account->getData();
        // } else {
        //     return $account;
        // }

        $np = new NovaPoshtaApi2('24877033d062f4907fe138bcf930f3c9'); // $account->Key

        // Import areas

        $import_areas = $this->importAreas($np);

        if ($import_areas->status() && $import_areas->status() != 200) {
            return $import_areas;
        }

        // Import cities

        $import_cities = $this->importCities($np);

        if ($import_cities->status() && $import_cities->status() != 200) {
            return $import_cities;
        }

        // Import warehouses

        $import_warehouses = $this->importWarehouses($np);

        if ($import_warehouses->status() && $import_warehouses->status() != 200) {
            return $import_warehouses;
        }

        // Import delivery payers

        $import_delivery_payer_types = $this->importDeliveryPayers($np);

        if ($import_delivery_payer_types->status() && $import_delivery_payer_types->status() != 200) {
            return $import_delivery_payer_types;
        }

        // Import redelivery payers

        $import_redelivery_payer_types = $this->importRedeliveryPayers($np);

        if ($import_redelivery_payer_types->status() && $import_redelivery_payer_types->status() != 200) {
            return $import_redelivery_payer_types;
        }

        // Import delivery cargos

        $import_delivery_cargos = $this->importDeliveryCargos($np);

        if ($import_delivery_cargos->status() && $import_delivery_cargos->status() != 200) {
            return $import_delivery_cargos;
        }

        // Import delivery descriptions

        $import_delivery_descriptions = $this->importDeliveryDescriptions($np);

        if ($import_delivery_descriptions->status() && $import_delivery_descriptions->status() != 200) {
            return $import_delivery_descriptions;
        }

        // Import redelivery cargos

        $import_redelivery_cargos = $this->importRedeliveryCargos($np);

        if ($import_redelivery_cargos->status() && $import_redelivery_cargos->status() != 200) {
            return $import_redelivery_cargos;
        }

        // Import services

        $import_services = $this->importServices($np);

        if ($import_services->status() && $import_services->status() != 200) {
            return $import_services;
        }

        // Import payments

        $import_payments = $this->importPayments($np);

        if ($import_payments->status() && $import_payments->status() != 200) {
            return $import_payments;
        }

        // Import counterparties

        $import_counterparties = $this->importCounterparties($np);

        if ($import_counterparties->status() && $import_counterparties->status() != 200) {
            return $import_counterparties;
        }

        // Import statuses

        $import_statuses = $this->importStatuses();

        if ($import_statuses->status() && $import_statuses->status() != 200) {
            return $import_statuses;
        }

        return response()->json(200);

    }
    
    public function importAreas ($np) {

        $areas = $np->getAreas();

        if ($areas['success'] == true && count($areas['data']) > 0) {

            DB::connection('shared_bd')->table('novaposhta_areas')->truncate();

            $areas = colsFrom2DArray($areas['data'], [
                'Ref', 
                'AreasCenter', 
                'Description'
            ]);

            $areas_chunk = array_chunk($areas, 5);

            foreach ($areas_chunk as $areas_array) {
    
                DB::connection('shared_bd')->table('novaposhta_areas')->insert($areas_array);
    
            }

            return response()->json(200);

        } else {

            return response()->json([
                'message' => 'Помилка при імпорті населених пунктів'
            ], 409);

        }

    }

    public function importCities ($np) {

        $cities = $np->getCities();

        if ($cities['success'] == true && count($cities['data']) > 0) {

            DB::connection('shared_bd')->table('novaposhta_cities')->truncate();

            $cities = colsFrom2DArray($cities['data'], [
                'Description', 
                'Ref', 
                'Delivery1', 
                'Delivery2', 
                'Delivery3',
                'Delivery4',
                'Delivery5',
                'Delivery6',
                'Delivery7',
                'Area',
                'SettlementType',
                'SettlementTypeDescription',
                'CityID'
            ]);

            $cities_chunk = array_chunk($cities, 5);

            foreach ($cities_chunk as $cities_array) {
    
                DB::connection('shared_bd')->table('novaposhta_cities')->insert($cities_array);
    
            }

            return response()->json(200);

        } else {

            return response()->json([
                'message' => 'Помилка при імпорті населених пунктів'
            ], 409);

        }

    }

    public function importWarehouses ($np) {

        $warehouses = $np->getWarehouses(null);

        if ($warehouses['success'] == true && count($warehouses['data']) > 0) {

            DB::connection('shared_bd')->table('novaposhta_warehouses')->truncate();

            $warehouses = colsFrom2DArray($warehouses['data'], [
                'SiteKey',
                'Description',
                'ShortAddress',
                'TypeOfWarehouse',
                'Ref',
                'Number',
                'CityRef',
                'CityDescription',
                'WarehouseStatus'
            ]);

            $warehouses_chunk = array_chunk($warehouses, 10);

            foreach ($warehouses_chunk as $warehouses_array) {
    
                DB::connection('shared_bd')->table('novaposhta_warehouses')->insert($warehouses_array);
    
            }

            return response()->json(200);

        } else {

            return response()->json([
                'message' => 'Помилка при імпорті населених пунктів'
            ], 409);

        }

    }

    public function importDeliveryPayers ($np) {

        $delivery_payer_types = $np->__call('getTypesOfPayers', null);

        if ($delivery_payer_types['success'] == true && count($delivery_payer_types['data']) > 0) {

            DB::connection('shared_bd')->table('novaposhta_delivery_payers')->truncate();

            $delivery_payer_types = colsFrom2DArray($delivery_payer_types['data'], [
                'Description',
                'Ref'
            ]);

            $delivery_payer_types_chunk = array_chunk($delivery_payer_types, 10);

            foreach ($delivery_payer_types_chunk as $delivery_payer_types_array) {
    
                DB::connection('shared_bd')->table('novaposhta_delivery_payers')->insert($delivery_payer_types_array);
    
            }

            return response()->json(200);

        } else {

            return response()->json([
                'message' => 'Помилка при імпорті типів платників за доставку'
            ], 409);

        }

    }

    public function importRedeliveryPayers ($np) {

        $redelivery_payer_types = $np->__call('getTypesOfPayersForRedelivery', null);

        if ($redelivery_payer_types['success'] == true && count($redelivery_payer_types['data']) > 0) {

            DB::connection('shared_bd')->table('novaposhta_redelivery_payers')->truncate();

            $redelivery_payer_types = colsFrom2DArray($redelivery_payer_types['data'], [
                'Description',
                'Ref'
            ]);

            $redelivery_payer_types_chunk = array_chunk($redelivery_payer_types, 10);

            foreach ($redelivery_payer_types_chunk as $redelivery_payer_types_array) {
    
                DB::connection('shared_bd')->table('novaposhta_redelivery_payers')->insert($redelivery_payer_types_array);
    
            }

            return response()->json(200);

        } else {

            return response()->json([
                'message' => 'Помилка при імпорті типів платників за доставку'
            ], 409);

        }

    }

    public function importDeliveryCargos ($np) {

        $delivery_cargos = $np->__call('getCargoTypes', null);

        if ($delivery_cargos['success'] == true && count($delivery_cargos['data']) > 0) {

            DB::connection('shared_bd')->table('novaposhta_delivery_cargos')->truncate();

            $delivery_cargos = colsFrom2DArray($delivery_cargos['data'], [
                'Description',
                'Ref'
            ]);

            $delivery_cargos_chunk = array_chunk($delivery_cargos, 10);

            foreach ($delivery_cargos_chunk as $delivery_cargos_array) {
    
                DB::connection('shared_bd')->table('novaposhta_delivery_cargos')->insert($delivery_cargos_array);
    
            }

            return response()->json(200);

        } else {

            return response()->json([
                'message' => 'Помилка при імпорті типів платників за доставку'
            ], 409);

        }

    }

    public function importDeliveryDescriptions ($np) {

        $delivery_cargos = $np->__call('getCargoDescriptionList', null);

        if ($delivery_cargos['success'] == true && count($delivery_cargos['data']) > 0) {

            DB::connection('shared_bd')->table('novaposhta_delivery_description')->truncate();

            $delivery_cargos = colsFrom2DArray($delivery_cargos['data'], [
                'Description',
                'Ref'
            ]);

            $delivery_cargos_chunk = array_chunk($delivery_cargos, 10);

            foreach ($delivery_cargos_chunk as $delivery_cargos_array) {
    
                DB::connection('shared_bd')->table('novaposhta_delivery_description')->insert($delivery_cargos_array);
    
            }

            return response()->json(200);

        } else {

            return response()->json([
                'message' => 'Помилка при імпорті описів посилки'
            ], 409);

        }

    }
    
    public function importRedeliveryCargos ($np) {

        $redelivery_cargos = $np->__call('getBackwardDeliveryCargoTypes', null);

        if ($redelivery_cargos['success'] == true && count($redelivery_cargos['data']) > 0) {

            DB::connection('shared_bd')->table('novaposhta_redelivery_cargos')->truncate();

            $redelivery_cargos = colsFrom2DArray($redelivery_cargos['data'], [
                'Description',
                'Ref'
            ]);

            $redelivery_cargos_chunk = array_chunk($redelivery_cargos, 10);

            foreach ($redelivery_cargos_chunk as $redelivery_cargos_array) {
    
                DB::connection('shared_bd')->table('novaposhta_redelivery_cargos')->insert($redelivery_cargos_array);
    
            }

            return response()->json(200);

        } else {

            return response()->json([
                'message' => 'Помилка при імпорті типів платників за доставку'
            ], 409);

        }

    }

    public function importServices ($np) {

        $services = $np->__call('getServiceTypes', null);

        if ($services['success'] == true && count($services['data']) > 0) {

            DB::connection('shared_bd')->table('novaposhta_services')->truncate();

            $services = colsFrom2DArray($services['data'], [
                'Description',
                'Ref'
            ]);

            $services_chunk = array_chunk($services, 10);

            foreach ($services_chunk as $services_array) {
    
                DB::connection('shared_bd')->table('novaposhta_services')->insert($services_array);
    
            }

            return response()->json(200);

        } else {

            return response()->json([
                'message' => 'Помилка при імпорті типів доставки'
            ], 409);

        }

    }

    public function importPayments ($np) {

        $payments = $np->__call('getPaymentForms', null);

        if ($payments['success'] == true && count($payments['data']) > 0) {

            DB::connection('shared_bd')->table('novaposhta_payments')->truncate();

            $payments = colsFrom2DArray($payments['data'], [
                'Description',
                'Ref'
            ]);

            $payments_chunk = array_chunk($payments, 10);

            foreach ($payments_chunk as $payments_array) {
    
                DB::connection('shared_bd')->table('novaposhta_payments')->insert($payments_array);
    
            }

            return response()->json(200);

        } else {

            return response()->json([
                'message' => 'Помилка при імпорті типів доставки'
            ], 409);

        }

    }

    public function importCounterparties ($np) {

        $counterparties = $np->__call('getTypesOfCounterparties', null);

        if ($counterparties['success'] == true && count($counterparties['data']) > 0) {

            DB::connection('shared_bd')->table('novaposhta_counterparties')->truncate();

            $counterparties = colsFrom2DArray($counterparties['data'], [
                'Description',
                'Ref'
            ]);

            $counterparties_chunk = array_chunk($counterparties, 10);

            foreach ($counterparties_chunk as $counterparties_array) {
    
                DB::connection('shared_bd')->table('novaposhta_counterparties')->insert($counterparties_array);
    
            }

            return response()->json(200);

        } else {

            return response()->json([
                'message' => 'Помилка при імпорті контрагентів'
            ], 409);

        }

    }

    public function importStatuses () {

        $statuses = json_decode('[{"StateId":"1","StateName":"Нова пошта очікує надходження від відправника"},{"StateId":"2","StateName":"Видалено"},{"StateId":"3","StateName":"Номер не знайдено"},{"StateId":"4","StateName":"Відправлення у місті"},{"StateId":"41","StateName":"Відправлення у місті"},{"StateId":"5","StateName":"Відправлення прямує до міста"},{"StateId":"6","StateName":"Відправлення у місті. Очікуйте додаткове повідомлення про прибуття."},{"StateId":"7","StateName":"Прибув на відділення"},{"StateId":"8","StateName":"Прибув на відділення"},{"StateId":"9","StateName":"Відправлення отримано"},{"StateId":"10","StateName":"Відправлення отримано. Протягом доби ви одержите SMS-повідомлення про надходження грошового переказу"},{"StateId":"11","StateName":"Відправлення отримано. Грошовий переказ видано одержувачу."},{"StateId":"14","StateName":"Відправлення передано до огляду отримувачу"},{"StateId":"101","StateName":"На шляху до одержувача"},{"StateId":"102","StateName":"Відмова одержувача"},{"StateId":"103","StateName":"Відмова одержувача"},{"StateId":"104","StateName":"Змінено адресу"},{"StateId":"105","StateName":"Припинено зберігання"},{"StateId":"106","StateName":"Одержано і створено ЄН зворотньої доставки"},{"StateId":"108","StateName":"Відмова одержувача"}]');

        DB::connection('shared_bd')->table('novaposhta_statuses')->truncate();

        $statuses = colsFrom2DArray($statuses, [
            'StateId', 
            'StateName'
        ]);

        $statuses_chunk = array_chunk($statuses, 5);

        foreach ($statuses_chunk as $statuses_array) {

            DB::connection('shared_bd')->table('novaposhta_statuses')->insert($statuses_array);

        }

        return response()->json(200);

    }

}
