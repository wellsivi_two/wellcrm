<?php

namespace App\Http\Controllers\Store\Integration;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Integration;

use Gate;

class IntegrationsController extends Controller
{
    
    public function show (Request $request) {
        
        abort_unless(Gate::allows('integration_view'), 403);

        $integrations = Integration::get();

        if (view()->exists('store.integrations')) {
            return view('store.integrations')->withTitle('Інтеграції')->with([
                'integrations' => $integrations
            ]);
        }

    }

}
