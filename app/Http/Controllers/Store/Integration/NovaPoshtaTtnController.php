<?php

namespace App\Http\Controllers\Store\Integration;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\NovaPoshtaTtn;

use LisDev\Delivery\NovaPoshtaApi2;
use DB;

class NovaPoshtaTtnController extends Controller
{
    
    public function save ($model, $id, Request $request) {

        $data = $request->all();

        $params = [];

        if (isset($model)) $params['model'] = (is_null($model) || empty($model)) ? '' : $model;
        if (isset($id)) $params['model_id'] = (is_null($id) || empty($id)) ? '' : $id;

        if (array_key_exists("novaposhta_account_id", $data)) $params['novaposhta_account_id'] = (empty($data['novaposhta_account_id'])) ? 0 : $data['novaposhta_account_id'];

        if (array_key_exists("cost", $data)) $params['cost'] = (empty($data['cost'])) ? 0 : $data['cost'];
        if (array_key_exists("weight", $data)) $params['weight'] = (empty($data['weight'])) ? 0 : $data['weight'];
        if (array_key_exists("length", $data)) $params['length'] = (empty($data['length'])) ? 0 : $data['length'];
        if (array_key_exists("width", $data)) $params['width'] = (empty($data['width'])) ? 0 : $data['width'];
        if (array_key_exists("height", $data)) $params['height'] = (empty($data['height'])) ? 0 : $data['height'];
        if (array_key_exists("volumetric_weight", $data)) $params['volumetric_weight'] = (empty($data['volumetric_weight'])) ? 0 : $data['volumetric_weight'];

        if (array_key_exists("phone", $data)) $params['phone'] = (empty($data['phone'])) ? '' : preg_replace('/[^0-9]/', '', $data['phone']);

        if (array_key_exists("last_name", $data)) $params['last_name'] = (empty($data['last_name'])) ? '' : trim($data['last_name']);
        if (array_key_exists("first_name", $data)) $params['first_name'] = (empty($data['first_name'])) ? '' : trim($data['first_name']);
        if (array_key_exists("middle_name", $data)) $params['middle_name'] = (empty($data['middle_name'])) ? '' : trim($data['middle_name']);

        if (array_key_exists("delivery_service", $data)) $params['delivery_service'] = (empty($data['delivery_service'])) ? '' : trim($data['delivery_service']);
        if (array_key_exists("delivery_payer", $data)) $params['delivery_payer'] = (empty($data['delivery_payer'])) ? '' : trim($data['delivery_payer']);
        if (array_key_exists("delivery_payment", $data)) $params['delivery_payment'] = (empty($data['delivery_payment'])) ? '' : trim($data['delivery_payment']);
        if (array_key_exists("delivery_cargo", $data)) $params['delivery_cargo'] = (empty($data['delivery_cargo'])) ? '' : $data['delivery_cargo'];

        if (array_key_exists("delivery_description", $data)) $params['delivery_description'] = (empty($data['delivery_description'])) ? '' : $data['delivery_description'];
        if (array_key_exists("delivery_description_Ref", $data)) $params['delivery_description_Ref'] = (empty($data['delivery_description_Ref'])) ? '' : $data['delivery_description_Ref'];

        if (array_key_exists("area", $data)) $params['area'] = (empty($data['area'])) ? '' : $data['area'];
        if (array_key_exists("area_Ref", $data)) $params['area_Ref'] = (empty($data['area_Ref'])) ? '' : $data['area_Ref'];

        if (array_key_exists("city", $data)) $params['city'] = (empty($data['city'])) ? '' : $data['city'];
        if (array_key_exists("city_Ref", $data)) $params['city_Ref'] = (empty($data['city_Ref'])) ? '' : $data['city_Ref'];

        if (array_key_exists("warehouse", $data)) $params['warehouse'] = (empty($data['warehouse'])) ? '' : $data['warehouse'];
        if (array_key_exists("warehouse_Ref", $data)) $params['warehouse_Ref'] = (empty($data['warehouse_Ref'])) ? '' : $data['warehouse_Ref'];

        if (array_key_exists("redelivery_cargo", $data)) $params['redelivery_cargo'] = (empty($data['redelivery_cargo'])) ? '' : $data['redelivery_cargo'];
        if (array_key_exists("redelivery_additional_string", $data)) $params['redelivery_additional_string'] = (empty($data['redelivery_additional_string'])) ? '' : $data['redelivery_additional_string'];
        if (array_key_exists("redelivery_payer", $data)) $params['redelivery_payer'] = (empty($data['redelivery_payer'])) ? '' : $data['redelivery_payer'];
        
        $novaposhta_ttn = NovaPoshtaTtn::where([
            'model' => $model,
            'model_id' => $id
        ])->first();


        $response = (is_null($novaposhta_ttn)) ? $this->create($params) : $this->update($params);

        if ($response->status() && $response->status() == 200) {
            $novaposhta_ttn = NovaPoshtaTtn::find($response->getData()->id);
        } else {
            return $response;
        }

        return response()->json([
            'message' => 'ТТН збережено',
            'id' => $novaposhta_ttn->id
        ], 200);

    }

    public function create ($params) {

        $create_model = new NovaPoshtaTtn($params);
        $create_model->save();
        
        return response()->json(['id' => $create_model->id], 200);
        
    }

    public function update ($params) {

        $update_model = NovaPoshtaTtn::where([
            'model' => $params['model'],
            'model_id' => $params['model_id']
        ])->first();

        if ($update_model->created == 1) {
            return response()->json(['id' => $update_model->id], 200);
        }

        $update_model->fill($params);

        $update_model->save();

        return response()->json(['id' => $update_model->id], 200);
        
    }

    public function createTtn (Request $request) {

        $novaposhta_ttn = NovaPoshtaTtn::find($request->id);

        if ($novaposhta_ttn) {

            if (empty($novaposhta_ttn->novaposhta_account_id)) return response()->json(['message' => 'Виберіть акаунт Нової Пошти'], 409);

            $account = DB::table('novaposhta_accounts')->find($novaposhta_ttn->novaposhta_account_id);

            $np = new NovaPoshtaApi2($account->Key);

            $params = [

                "NewAddress" => 1,
                "SeatsAmount" => 1,
                "DateTime" => date('d.m.Y'),
                "Description"   => $novaposhta_ttn->delivery_cargo,
                "AdditionalInformation" => $novaposhta_ttn->delivery_cargo,
                "InfoRegClientBarcodes" => $novaposhta_ttn->model_id,
                "BackwardDeliveryData" => [],

                "PayerType" => $novaposhta_ttn->delivery_payer,
                "PaymentMethod" => $novaposhta_ttn->delivery_payment,
                "CargoType"     => $novaposhta_ttn->delivery_cargo_Ref,
                "ServiceType"   => $novaposhta_ttn->delivery_service,

                "Cost" => $novaposhta_ttn->cost,
                "Weight" => $novaposhta_ttn->weight,
                "VolumeGeneral" => $novaposhta_ttn->volumetric_weight,

            ];

            $recipient = [
                'CounterpartyType'     => 'PrivatePerson',
                'RecipientType' => 'PrivatePerson',
                'FirstName'         => $novaposhta_ttn->first_name,
                'LastName'          => $novaposhta_ttn->last_name,
                'Phone'             => preg_replace('/[^0-9]/', '', $novaposhta_ttn->phone)
            ];

            $sender = [
                'Sender' => $account->sender_Ref,
                'ContactSender' => $account->contact_Ref,
                'SendersPhone' => $account->Phones,
            ];

            // Delivery Type

            if ($novaposhta_ttn->delivery_service == 'WarehouseWarehouse') {

                // Recipient

                $recipient = array_merge($recipient, [
                    'Recipient' => false,
                    'City' => $novaposhta_ttn->city,
                    'RecipientCityName' => $novaposhta_ttn->city,
                    'CityRef' => $novaposhta_ttn->city_Ref,
                    'CityRecipient' => $novaposhta_ttn->city_Ref,
                    'RecipientAddress' => $novaposhta_ttn->warehouse_Ref,
                    'Region' => false
                ]);

                // Sender

                $sender = array_merge($sender, [
                    'CitySender' => $account->city_Ref,
                    'SenderAddress' => $account->warehouse_Ref,
                ]);

            }

            // Redilivery Type

            if (!empty($novaposhta_ttn->redelivery_cargo)) {
    
                $params['BackwardDeliveryData'][] = (object) [
                    'PayerType' => $novaposhta_ttn->redelivery_payer,
                    'CargoType' => $novaposhta_ttn->redelivery_cargo,
                    'RedeliveryString' => $novaposhta_ttn->redelivery_additional_string
                ];

            }

            $data = array_merge($sender, $recipient, $params);
            
            $response = $np->newInternetDocument($sender, $recipient, $params);

            if ($response['success'] == true) {

                $np_order_response = $np->getDocument($response['data'][0]['Ref']);

                $np_order = $np_order_response['data'][0];

                $params = [
                    'number' => $np_order['Number'],
                    'ref' => $np_order['Ref'],
                    'status_id' => $np_order['StateId'],
                    'status_name' => $np_order['StateName'],
                    'created' => 1
                ];

                $novaposhta_ttn->fill($params);

                $novaposhta_ttn->save();

                return response()->json(['message' => 'ТТН створено!'], 200);

            } else {

                $error_string = '';

                foreach ($response['errors'] as $error) {
                    $error_string .= $error . ';<br />';
                }

                return response()->json(['message' => $error_string], 409);

            }

        } else {

            return response()->json(['message' => 'ТТН можна створювати тільки в існуючому замовленні'], 409);

        }
        
    }

    public function deleteTtn (Request $request) {

        $novaposhta_ttn = NovaPoshtaTtn::find($request->id);

        if ($novaposhta_ttn) {

            if (empty($novaposhta_ttn->novaposhta_account_id)) return response()->json(['message' => 'Виберіть акаунт Нової Пошти'], 409);

            $account = DB::table('novaposhta_accounts')->find($novaposhta_ttn->novaposhta_account_id);

            $np = new NovaPoshtaApi2($account->Key);

            $response =  $np->model('InternetDocument')->delete([
                'DocumentRefs' => $novaposhta_ttn->ref
            ]);

            if ($response['success'] == true) {

                $params = [
                    'number' => '',
                    'ref' => '',
                    'status_id' => 0,
                    'status_name' => '',
                    'created' => 0
                ];

                $novaposhta_ttn->fill($params);

                $novaposhta_ttn->save();

                return response()->json(['message' => 'ТТН видалена!'], 200);

            } else {

                $error_string = '';

                foreach ($response['errors'] as $error) {
                    $error_string .= $error . ';<br />';
                }

                return response()->json(['message' => $error_string], 409);

            }

        } else {

            return response()->json(['message' => 'ТТН можна видалятися тільки в існуючому замовленні'], 409);

        }
        
    }

}
