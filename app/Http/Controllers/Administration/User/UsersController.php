<?php

namespace App\Http\Controllers\Administration\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use Gate;

class UsersController extends Controller
{
    public function show (Request $request) {
        
        abort_unless(Gate::allows('user_view'), 403);

        if (view()->exists('administration.users')) {
            return view('administration.users')->withTitle('Користувачі');
        }

    }

    public function select (Request $request) {

        // abort_unless(Gate::allows('user_view'), 403);

        $users = User::with('roles')->get();

        foreach ($users as $user) {

            $user_roles_array = array_column($user->roles->toArray(), 'name');

            $user_roles_string = implode(', ', $user_roles_array);

            $users->find($user->id)->roles_string = $user_roles_string;

        }

        $response = array(
            "draw" => microtime(),
            "recordsTotal" => count($users),
            "recordsFiltered" => count($users),
            "data" => $users
        );

        return response()->json($response, 200);

    }
}
