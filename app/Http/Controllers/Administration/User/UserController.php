<?php

namespace App\Http\Controllers\Administration\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use App\Role;

use Gate;

class UserController extends Controller
{
    public function show ($id, Request $request) {
        
        abort_unless(Gate::allows('user_view'), 403);

        $param = [
            'user' => (object) [
                'id' => 0
            ],
            'roles' => Role::get()
        ];

        $user = User::with('roles')->find($id);

        if (is_null($user)) {

            $title = 'Створення користувача';

        } else {

            $title = 'Редагування користувача';

            $param['user'] = $user;

        }

        if (view()->exists('administration.user')) {
            return view('administration.user')->with($param)->withTitle($title);
        }

    }
    
    public function save ($id, Request $request) {
        
        if (Gate::allows('user_save')) {
            
            $user = User::with('roles')->find($id);

            $response = (is_null($user)) ? $this->create($request) : $this->update($id, $request);

            if ($response->status() && $response->status() == 200) {

                $user = User::with('roles')->find($response->getData()->id);
            } else {
                return $response;
            }
            
            if (Gate::allows('user_edit_roles')) {
                
                if (isset($request->roles) && !empty($request->roles)) {

                    if (isset($user->roles)) {
                        $user->roles()->detach();
                    }

                    $user->roles()->attach($request->roles);

                }
                
            }

            return response()->json([
                'message' => 'Користувач збережений',
                'id' => $user->id
            ], 200);
            
        }

    }
    
    public function create (Request $request) {

        $create_value = $request->only(['name', 'surname', 'email', 'phone']);

        $create_value['phone'] = preg_replace('/[^0-9]/', '', $create_value['phone']);

        if (!is_null($request->password) && $request->password == $request->re_password) {
            $create_value['password'] = bcrypt($request->password);
        } else {
            return response()->json(['message' => 'Паролі не співпадають'], 409);
        }

        $check_duplicate = User::where('phone', $create_value['phone'])
            ->orWhere('email', $create_value['email'])
            ->first();

        if (!isset($check_duplicate)) {

            $user = new User($create_value);
            $user->save();
            
            return response()->json(['id' => $user->id], 200);

        } else {
            return response()->json(['message' => 'Користувач з такою поштою або номером телефну вже існує в системі'], 409);
        } 
        
    }

    public function update ($id, Request $request) {

        $update_value = $request->only(['name', 'surname', 'email', 'phone']);

        $update_value['phone'] = preg_replace('/[^0-9]/', '', $update_value['phone']);

        if (!is_null($request->password) && $request->password == $request->re_password) {
            $update_value['password'] = bcrypt($request->password);
        } else if (!is_null($request->password) && $request->password != $request->re_password) {
            return response()->json(['message' => 'Паролі не співпадають'], 409);
        }

        $user = User::find($id);

        $user->fill($update_value);

        $user->save();

        return response()->json(['id' => $user->id], 200);
        
    }
    
    public function complete (Request $request) {
        
        $search = $request->get('term');

        $result = User::where('id', 'LIKE', '%'. $search. '%')
            ->orWhere('name', 'LIKE', '%'. $search. '%')
            ->orWhere('surname', 'LIKE', '%'. $search. '%')
            ->get();

        return response()->json($result);
        
    }
    
    public function delete ($id) {
        
        if (Gate::allows('user_delete')) {
            
            User::find($id)->delete();

            return response()->json(200);
            
        } else {
        
            return response()->json(403);
        
        }   
        
    }
    
}
