<?php

namespace App\Http\Controllers\Administration\Role;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Role;
use App\User;
use App\Permission;

use Gate;

class RoleController extends Controller
{
    public function show ($id, Request $request) {
        
        abort_unless(Gate::allows('role_view'), 403);

        $param = [
            'role' => (object) [
                'id' => 0
            ],
            'users' => User::get(),
            'permissions' => Permission::get()
        ];

        $role = Role::with(['users', 'permissions'])->find($id);

        if (is_null($role)) {

            $title = 'Створення ролі';

        } else {

            $title = 'Редагування ролі';

            $param['role'] = $role;

        }

        if (view()->exists('administration.role')) {
            return view('administration.role')->with($param)->withTitle($title);
        }

    }
    
    public function save ($id, Request $request) {
        
        if (Gate::allows('role_save')) {
            
            $role = Role::find($id);

            $response = (is_null($role)) ? $this->create($request) : $this->update($id, $request);

            if ($response->status() && $response->status() == 200) {
                $role = Role::with(['users', 'permissions'])->find($response->getData()->id);
            } else {
                return $response;
            }

            $this->users($role->id, $request);

            $request->permissions = array_keys($request->permissions);

            $this->permissions($role->id, $request);

            return response()->json([
                'message' => 'Роль збережена',
                'id' => $role->id
            ], 200);
            
        }

    }
    
    public function create (Request $request) {

        $create_value = $request->only(['name', 'responsible']);

        $role = new Role($create_value);
        $role->save();

        return response()->json(['id' => $role->id], 200);
        
    }

    public function update ($id, Request $request) {

        $update_value = $request->only(['name', 'responsible']);

        $role = Role::find($id);
        $role->fill($update_value);
        $role->save();

        return response()->json(['id' => $role->id], 200);
        
    }
    
    public function users ($id, Request $request) {
        
        if (Gate::allows('role_view_members') && Gate::allows('role_edit_members')) {
            
            $role = Role::with('users')->find($id);
        
            $role_users_id = array_column($role->users->toArray(), 'id');

            $users_for_add = array_diff($request->users, $role_users_id);

            $users_for_remove = array_diff($role_users_id, $request->users);

            $role->users()->detach($users_for_remove);

            $role->users()->attach($users_for_add);
            
        }
        
    }
    
    public function permissions ($id, Request $request) {
        
        if (Gate::allows('role_view_permissions') && Gate::allows('role_edit_permissions')) {

            $role = Role::with('permissions')->find($id);

            $role_permissions_id = array_column($role->permissions->toArray(), 'id');

            $permissions_for_add = array_diff($request->permissions, $role_permissions_id);

            $permissions_for_remove = array_diff($role_permissions_id, $request->permissions);

            $role->permissions()->detach($permissions_for_remove);

            $role->permissions()->attach($permissions_for_add);
            
        }
        
    }
    
    public function delete ($id) {
        
        if (Gate::allows('role_delete')) {
            
            Role::find($id)->delete();

            return response()->json(200);
            
        } else {
        
            return response()->json(403);
        
        }   
        
    }
    
}
