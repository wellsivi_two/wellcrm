<?php

namespace App\Http\Controllers\Administration\Role;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Role;
use Gate;

class RolesController extends Controller
{
    public function show (Request $request) {
        
        abort_unless(Gate::allows('role_view'), 403);

        if (view()->exists('administration.roles')) {
            return view('administration.roles')->withTitle('Ролі');
        }

    }

    public function select (Request $request) {

        $roles = Role::with('users')->get();

        foreach ($roles as $role) {

            $roles->find($role->id)->users_count = count($role->users);

        }

        $response = array(
            "draw" => microtime(),
            "recordsTotal" => count($roles),
            "recordsFiltered" => count($roles),
            "data" => $roles
        );

        return response()->json($response, 200);

    }
}
