<?php

namespace App\Http\Controllers\Administration\Status;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Status;
use App\Store;
use App\Essence;

use Gate;


class StatusController extends Controller
{
    
    public function show ($id, Request $request) {
        
        abort_unless(Gate::allows('status_view'), 403);

        $param = [
            'status' => (object) [
                'id' => 0
            ],
            'stores' => Store::get(),
            'essences' => Essence::get()
        ];

        $status = Status::find($id);

        if (is_null($status)) {

            $title = 'Створення статусу';

        } else {

            $title = 'Редагування статусу';

            $param['status'] = $status;

        }

        if (view()->exists('administration.status')) {
            return view('administration.status')->with($param)->withTitle($title);
        }

    }

    public function save ($id, Request $request) {
        
        if (Gate::allows('status_save')) {

            // Validate Data

            $errors = [];

            if (empty($request->name)) {
                array_push($errors, [
                    'message' => "Назва",
                    'input' => 'name'
                ]);
            }

            if (empty($request->essence_id)) {
                array_push($errors, [
                    'message' => "Сутність",
                    'input' => 'essence_id'
                ]);
            }

            if (count($errors) > 0) {

                return response()->json([
                    'errors' => $errors,
                    'message' => 'В магазині помилки'
                ], 406);

            }
            
            $status = Status::find($id);

            $response = (is_null($status)) ? $this->create($request) : $this->update($id, $request);

            if ($response->status() && $response->status() == 200) {
                $status = Status::find($response->getData()->id);
            } else {
                return $response;
            }

            return response()->json([
                'message' => 'Статус збережений',
                'id' => $status->id
            ], 200);
            
        }

    }

    public function create (Request $request) {

        $create_value = $request->only(['name', 'essence_id']);

        $status = new Status($create_value);
        $status->save();

        if ($status) {
            return response()->json(['id' => $status->id], 200);
        } else {
            return response()->json(['message' => 'Не получилось створити магазин'], 409);
        } 
        
    }

    public function update ($id, Request $request) {

        $update_value = $request->only(['name', 'essence_id']);

        $status = Status::find($id);

        $status->fill($update_value);

        $status->save();

        return response()->json(['id' => $status->id], 200);
        
    }

    public function delete ($id) {
        
        if (Gate::allows('status_delete')) {
            
            Status::find($id)->delete();

            return response()->json(200);
            
        } else {
        
            return response()->json(403);
        
        }   
        
    }

}
