<?php

namespace App\Http\Controllers\Administration\Status;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Status;

use Gate;


class StatusesController extends Controller
{
    
    public function show (Request $request) {
        
        abort_unless(Gate::allows('status_view'), 403);

        if (view()->exists('administration.statuses')) {
            return view('administration.statuses')->withTitle('Статуси');
        }

    }

    public function select (Request $request) {

        $statuses = Status::select(
            'statuses.*',
            'essence.name as essence_name'
        )
        ->leftJoin('essences as essence', 'statuses.essence_id', '=', 'essence.id')
        ->get();

        $response = array(
            "draw" => microtime(),
            "recordsTotal" => count($statuses),
            "recordsFiltered" => count($statuses),
            "data" => $statuses
        );

        return response()->json($response, 200);

    }

}
