<?php

namespace App\Http\Controllers\Administration\Payment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Payment;

use Gate;

class PaymentController extends Controller
{
    
    public function show ($id, Request $request) {
        
        abort_unless(Gate::allows('payment_view'), 403);

        $param = [
            'payment' => (object) [
                'id' => 0
            ]
        ];

        $payment = Payment::find($id);

        if (is_null($payment)) {

            $title = 'Створення оплати';

        } else {

            $title = 'Редагування оплати';

            $param['payment'] = $payment;

        }

        if (view()->exists('administration.payment')) {
            return view('administration.payment')->with($param)->withTitle($title);
        }

    }

    public function save ($id, Request $request) {
        
        if (Gate::allows('payment_save')) {

            // Validate Data

            $errors = [];

            if (empty($request->name)) {
                array_push($errors, [
                    'message' => "Назва",
                    'input' => 'name'
                ]);
            }

            if (count($errors) > 0) {

                return response()->json([
                    'errors' => $errors,
                    'message' => 'В оплаті помилки'
                ], 406);

            }
            
            $payment = Payment::find($id);

            $response = (is_null($payment)) ? $this->create($request) : $this->update($id, $request);

            if ($response->status() && $response->status() == 200) {
                $payment = Payment::find($response->getData()->id);
            } else {
                return $response;
            }

            return response()->json([
                'message' => 'Оплата збережений',
                'id' => $payment->id
            ], 200);
            
        }

    }

    public function create (Request $request) {

        $create_value = $request->only(['name']);

        $payment = new Payment($create_value);
        $payment->save();

        if ($payment) {
            return response()->json(['id' => $payment->id], 200);
        } else {
            return response()->json(['message' => 'Не получилось створити магазин'], 409);
        } 
        
    }

    public function update ($id, Request $request) {

        $update_value = $request->only(['name']);

        $payment = Payment::find($id);

        $payment->fill($update_value);

        $payment->save();

        return response()->json(['id' => $payment->id], 200);
        
    }

    public function delete ($id) {
        
        if (Gate::allows('payment_delete')) {
            
            Payment::find($id)->delete();

            return response()->json(200);
            
        } else {
        
            return response()->json(403);
        
        }   
        
    }

}
