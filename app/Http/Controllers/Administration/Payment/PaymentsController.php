<?php

namespace App\Http\Controllers\Administration\Payment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Payment;

use Gate;

class PaymentsController extends Controller
{
    
    public function show (Request $request) {
        
        abort_unless(Gate::allows('payment_view'), 403);

        if (view()->exists('administration.payments')) {
            return view('administration.payments')->withTitle('Оплати');
        }

    }

    public function select (Request $request) {

        $payments = Payment::get();

        $response = array(
            "draw" => microtime(),
            "recordsTotal" => count($payments),
            "recordsFiltered" => count($payments),
            "data" => $payments
        );

        return response()->json($response, 200);

    }

}
