<?php

namespace App\Http\Controllers\Administration\Delivery;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Delivery;

use Gate;
use DB;

class DeliveryController extends Controller
{
    
    public function show ($id, Request $request) {
        
        abort_unless(Gate::allows('delivery_view'), 403);

        $param = [
            'delivery' => (object) [
                'id' => 0
            ],
            'integrations' => DB::connection('shared_bd')->table('crm_integrations')->where('type_ref', 'delivery')->get()
        ];

        $delivery = Delivery::find($id);

        if (is_null($delivery)) {

            $title = 'Створення доставки';

        } else {

            $title = 'Редагування доставки';

            $param['delivery'] = $delivery;

        }

        if (view()->exists('administration.delivery')) {
            return view('administration.delivery')->with($param)->withTitle($title);
        }

    }

    public function save ($id, Request $request) {
        
        if (Gate::allows('delivery_save')) {

            // Validate Data

            $errors = [];

            if (empty($request->name)) {
                array_push($errors, [
                    'message' => "Назва",
                    'input' => 'name'
                ]);
            }

            if (count($errors) > 0) {

                return response()->json([
                    'errors' => $errors,
                    'message' => 'В магазині помилки'
                ], 406);

            }
            
            $delivery = Delivery::find($id);

            $response = (is_null($delivery)) ? $this->create($request) : $this->update($id, $request);

            if ($response->status() && $response->status() == 200) {
                $delivery = Delivery::find($response->getData()->id);
            } else {
                return $response;
            }

            return response()->json([
                'message' => 'Статус збережений',
                'id' => $delivery->id
            ], 200);
            
        }

    }

    public function create (Request $request) {

        $create_value = $request->only(['name', 'integration_id']);

        $delivery = new Delivery($create_value);
        $delivery->save();

        if ($delivery) {
            return response()->json(['id' => $delivery->id], 200);
        } else {
            return response()->json(['message' => 'Не получилось створити магазин'], 409);
        } 
        
    }

    public function update ($id, Request $request) {

        $update_value = $request->only(['name', 'integration_id']);

        $delivery = Delivery::find($id);

        $delivery->fill($update_value);

        $delivery->save();

        return response()->json(['id' => $delivery->id], 200);
        
    }

    public function delete ($id) {
        
        if (Gate::allows('delivery_delete')) {
            
            Delivery::find($id)->delete();

            return response()->json(200);
            
        } else {
        
            return response()->json(403);
        
        }   
        
    }

}
