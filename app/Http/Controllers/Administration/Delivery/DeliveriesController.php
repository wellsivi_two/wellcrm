<?php

namespace App\Http\Controllers\Administration\Delivery;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Delivery;

use Gate;

class DeliveriesController extends Controller
{
    
    public function show (Request $request) {
        
        abort_unless(Gate::allows('delivery_view'), 403);

        if (view()->exists('administration.deliveries')) {
            return view('administration.deliveries')->withTitle('Доставки');
        }

    }

    public function select (Request $request) {

        $deliveries = Delivery::get();

        $response = array(
            "draw" => microtime(),
            "recordsTotal" => count($deliveries),
            "recordsFiltered" => count($deliveries),
            "data" => $deliveries
        );

        return response()->json($response, 200);

    }

}
