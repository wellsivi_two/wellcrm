<?php

namespace App\Http\Controllers\Administration\Store;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Store;

use Gate;

class StoresController extends Controller
{
    
    public function show (Request $request) {
        
        abort_unless(Gate::allows('store_view'), 403);

        if (view()->exists('administration.store.stores')) {
            return view('administration.store.stores')->withTitle('Магазини');
        }

    }

    public function select (Request $request) {

        $stores = Store::get();

        $response = array(
            "draw" => microtime(),
            "recordsTotal" => count($stores),
            "recordsFiltered" => count($stores),
            "data" => $stores
        );

        return response()->json($response, 200);

    }

}
