<?php

namespace App\Http\Controllers\Administration\Store;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Store;

use App\Status;
use App\StoreStatus;

use App\Delivery;
use App\StoreDelivery;

use App\Payment;
use App\StorePayment;

use Gate;

class StoreController extends Controller
{
    
    public function show ($id, Request $request) {
        
        abort_unless(Gate::allows('store_view'), 403);

        $param = [
            'store' => (object) [
                'id' => 0
            ],
            'statuses' => Status::where('essence_id', 1)->get(),
            'deliveries' => Delivery::get(),
            'payments' => Payment::get()
        ];

        $store = Store::with(['statuses', 'deliveries', 'payments'])->find($id);

        if (is_null($store)) {

            $title = 'Створення магазину';

        } else {

            $title = 'Редагування магазину';

            $param['store'] = $store;

        }

        if (view()->exists('administration.store.store')) {
            return view('administration.store.store')->with($param)->withTitle($title);
        }

    }

    public function save ($id, Request $request) {
        
        if (Gate::allows('store_save')) {

            // Validate Data

            $errors = [];

            if (empty($request->name)) {
                array_push($errors, [
                    'message' => "Назва",
                    'input' => 'name'
                ]);
            }

            if (empty($request->statuses)) {
                array_push($errors, [
                    'message' => "Статуси",
                    'input' => 'statuses'
                ]);
            }

            if (empty($request->deliveries)) {
                array_push($errors, [
                    'message' => "Доставки",
                    'input' => 'deliveries'
                ]);
            }

            if (empty($request->payments)) {
                array_push($errors, [
                    'message' => "Оплати",
                    'input' => 'payments'
                ]);
            }

            if (count($errors) > 0) {

                return response()->json([
                    'errors' => $errors,
                    'message' => 'В магазині помилки'
                ], 406);

            }

            if (isset($request->orders_status_matrix)) {
                $request->orders_status_matrix = json_encode($request->orders_status_matrix, true);
            }

            if (empty($request->start_order_status_id)) {
                $request->start_order_status_id = 0;
            }

            $store = Store::find($id);

            $response = (is_null($store)) ? $this->create($request) : $this->update($id, $request);

            if ($response->status() && $response->status() == 200) {
                $store = Store::with(['statuses'])->find($response->getData()->id);
            } else {
                return $response;
            }

            // Save Statuses

            if (!is_array($request->statuses)) {
                $request->statuses = [
                    $request->statuses
                ];
            }

            $request_statuses = new Request($request->statuses);
            $statuses_response = $this->saveStatuses($store->id, $request_statuses);

            if ($statuses_response->status() && $statuses_response->status() != 200) {
                return $statuses_response;
            }

            // Save Delivery

            if (!is_array($request->deliveries)) {
                $request->deliveries = [
                    $request->deliveries
                ];
            }

            $request_deliveries = new Request($request->deliveries);
            $deliveries_response = $this->saveDeliveries($store->id, $request_deliveries);

            if ($deliveries_response->status() && $deliveries_response->status() != 200) {
                return $deliveries_response;
            }

            // Save Delivery

            if (!is_array($request->payments)) {
                $request->payments = [
                    $request->payments
                ];
            }

            $request_payments = new Request($request->payments);
            $response_payments = $this->savePayments($store->id, $request_payments);

            if ($response_payments->status() && $response_payments->status() != 200) {
                return $response_payments;
            }

            return response()->json([
                'message' => 'Магазин збережений',
                'id' => $store->id
            ], 200);
            
        }

    }

    public function create (Request $request) {

        $create_value = $request->only(['name']);

        $store = new Store($create_value);
        $store->save();

        if ($store) {
            return response()->json(['id' => $store->id], 200);
        } else {
            return response()->json(['message' => 'Не получилось створити магазин'], 409);
        } 
        
    }

    public function update ($id, Request $request) {

        $update_value = $request->only(['name', 'orders_status_matrix', 'start_order_status_id']);

        $store = Store::find($id);

        $store->fill($update_value);

        $store->save();

        return response()->json(['id' => $store->id], 200);
        
    }

    public function delete ($id) {
        
        if (Gate::allows('store_delete')) {
            
            Store::find($id)->delete();

            return response()->json(200);
            
        } else {
        
            return response()->json(403);
        
        }   
        
    }

    public function saveStatuses ($id, Request $request) {

        $store = Store::with(['statuses'])->find($id);

        $new_value = $request->all();
        $old_value = array_column($store->statuses->toArray(), 'id');

        // Insert statuses

        $insert = array_diff($new_value, $old_value);

        if (count($insert) > 0) {

            foreach ($new_value as $status) {

                if (in_array($status, $insert)) {
    
                    $new_status = new StoreStatus([
                        'store_id' => $store->id,
                        'status_id' => $status
                    ]);
    
                    $new_status->save();
    
                }
            }

        }

        // Update status

        $update = array_intersect($new_value, $old_value);

        if (count($update) > 0) {

            foreach ($new_value as $value) {

                if (in_array($value, $update)) {
    
                    // Future Code
    
                }
            }

        }

        // Delete product

        $delete = array_diff($old_value, $new_value);

        if (count($delete) > 0) {
            StoreStatus::where('store_id', $store->id)->whereIn('status_id', $delete)->delete();
        }

        return response()->json(200);
        
    }

    public function saveDeliveries ($id, Request $request) {

        $store = Store::with(['deliveries'])->find($id);

        $new_value = $request->all();
        $old_value = array_column($store->deliveries->toArray(), 'id');

        // Insert statuses

        $insert = array_diff($new_value, $old_value);

        if (count($insert) > 0) {

            foreach ($new_value as $value) {

                if (in_array($value, $insert)) {
    
                    $new_option = new StoreDelivery([
                        'store_id' => $store->id,
                        'delivery_id' => $value
                    ]);
    
                    $new_option->save();
    
                }
            }

        }

        // Update status

        $update = array_intersect($new_value, $old_value);

        if (count($update) > 0) {

            foreach ($new_value as $value) {

                if (in_array($value, $update)) {
    
                    // Future Code
    
                }
            }

        }

        // Delete product

        $delete = array_diff($old_value, $new_value);

        if (count($delete) > 0) {
            StoreDelivery::where('store_id', $store->id)->whereIn('delivery_id', $delete)->delete();
        }

        return response()->json(200);
        
    }

    public function savePayments ($id, Request $request) {

        $store = Store::with('payments')->find($id);

        $new_value = $request->all();
        $old_value = array_column($store->payments->toArray(), 'id');

        // Insert statuses

        $insert = array_diff($new_value, $old_value);

        if (count($insert) > 0) {

            foreach ($new_value as $value) {

                if (in_array($value, $insert)) {
    
                    $new_option = new StorePayment([
                        'store_id' => $store->id,
                        'payment_id' => $value
                    ]);
    
                    $new_option->save();
    
                }
            }

        }

        // Update status

        $update = array_intersect($new_value, $old_value);

        if (count($update) > 0) {

            foreach ($new_value as $value) {

                if (in_array($value, $update)) {
    
                    // Future Code
    
                }
            }

        }

        // Delete product

        $delete = array_diff($old_value, $new_value);

        if (count($delete) > 0) {
            StorePayment::where('store_id', $store->id)->whereIn('payment_id', $delete)->delete();
        }

        return response()->json(200);
        
    }

    public function renderOrdersStatusMatrix ($store_id) {

        $store = Store::with(['statuses'])->find($store_id);

        $orders_status_matrix = json_decode($store->orders_status_matrix, true);

        $statuses = [];

        foreach ($store->statuses as $key => $values) {

            if (isset($orders_status_matrix[$values->id])) {

                $json_value = $orders_status_matrix[$values->id];

                if (is_string($json_value)) {
                    $json_value = [$json_value];
                }

                $values->selected = $json_value;

            } else {

                $values->selected = [];

            }

            if ($values->id == $store->start_order_status_id) {
                $values->start_status = true;
            } else {
                $values->start_status = false;
            }

            array_push($statuses, $values);

        }

        $orders_statuses_matrix = view('administration.store.orders_statuses_matrix')->with([
            'statuses' => $statuses
        ])->render();

        return response()->json([
            'orders_statuses_matrix' => $orders_statuses_matrix
        ], 200);

    }

}
