<?php

namespace App\Http\Controllers\Storage\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Product;
use Gate;

class ProductsController extends Controller
{
    public function show (Request $request) {
        
        abort_unless(Gate::allows('product_view'), 403);

        if (view()->exists('storage.products')) {
            return view('storage.products')->withTitle('Товари');
        }

    }
    
    public function select (Request $request) {

        $products = Product::get();

        $response = array(
            "draw" => microtime(),
            "recordsTotal" => count($products),
            "recordsFiltered" => count($products),
            "data" => $products
        );

        return response()->json($response, 200);

    }
}
