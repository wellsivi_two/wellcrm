<?php

namespace App\Http\Controllers\Storage\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Product;
use App\Store;
use Gate;
use DB;

class ProductController extends Controller
{
    public function show ($id, Request $request) {
        
        abort_unless(Gate::allows('product_view'), 403);

        $param = [
            'product' => (object) [
                'id' => 0
            ],
            'stores' => Store::get()
        ];

        $product = Product::find($id);

        if (is_null($product)) {

            $title = 'Створення товару';

        } else {

            $title = 'Редагування товару';

            $param['product'] = $product;

        }

        if (view()->exists('storage.product')) {
            return view('storage.product')->with($param)->withTitle($title);
        }

    }
    
    public function save ($id, Request $request) {
        
        if (Gate::allows('product_save')) {

             // Validate Data

            $errors = [];

            if (empty($request->store_id)) {
                array_push($errors, [
                    'message' => "Магазин",
                    'input' => 'store_id'
                ]);
            }

            if (count($errors) > 0) {

                return response()->json([
                    'errors' => $errors,
                    'message' => 'В продукті помилки'
                ], 406);

            }
            
            $product = Product::find($id);

            $response = (is_null($product)) ? $this->create($request) : $this->update($id, $request);

            if ($response->status() && $response->status() == 200) {
                $product = Product::find($response->getData()->id);
            } else {
                return $response;
            }

            return response()->json([
                'message' => 'Товар збережений',
                'id' => $product->id
            ], 200);
            
        }

    }
    
    public function create (Request $request) {

        $create_value = $request->only(['article', 'name', 'cost', 'price', 'store_id']);

        $check_duplicate = Product::where('article', $create_value['article'])->first();

        if (!isset($check_duplicate)) {

            $product = new Product($create_value);
            $product->save();
            
            return response()->json(['id' => $product->id], 200);

        } else {
            return response()->json(['message' => 'Товар з таким артикулом вже існує в системі'], 409);
        } 
        
    }

    public function update ($id, Request $request) {

        $update_value = $request->only(['article', 'name', 'cost', 'price', 'store_id']);

        $product = Product::find($id);

        $product->fill($update_value);

        $product->save();

        return response()->json(['id' => $product->id], 200);
        
    }

    public function complete (Request $request) {
        
        $search = $request->get('term');

        $result = Product::select(
            '*',
            DB::raw('CONCAT(products.id, " - ", products.name, " (", products.article, ")") as value'),
            DB::raw('CONCAT(products.id, " - ", products.name, " (", products.article, ")") as label')
        )->where('id', 'LIKE', '%'. $search. '%');

        $store_id = $request->get('store_id');

        if (isset($store_id) && !empty($store_id)) {
            $result->where('store_id', $store_id);
        }

        return response()->json($result->get());
        
    }
    
    public function delete ($id) {
        
        if (Gate::allows('product_delete')) {
            
            Product::find($id)->delete();

            return response()->json(200);
            
        } else {
        
            return response()->json(403);
        
        }   
        
    }
    
}
