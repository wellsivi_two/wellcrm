<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use Auth;

use App\User;

class LoginController extends Controller
{
    public function show (Request $request) {

        if (view()->exists('auth.login')) {
            return view('auth.login')->withTitle('Авторизація');
        }

    }

    public function login (Request $request) {

        $data = $request->all();

        if (isset($data['remember'])) {
            $remember = true;
        } else {
            $remember = false;
        }

        $search_account = User::where('email', $data['email'])->first();

        if (!is_null($search_account)) {

            if (Hash::check($data['password'], $search_account->password)) {

                $authorization = Auth::attempt([
                    'email'     => $data['email'],
                    'password'  => $data['password']
                ], $remember);
        
                if ($authorization) {
                    return response()->json([
                        'auth' => $authorization,
                        'intended' => '/'
                    ], 200);
                } else {
                    return response()->json(['message' => 'Збій в системі, зверніться до розробника'], 409);
                }

            } else {
                return response()->json(['message' => 'Паролі не співпадають'], 409);
            }

        } else {
            return response()->json(['message' => 'Акаунт з таким логіном не найдено'], 409);
        }

    }

    public function logout (Request $request) {

        Auth::logout();

        $request->session()->invalidate();

    }
}
