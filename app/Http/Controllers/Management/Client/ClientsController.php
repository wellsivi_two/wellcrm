<?php

namespace App\Http\Controllers\Management\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Client;
use Gate;


class ClientsController extends Controller
{
    
    public function show (Request $request) {
        
        abort_unless(Gate::allows('client_view'), 403);

        if (view()->exists('management.clients')) {
            return view('management.clients')->withTitle('Клієнти');
        }

    }
    
    public function select (Request $request) {

        $clients = Client::get();

        $response = array(
            "draw" => microtime(),
            "recordsTotal" => count($clients),
            "recordsFiltered" => count($clients),
            "data" => $clients
        );

        return response()->json($response, 200);

    }
    
}
