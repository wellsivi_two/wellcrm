<?php

namespace App\Http\Controllers\Management\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Client;
use App\ClientPhone;
use App\ClientEmail;
use Gate;
use DB;

class ClientController extends Controller
{
    public function show ($id, Request $request) {
        
        abort_unless(Gate::allows('client_view'), 403);

        $param = [
            'client' => (object) [
                'id' => 0
            ]
        ];

        $client = Client::with(['emails', 'phones'])->find($id);

        if (is_null($client)) {

            $title = 'Створення клієнта';

        } else {

            $title = 'Редагування клієнта';

            $param['client'] = $client;

        }

        if (view()->exists('management.client')) {
            return view('management.client')->with($param)->withTitle($title);
        }

    }
    
    public function save ($id, Request $request) {
        
        if (Gate::allows('client_save')) {

            // Verification Data

            $errors = [];

            if (empty($request->phone)) {
                array_push($errors, [
                    'message' => "Телефон",
                    'input' => 'new_phone'
                ]);
            }

            if (empty($request->email)) {
                array_push($errors, [
                    'message' => "Email",
                    'input' => 'new_email'
                ]);
            }

            if (empty($request->name)) {
                array_push($errors, [
                    'message' => "Ім'я",
                    'input' => 'name'
                ]);
            }

            if (empty($request->surname)) {
                array_push($errors, [
                    'message' => "Прізвище",
                    'input' => 'surname'
                ]);
            }

            if (!in_array($request->sex, ['MAN', 'WOMAN'])) {
                array_push($errors, [
                    'message' => "Стать",
                    'input' => 'sex'
                ]);
            }

            if (count($errors) > 0) {

                return response()->json([
                    'errors' => $errors,
                    'message' => 'В клієнті помилки'
                ], 406);

            }

            // Phones Data

            $phones_data = $this->differenceOldAndNewPhones($id, $request);

            if (count($phones_data['phones_for_add']) > 0) {

                $search_phones = ClientPhone::whereIn('phone', $phones_data['phones_for_add'])->first();

                if (is_object($search_phones)) {
        
                    return response()->json(['message' => 'Номер телефону (' . $search_phones->phone . ') належить іншому користувачу. <a href="/client/' . $search_phones->client_id . '">Переглянути користувача</a>'], 409);
        
                }

            }

            // Emails Data

            $emails_data = $this->differenceOldAndNewEmails($id, $request);

            if (count($emails_data['emails_for_add']) > 0) {

                $search_emails = ClientEmail::whereIn('email', $emails_data['emails_for_add'])->first();

                if (is_object($search_emails)) {
        
                    return response()->json(['message' => 'Номер телефону (' . $search_emails->email . ') належить іншому користувачу. <a href="/client/' . $search_emails->client_id . '">Переглянути користувача</a>'], 409);
        
                }

            }

            // Save client

            $client = Client::find($id);

            $response = (is_null($client)) ? $this->create($request) : $this->update($id, $request);

            if ($response->status() && $response->status() == 200) {
                $client = Client::find($response->getData()->id);
            } else {
                return $response;
            }

            // Save phones

            if ($phones_data['phones_for_add'] > 0) {

                foreach ($phones_data['phones_for_add'] as $phone) {

                    $phone = new ClientPhone([
                        'client_id' => $client->id,
                        'phone' => $phone
                    ]);
    
                    $phone->save();

                }

            }

            if ($phones_data['phones_for_remove'] > 0) {
                ClientPhone::whereIn('phone', $phones_data['phones_for_remove'])->delete();
            }

            // Save emails

            if ($emails_data['emails_for_add'] > 0) {

                foreach ($emails_data['emails_for_add'] as $email) {

                    $email = new ClientEmail([
                        'client_id' => $client->id,
                        'email' => $email
                    ]);
    
                    $email->save();

                }

            }

            if ($emails_data['emails_for_remove'] > 0) {
                ClientEmail::whereIn('email', $emails_data['emails_for_remove'])->delete();
            }

            return response()->json([
                'message' => 'Клієнт збережений',
                'id' => $client->id
            ], 200);
            
        }

    }
    
    public function create (Request $request) {

        $create_value = $request->only(['name', 'surname', 'sex']);

        $client = new Client($create_value);
        $client->save();
        
        return response()->json(['id' => $client->id], 200);
        
    }

    public function update ($id, Request $request) {

        $update_value = $request->only(['name', 'surname', 'sex']);

        $client = Client::find($id);

        $client->fill($update_value);

        $client->save();

        return response()->json(['id' => $client->id], 200);
        
    }

    public function delete ($id) {
        
        if (Gate::allows('user_delete')) {
            
            Client::find($id)->delete();

            return response()->json(200);
            
        } else {
        
            return response()->json(403);
        
        }   
        
    }

    public function differenceOldAndNewPhones ($id, Request $request) {

        $new_phones = $request->phone;

        if (is_string($new_phones)) {
            
            $new_phones = [
                $request->phone
            ];

        }

        foreach ($new_phones as $key => $phone) {

            $new_phones[$key] = intval(preg_replace("/[^0-9]/", '', $phone));

        }

        $client = Client::with('phones')->find($id);

        if (is_object($client)) {

            $old_phones = array_column($client->phones->toArray(), 'phone');

            $phones_for_add = array_diff($new_phones, $old_phones);

            $phones_for_remove = array_diff($old_phones, $new_phones);


        } else {

            $phones_for_add = $new_phones;

            $phones_for_remove = [];

        }

        return [
            'phones_for_add' => $phones_for_add,
            'phones_for_remove' => $phones_for_remove
        ];

    }

    public function differenceOldAndNewEmails ($id, Request $request) {

        $new_emails = $request->email;

        if (is_string($new_emails)) {
            
            $new_emails = [
                $request->email
            ];

        }

        $client = Client::with('emails')->find($id);

        if (is_object($client)) {

            $old_emails = array_column($client->emails->toArray(), 'email');

            $emails_for_add = array_diff($new_emails, $old_emails);

            $emails_for_remove = array_diff($old_emails, $new_emails);


        } else {

            $emails_for_add = $new_emails;

            $emails_for_remove = [];

        }

        return [
            'emails_for_add' => $emails_for_add,
            'emails_for_remove' => $emails_for_remove
        ];

    }

    public function completeByPhone (Request $request) {
        
        $search = $request->get('term');

        $clients = Client::with('phones', 'emails')->whereHas("phones", function($q) use ($search){ 
            $q->where("phone", 'LIKE','%' . $search . '%'); 
        })->limit(5)->get();

        foreach ($clients as $key => $value) {

            $phones_array = array_column($value->phones->toArray(), 'phone');

            $phones_string = implode(', ', $phones_array);
            
            $clients[$key]['value'] = $value->id . ' - ' . $value->name . ' ' . $value->surname . ' (' . $phones_string . ')';
            $clients[$key]['label'] = $value->id . ' - ' . $value->name . ' ' . $value->surname . ' (' . $phones_string . ')';

        }

        return response()->json($clients);
        
    }

    public function completeByEmail (Request $request) {
        
        $search = $request->get('term');

        $clients = Client::with('phones', 'emails')->whereHas("emails", function($q) use ($search){ 
            $q->where("email", 'LIKE','%' . $search . '%'); 
        })->limit(5)->get();

        foreach ($clients as $key => $value) {

            $emails_array = array_column($value->emails->toArray(), 'email');

            $emails_string = implode(', ', $emails_array);
            
            $clients[$key]['value'] = $value->id . ' - ' . $value->name . ' ' . $value->surname . ' (' . $emails_string . ')';
            $clients[$key]['label'] = $value->id . ' - ' . $value->name . ' ' . $value->surname . ' (' . $emails_string . ')';

        }

        return response()->json($clients);
        
    }

}
