<?php

namespace App\Http\Controllers\Management\Order;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Management\Client\ClientController as ClientController;
use App\Http\Controllers\Store\Integration\NovaPoshtaTtnController as NovaPoshtaTtnController;
use Illuminate\Http\Request;

use App\Order;
use App\User;
use App\Client;
use App\Store;
use App\Delivery;
use App\Integration;
use App\OrderProduct;
use App\NovaPoshtaAccount;
use App\NovaPoshtaTtn;

use Gate;
use DB;

class OrderController extends Controller
{
    
    public function show ($id, Request $request) {
        
        abort_unless(Gate::allows('order_view'), 403);

        $param = [
            'order' => (object) [
                'id' => 0,
                'client' => (object) [
                    'id' => 0
                ]
            ],
            'stores' => Store::with(['statuses', 'deliveries', 'payments'])->get()->keyBy('id')
        ];

        $order = Order::with(['client', 'products'])->find($id);

        if (is_null($order)) {

            $title = 'Створення замовлення';

            $param['available_statuses'] = [];

        } else {

            $title = 'Редагування замовлення';

            $param['order'] = $order;

            $store = Store::with(['statuses', 'deliveries', 'payments'])->find($order->store_id);

            $orders_status_matrix = json_decode($store->orders_status_matrix, true);

            $available_statuses = $orders_status_matrix[$order->status_id];

            if (is_string($available_statuses)) {
                $available_statuses = [$available_statuses];
            }

            $param['store'] = $store;
            $param['available_statuses'] = $available_statuses;

        }

        if (view()->exists('management.order.order')) {
            return view('management.order.order')->with($param)->withTitle($title);
        }

    }

    public function save ($id, Request $request) {
        
        abort_unless(Gate::allows('order_save'), 403);

        // Validate Data

        $errors = [];

        if ($request->store_id == 0) {
            array_push($errors, [
                'message' => "Магазин",
                'input' => 'store_id'
            ]);
        }

        if ($request->status_id == 0) {
            array_push($errors, [
                'message' => "Статус",
                'input' => 'status_id'
            ]);
        }

        if ($request->delivery_id == 0) {
            array_push($errors, [
                'message' => "Доставка",
                'input' => 'delivery_id'
            ]);
        }

        if ($request->payment_id == 0) {
            array_push($errors, [
                'message' => "Оплата",
                'input' => 'payment_id'
            ]);
        }

        if (floatval($request->total) == 0) {
            array_push($errors, [
                'message' => "Сума замовлення",
                'input' => 'total'
            ]);
        }

        if (count($errors) > 0) {

            return response()->json([
                'errors' => $errors,
                'message' => 'В замовленні помилки'
            ], 406);

        }

        // Save Client

        $request_client = new Request($request->client);
        $client_controller = new ClientController();

        $client_response = $client_controller->save($request->client_id, $request_client);

        if ($client_response->status() && $client_response->status() != 200) {
            return $client_response;
        }

        $request->merge([
            'client_id' => $client_response->getData()->id
        ]);

        // Save Order

        $order = Order::find($id);

        $order_response = (is_null($order)) ? $this->create($request) : $this->update($id, $request);

        if ($order_response->status() && $order_response->status() == 200) {
            $order = Order::with(['client', 'products'])->find($order_response->getData()->id);
        } else {
            return $order_response;
        }

        // Save Products

        $request_products = new Request($request->products);
        $products_response = $this->saveProducts($order->id, $request_products);

        if ($products_response->status() && $products_response->status() != 200) {
            return $products_response;
        }

        // Save NovaposhtaTtn

        if (isset($request->novaposhta)) {
            $request_novaposhta = new Request($request->novaposhta);
            $novaposhta_controller = new NovaPoshtaTtnController();

            $novaposhta_response = $novaposhta_controller->save('order', $order->id, $request_novaposhta);

            if ($novaposhta_response->status() && $novaposhta_response->status() != 200) {
                return $novaposhta_response;
            }
        }

        return response()->json([
            'id' => $order->id,
            'message' => 'Замовлення збережено!'
        ], 200);

    }

    public function saveProducts ($id, Request $request) {

        $order = Order::with(['client', 'products'])->find($id);

        $new_products_value = array_column($request->all(), 'id');
        $old_products_value = array_column($order->products->toArray(), 'id');

        // Insert product

        $insert = array_diff($new_products_value, $old_products_value);

        if (count($insert) > 0) {

            foreach ($request->all() as $product) {

                if (in_array($product['id'], $insert)) {
    
                    $new_product = new OrderProduct([
                        'order_id' => $order->id,
                        'product_id' => $product['id'],
                        'price' => $product['price'],
                        'discount' => $product['discount'],
                        'amount' => $product['amount'],
                        'sum' => $product['sum']
                    ]);
    
                    $new_product->save();
    
                }
            }

        }

        // Update product

        $update = array_intersect($new_products_value, $old_products_value);

        if (count($update) > 0) {

            foreach ($request->all() as $product) {

                if (in_array($product['id'], $update)) {
    
                    OrderProduct::where([
                        'product_id' => $product['id'],
                        'order_id' => $order->id
                    ])->update([
                        'price' => $product['price'],
                        'discount' => $product['discount'],
                        'amount' => $product['amount'],
                        'sum' => $product['sum']
                    ]);
    
                }
            }

        }

        // Delete product

        $delete = array_diff($old_products_value, $new_products_value);

        if (count($delete) > 0) {
            OrderProduct::whereIn('id', $delete)->delete();
        }

        return response()->json(200);
        
    }

    public function create (Request $request) {

        $create_value = $request->only(['client_id', 'store_id', 'status_id', 'delivery_id', 'payment_id', 'discount', 'total']);

        $order = new Order($create_value);
        $order->save();
        
        return response()->json(['id' => $order->id], 200);
        
    }

    public function update ($id, Request $request) {

        $update_value = $request->only(['client_id', 'store_id', 'status_id', 'delivery_id', 'payment_id', 'discount', 'total']);

        $order = Order::find($id);

        $order->fill($update_value);

        $order->save();

        return response()->json(['id' => $order->id], 200);
        
    }

    public function delete ($id) {
        
        if (Gate::allows('order_delete')) {
            
            Order::find($id)->delete();

            return response()->json(200);
            
        } else {
        
            return response()->json(403);
        
        }   
        
    }

    public function changeStatus ($id, Request $request) {

        $store_id = $request->get('store_id');

        $create = false;

        $order = Order::find($id);
        if (is_null($order)) $create = true;

        $store = Store::with('statuses')->find($store_id);

        $orders_status_matrix = json_decode($store->orders_status_matrix, true);

        $statuses = [];

        foreach ($store->statuses as $status) {

            if ($create) {

                if ($status->id == $store->start_order_status_id) {
                    array_push($statuses, $status);
                }

            } else {

                $available_statuses  = $orders_status_matrix[$order->status_id];

                if (is_string($available_statuses)) {
                    $available_statuses = [$available_statuses];
                }

                if (in_array($status->id, $available_statuses)) {
                    array_push($statuses, $status);
                }

            }

        }

        return response()->json(['statuses' => $statuses], 200);

    }

    public function changeDelivery ($id, Request $request) {
        
        abort_unless(Gate::allows('order_save'), 403);

        $delivery = Delivery::find($request->delivery_id);

        if (!empty($delivery->integration_id)) {

            $delivery->integration = Integration::find($delivery->integration_id);

            if ($delivery->integration->ref == 'novaposhta') {

                $delivery->integration->form = $this->renderNovaPoshtaForm($id);

            }

        }

        return response()->json($delivery, 200);

    }

    public function renderNovaPoshtaForm ($order_id) {

        $novaposhta_ttn = NovaPoshtaTtn::where([
            'model' => 'order',
            'model_id' => $order_id
        ])->first();

        return view('layouts.forms.novaposhta')->with([
            'novaposhta_ttn' => $novaposhta_ttn,
            'novaposhta_accounts' => NovaPoshtaAccount::where('active', 1)->get(),
            'novaposhta_payments' => DB::connection('shared_bd')->table('novaposhta_payments')->get(),
            'novaposhta_services' => DB::connection('shared_bd')->table('novaposhta_services')->get(),
            'novaposhta_delivery_cargos' => DB::connection('shared_bd')->table('novaposhta_delivery_cargos')->get(),
            'novaposhta_delivery_payers' => DB::connection('shared_bd')->table('novaposhta_delivery_payers')->get(),
            'novaposhta_redelivery_cargos' => DB::connection('shared_bd')->table('novaposhta_redelivery_cargos')->get(),
            'novaposhta_redelivery_payers' => DB::connection('shared_bd')->table('novaposhta_redelivery_payers')->get()
        ])->render();

    }

}
