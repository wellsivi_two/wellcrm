<?php

namespace App\Http\Controllers\Management\Order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Order;
use Gate;
use DB;

class OrdersController extends Controller
{

    public function show (Request $request) {
        
        abort_unless(Gate::allows('order_view'), 403);

        if (view()->exists('management.order.orders')) {
            return view('management.order.orders')->withTitle('Замовлення');
        }

    }
    
    public function select (Request $request) {

        $orders = Order::select(
            'orders.*',
            DB::raw("CONCAT(clients.name, ' ', clients.surname) as client_fio")
        )
        ->leftJoin('clients', 'orders.client_id', '=', 'clients.id')
        ->get();

        $response = array(
            "draw" => microtime(),
            "recordsTotal" => count($orders),
            "recordsFiltered" => count($orders),
            "data" => $orders
        );

        return response()->json($response, 200);

    }

}
