<?php

namespace App\Http\Middleware;

use App\Role;
use App\User;
use Closure;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;

class AuthGates
{

    public function handle($request, Closure $next)
    {

        $user = Auth::user();

        if (!app()->runningInConsole() && $user) {

            $roles = Role::with('permissions')->get();

            $permissionsArray = [];

            foreach ($roles as $role) {

                foreach ($role->permissions as $permissions) {

                    $name = $permissions->element . '_' . $permissions->action;

                    $permissionsArray[$name][] = $role->id;

                }

            }

            foreach ($permissionsArray as $title => $roles) {

                Gate::define($title, function ($user) use ($roles) {

                    return count(array_intersect($user->roles->pluck('id')->toArray(), $roles));

                });

            }

        }

        return $next($request);

    }

}
