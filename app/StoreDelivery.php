<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreDelivery extends Model
{
    
    protected $table = 'stores_deliveries';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'id',
        'store_id',
        'delivery_id'
    ];

}
