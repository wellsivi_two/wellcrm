<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StorePayment extends Model
{
    
    protected $table = 'stores_payments';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'id',
        'store_id',
        'payment_id'
    ];

}
