<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientPhone extends Model
{
    protected $table = 'clients_phones';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'id', 'client_id', 'phone'
    ];
}
