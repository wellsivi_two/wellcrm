<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NovaPoshtaTtn extends Model
{
    use SoftDeletes;

    protected $table = 'novaposhta_ttns';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'id',

        'model',
        'model_id',
        'novaposhta_account_id',

        'cost',
        'weight',

        'length',
        'width',
        'height',
        'volumetric_weight',

        'last_name',
        'first_name',
        'middle_name',
        'phone',

        'delivery_service',
        'delivery_payer',
        'delivery_payment',
        'delivery_cargo',

        'delivery_description',
        'delivery_description_Ref',

        'area',
        'area_Ref',

        'city',
        'city_Ref',

        'warehouse',
        'warehouse_Ref',

        'redelivery_cargo',
        'redelivery_additional_string',
        'redelivery_payer',

        'created',
        'number',
        'ref',
        'status_id',
        'status_name',

        'create_at',
        'update_at',
    ];
}
