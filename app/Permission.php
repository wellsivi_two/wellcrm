<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = 'permissions';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    public function roles () {
        return $this->belongsToMany('App\Role', 'roles_permissions', 'role_id', 'permission_id');
    }
}
