<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Essence extends Model
{

    protected $table = 'essences';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;

    protected $fillable = [
        'id', 'name', 'settings'
    ];

}
