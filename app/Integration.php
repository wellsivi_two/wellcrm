<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Integration extends Model
{

    protected $table = 'crm_integrations';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;
    protected $connection = 'shared_bd';

    protected $fillable = [
        'id', 'key', 'name', 'type_key', 'type_name', 'title', 'description', 'settings', 'price', 'active'
    ];

}
