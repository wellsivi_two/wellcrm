@extends('layouts.template')

@section('title')
{{ $title }}
@endsection

@section('style')

@endsection


@section('content')

<div class="row">
    <div class="col-md-6">

        <div class="card">
           <form id="product_form">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Продукт</h5>
            </div>
            <div class="card-body">
                    <fieldset class="mb-3">
                        <legend class="text-uppercase font-size-sm font-weight-bold">Базова інформація</legend>
                        
                        <div class="row">
                          
                            <div class="col-6">
                                <div class="form-group form-group-float">
									<label class="form-group-float-label is-visible">Артикул</label>
									<input type="text" class="form-control" name="article" placeholder="Артикул" value="{{ optional($product)->article }}">
								</div>
                            </div>
                           
                            <div class="col-6">
                                <div class="form-group form-group-float is-success">
									<label class="form-group-float-label is-visible">Назва</label>
									<input type="text" class="form-control" name="name" placeholder="Назва" value="{{ optional($product)->name }}">
								</div>
                            </div>
                            
                            <div class="col-6">
                                <div class="form-group form-group-float">
									<label class="form-group-float-label is-visible">Собівартість</label>
									<input type="number" class="form-control" name="cost" placeholder="Собівартість" value="{{ optional($product)->cost }}">
								</div>
                            </div>
                            
                            <div class="col-6">
                                <div class="form-group form-group-float">
									<label class="form-group-float-label is-visible">Ціна</label>
									<input type="number" class="form-control" name="price" placeholder="Ціна" value="{{ optional($product)->price }}">
								</div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group form-group-float">
									<label class="form-group-float-label is-visible">Магазин</label>
									<select class="form-control select" name="store_id" data-fouc>
                                        <option value="0">Не вибрано</option>
                                        @foreach ($stores as $store)
                                            <option value="{{ $store->id }}">{{ $store->name}}</option>
                                        @endforeach
									</select>
								</div>
                            </div>
                            
                        </div>
                    </fieldset>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('footer')

    @if (Auth::user()->can('product_save') or Auth::user()->can('product_delete'))

        <div class="navbar navbar-expand-lg navbar-light fixed-bottom">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Панель управління
                </button>
            </div>

            <div class="navbar-collapse collapse p-1" id="navbar-footer">
                <ul class="navbar-nav ml-lg-auto">
                    @can('product_save')
                    <li class="nav-item">
                        <button type="submit" id="save_product" product_id="{{ $product->id }}" form="product_form" class="btn btn-outline alpha-success text-success-800 legitRipple"><i class="icon-floppy-disk mr-2"></i>Зберегти</button>
                    </li>
                    @endcan
                    @can('product_delete')
                    <li class="nav-item">
                        <button id="delete_product" product_id="{{ $product->id }}" class="btn btn-outline alpha-danger text-danger-800 legitRipple"><i class="icon-bin mr-2"></i>Видали</button>
                    </li>
                    @endcan
                </ul>
            </div>
        </div>

    @endif

@endsection

@section('scripts')
<script src="{{ URL::asset('global_assets/js/plugins/forms/validation/validate.min.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/notifications/pnotify.min.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>

<script>
    var product = {!! json_encode($product) !!},
        stores = {!! json_encode($stores) !!};
</script>

<script src="{{ URL::asset('assets/js/content/storage/product.js') }}"></script>

@can('user_edit_roles')
<script type="application/javascript">
    $('select[name="roles"]').prop('disabled', false);
</script>
@endcan

@endsection