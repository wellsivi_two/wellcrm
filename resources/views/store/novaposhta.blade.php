@extends('layouts.template')

@section('title')
{{ $title }}
@endsection

@section('style')

@endsection


@section('content')

<div class="row">

    <div class="col-md-12">

        <div class="card">
           <form id="user_form">
                <div class="card-header bg-white header-elements-sm-inline">
                    <h5 class="card-title">Налаштування</h5>
                    <div class="header-elements">
                        <button id="save_integration" class="btn btn-outline alpha-success text-success-800 legitRipple">Зберегти</button>
                        <button id="update_novaposhta_data" class="btn btn-outline alpha-blue text-blue-800 legitRipple">Імпортувати дані</button>
                    </div>
                </div>
            </form>
        </div>

        <div class="card">
           <form id="user_form">
                <div class="card-header bg-white header-elements-sm-inline">
                    <h5 class="card-title">Контакти для відправки</h5>
                    <div class="header-elements">
                        <div class="form-group mb-0">
                            <input type="text" class="form-control wmin-300" name="search_account" placeholder="API ключ">
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table" id="accounts_table">
                        <thead>
                            <tr>
                                <th>Ключ</th>
                                <th>Контактне лице</th>
                                <th>Номер телефону</th>
                                <th>Населений пункт</th>
                                <th>Відділення</th>
                                <th>Ліміт на місяць</th>
                                <th>Активний</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($novaposhta->accounts as $account)

                                <tr class="novaposhta_account" key="{{ $account->Key }}" sender_Ref="{{ $account->sender_Ref }}" contact_Ref="{{ $account->contact_Ref }}">
                                    <td>{{ $account->Key }}</td>
                                    <td>{{ $account->Description }}</td>
                                    <td>{{ $account->Phones }}</td>
                                    <td><input type="text" name="city" class="form-control" Ref="{{ $account->city_Ref }}" placeholder="Місто" value="{{ $account->city }}"></td>
                                    <td><input type="text" name="warehouse" class="form-control" Ref="{{ $account->warehouse_Ref }}" placeholder="Відділення" value="{{ $account->warehouse }}"></td>
                                    <td><input type="number" name="month_limit" class="form-control" value="{{ $account->month_limit }}"></td>
                                    <td><input type="checkbox" name="active" class="form-check-input-styled" {{ $account->active ? 'checked' : '' }} data-fouc></td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </form>
        </div>

    </div>

    <div class="col-md-4">
        <div class="card">
           <form id="store_form">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Статуси </h5>
                </div>
                <div class="table-responsive">
                    <table class="table" id="statuses_table">
                        <thead>
                            <tr>
                                <th>Статус</th>
                                <th>Списувати ліміти</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($novaposhta->statuses as $status)

                                <tr class="status" StateId="{{ $status->StateId }}">
                                    <td>{{ $status->StateName }}</td>
                                    <td><input type="checkbox" name="limit_debiting" class="form-check-input-styled" {{ $status->limit_debiting ? 'checked' : '' }} data-fouc></td>
                                </tr>

                            @endforeach

                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    </div>

</div>

@endsection

@section('scripts')

<script src="{{ URL::asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/notifications/pnotify.min.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/extensions/jquery_ui/widgets.min.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>

<script>
    var novaposhta = {!! json_encode($novaposhta) !!};
</script>

<script src="{{ URL::asset('assets/js/content/store/novaposhta.js') }}"></script>

@endsection