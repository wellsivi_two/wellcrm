@extends('layouts.template')

@section('title')
{{ $title }}
@endsection

@section('style')

@endsection


@section('content')

<div class="w-100 overflow-auto order-2 order-md-1">

    <!-- List -->

    @foreach ($integrations as $integration)

    <div class="card card-body">
        <div class="media align-items-center align-items-lg-start text-center text-lg-left flex-column flex-lg-row">
            <div class="mr-lg-3 mb-3 mb-lg-0">
                <a href="/integration/{{ $integration->ref }}" data-popup="lightbox">
                    <img src="{{ URL::asset('global_assets/images/backgrounds/boxed_bg_retina.png') }}" width="96" alt="">
                </a>
            </div>

            <div class="media-body">
                <h6 class="media-title font-weight-semibold">
                    <a href="/integration/{{ $integration->ref }}">{{ $integration->title }}</a>
                </h6>

                <ul class="list-inline list-inline-dotted mb-3 mb-lg-2">
                    <li class="list-inline-item"><a href="#" class="text-muted">{{ $integration->type_name }}</a></li>
                    <li class="list-inline-item"><a href="#" class="text-muted">{{ $integration->name }}</a></li>
                </ul>

                <p class="mb-3">{{ $integration->description }}</p>
            </div>

            <div class="mt-3 mt-lg-0 ml-lg-3 text-center">
                <h3 class="mb-0 font-weight-semibold">{{ $integration->price }} грн.</h3>

                <a type="button" href="/integration/{{ $integration->ref }}" class="btn bg-teal-400 mt-3 legitRipple">Переглянути</a>
            </div>
        </div>
    </div>

    @endforeach
    <!-- /list -->

</div>

@endsection

@section('scripts')

<script src="{{ URL::asset('global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>

<script src="{{ URL::asset('assets/js/content/administration/integrations.js') }}"></script>

@endsection