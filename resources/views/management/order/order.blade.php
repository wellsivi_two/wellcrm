@extends('layouts.template')

@section('title')
{{ $title }}
@endsection

@section('style')

@endsection


@section('content')

<div class="row">
    <div class="col-md-3">
        <div class="card">
           <form id="client_form">
                <div class="card-header bg-white header-elements-sm-inline">
                    <h6 class="card-title">Клієнт</h6>
                    <div class="header-elements">
                        <button class="btn btn-outline alpha-blue text-blue-800 legitRipple" id="clear_client_form">Очистити форму</button>
                    </div>
                </div>
                <div class="card-body">
                    <fieldset class="mb-3">
                        <legend class="text-uppercase font-size-sm font-weight-bold">Базова інформація</legend>
                        
                        <div class="row">
                          
                            <div class="col-md-12">
                                <div class="form-group form-group-float">
									<label class="form-group-float-label is-visible">Ім'я</label>
									<input type="text" name="name" class="form-control" placeholder="Ім'я" value="{{ optional($order->client)->name }}">
								</div>
                            </div>
                           
                            <div class="col-md-12">
                                <div class="form-group form-group-float is-success">
									<label class="form-group-float-label is-visible">Прізвище</label>
									<input type="text" name="surname" class="form-control" placeholder="Прізвище" value="{{ optional($order->client)->surname }}">
								</div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="form-group form-group-float is-success">
									<label class="form-group-float-label is-visible">Стать</label>
                                    <select class="form-control select" name="sex" data-fouc>
                                        <option value="0">Не вибрано</option>
                                        <option value="MAN">Чоловік</option>
                                        <option value="WOMAN">Жінка</option>
									</select>
								</div>
                            </div>
                            
                        </div>
                    </fieldset>
                    <fieldset class="mb-3">
                        <legend class="text-uppercase font-size-sm font-weight-bold">Контактна інформація</legend>
                        
                        <div class="row">
                          
                            <div class="col-md-12">
                                <div class="form-group form-group-float">
									<label class="form-group-float-label is-visible">Телефони</label>
                                    <div class="input-group mb-3">
                                        <span class="input-group-prepend">
                                            <button id="new_phone_button" class="btn btn-light btn-icon legitRipple"><i class="icon-plus3"></i></button>
                                        </span>
                                        <input type="text" name="new_phone" class="form-control" placeholder="Новий телефон">
                                    </div>
                                    <div id="phones_block">
                                        @if (isset($order->client->phones))
                                            @foreach ($order->client->phones as $phone)
                                                <div class="input-group mb-3">
                                                    <span class="input-group-prepend">
                                                        <button class="btn btn-light btn-icon legitRipple delete_phone_button"><i class="icon-cross2"></i></button>
                                                    </span>
                                                    <input type="text" name="phone" class="form-control" readonly placeholder="38 (999) 999-99-99" value="{{ $phone->phone }}">
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
								</div>
                            </div>
                           
                            <div class="col-md-12">
                                <div class="form-group form-group-float is-success">
									<label class="form-group-float-label is-visible">E-mail</label>
                                    <div class="input-group mb-3">
                                        <span class="input-group-prepend">
                                            <button id="new_email_button" class="btn btn-light btn-icon legitRipple"><i class="icon-plus3"></i></button>
                                        </span>
                                        <input type="text" name="new_email" class="form-control" placeholder="Новий email">
                                    </div>
                                    <div id="emails_block">
                                        @if (isset($order->client->emails))
                                            @foreach ($order->client->emails as $email)
                                                <div class="input-group mb-3">
                                                    <span class="input-group-prepend">
                                                        <button class="btn btn-light btn-icon legitRipple delete_email_button" type="button"><i class="icon-cross2"></i></button>
                                                    </span>
                                                    <input type="text" name="email" class="form-control" readonly placeholder="Email" value="{{ $email->email }}">
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
								</div>
                            </div>
                            
                        </div>
                    </fieldset>
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-9">
        <div class="card">
           <form id="order_form">
                <div class="card-header bg-white header-elements-sm-inline">
                    <h6 class="card-title">Замовлення</h6>
                    <div class="header-elements">
                        @can('order_save')
                            <button id="save_order" class="btn btn-outline alpha-success text-success-800 legitRipple">Зберегти</button>
                        @endcan
                        @can('order_delete')
                            <button id="delete_order" class="btn btn-outline alpha-danger text-danger-800 legitRipple ml-3">Видалити</button>
                        @endcan
                    </div>
                </div>
                <div class="card-body">
                    <fieldset class="mb-3">
                        <legend class="text-uppercase font-size-sm font-weight-bold">Базова інформація</legend>
                        
                        <div class="row">

                            <div class="col-md-3">
                                <div class="form-group form-group-float">
									<label class="form-group-float-label is-visible">Магазин</label>
									<select class="form-control select" name="store_id" data-fouc>
                                        <option value="0">Не вибрано</option>
                                        @foreach ($stores as $store)
                                            <option value="{{ $store->id }}">{{ $store->name}}</option>
                                        @endforeach
									</select>
								</div>
                            </div>
                          
                            <div class="col-md-3">
                                <div class="form-group form-group-float store_info">
									<label class="form-group-float-label is-visible">Статус</label>
									<select class="form-control select" name="status_id" data-fouc>
                                        <option value="0">Не вибрано</option>
									</select>
								</div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group form-group-float store_info">
									<label class="form-group-float-label is-visible">Доставка</label>
									<select class="form-control select" name="delivery_id" data-fouc>
                                        <option value="0">Не вибрано</option>
                                        @if (isset($status))
                                            @foreach ($store->deliveries as $delivery)
                                                <option value="{{ $delivery->id }}">{{ $delivery->name}}</option>
                                            @endforeach
                                        @endif
									</select>
								</div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group form-group-float store_info">
									<label class="form-group-float-label is-visible">Оплата</label>
									<select class="form-control select" name="payment_id" data-fouc>
                                        <option value="0">Не вибрано</option>
                                        @if (isset($status))
                                            @foreach ($store->payments as $payment)
                                                <option value="{{ $payment->id }}">{{ $payment->name}}</option>
                                            @endforeach
                                        @endif
									</select>
								</div>
                            </div>
                            
                        </div>
                    </fieldset>
                </div>
            </form>
        </div>
        <div class="card" id="product_card" style="display: none;">
            <div class="card-header bg-white header-elements-sm-inline">
                <h6 class="card-title">Товари</h6>
                <div class="header-elements">
                    @can('order_add_product')
                        <button class="btn btn-outline alpha-blue text-blue-800 legitRipple" data-toggle="modal" data-target="#modal_add_product">Добавити товар</button>
                    @endcan
                </div>
            </div>
            <div class="table-responsive">
                <table class="table" id="product_table">
                    <thead>
                        <tr>
                            <th>Дії</th>
                            <th>#</th>
                            <th>Назва</th>
                            <th>Ціна (грн.)</th>
                            <th>Знижка (грн. \ 1шт.)</th>
                            <th>Кількість (шт.)</th>
                            <th>Сума (грн.)</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (isset($order->products))

                            @foreach ($order->products as $product)
                                <tr class="product" id="{{ $product->id }}">
                                    <td><button type="button" class="btn btn-light btn-icon mr-2 delete_product_button"><i class="icon-cross2"></i></button></td>
                                    <td>{{ $product->id }} </td>
                                    <td>{{ $product->name }}</td>
                                    <td><input type="number" readonly name="price" step="0.01" class="form-control" value="{{ $product->pivot->price }}"></td>
                                    <td><input type="number" readonly  name="discount" step="0.01" class="form-control" value="{{ $product->pivot->discount }}"></td>
                                    <td><input type="number" name="amount" class="form-control" value="{{ $product->pivot->amount }}"></td>
                                    <td><input type="number" readonly name="sum" step="0.01" class="form-control" value="{{ $product->pivot->sum }}"></td>
                                </tr>
                            @endforeach

                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="3"></th>
                            <th>Знижка :</th>
                            <th>
                                <input type="number" step="0.01" name="discount" onkeyup="countTotalOrder()" class="form-control" placeholder="Знижка" value="{{ optional($order)->discount }}">
                            </th>
                            <th>Сума :</th>
                            <th>
                                <input type="number" step="0.01" name="total" class="form-control" placeholder="Сума" value="{{ optional($order)->total }}">
                            </th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8" id="delivery_block">
                
            </div>
        </div>
    </div>
</div>

<div id="modal_add_product" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Добавлення товару</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

                <div class="form-group form-group-float">
                    <label class="form-group-float-label is-visible">Товар</label>
                    <input type="text" name="search_product" class="form-control" placeholder="ID, Артикул, Назва">
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Закрити</button>
                <button type="button" class="btn bg-primary" id="add_product_button">Добавити</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ URL::asset('global_assets/js/plugins/forms/validation/validate.min.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/notifications/pnotify.min.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/forms/inputs/inputmask.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/pickers/pickadate/picker.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/pickers/pickadate/picker.date.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/extensions/jquery_ui/widgets.min.js') }}"></script>


<script>
    var order = {!! json_encode($order) !!},
        stores = {!! json_encode($stores) !!},
        available_statuses = {!! json_encode($available_statuses) !!}
</script>

<script src="{{ URL::asset('assets/js/content/management/order.js') }}"></script>

@endsection