@extends('layouts.template')

@section('title')
{{ $title }}
@endsection

@section('style')

@endsection


@section('content')

<div class="row">
    <div class="col-md-6">

        <div class="card">
           <form id="client_form">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Клієнт</h5>
            </div>
            <div class="card-body">
                    <fieldset class="mb-3">
                        <legend class="text-uppercase font-size-sm font-weight-bold">Базова інформація</legend>
                        
                        <div class="row">
                          
                            <div class="col-6">
                                <div class="form-group form-group-float">
									<label class="form-group-float-label is-visible">Ім'я</label>
									<input type="text" name="name" class="form-control" placeholder="Ім'я" value="{{ optional($client)->name }}">
								</div>
                            </div>
                           
                            <div class="col-6">
                                <div class="form-group form-group-float is-success">
									<label class="form-group-float-label is-visible">Прізвище</label>
									<input type="text" name="surname" class="form-control" placeholder="Прізвище" value="{{ optional($client)->surname }}">
								</div>
                            </div>
                            
                            <div class="col-6">
                                <div class="form-group form-group-float is-success">
									<label class="form-group-float-label is-visible">Стать</label>
                                    <select class="form-control select" name="sex" data-fouc>
                                        <option value="MAN">Чоловік</option>
                                        <option value="WOMAN">Жінка</option>
									</select>
								</div>
                            </div>
                            
                        </div>
                    </fieldset>
                    
                    <fieldset class="mb-3">
                        <legend class="text-uppercase font-size-sm font-weight-bold">Контактна інформація</legend>
                        
                        <div class="row">
                          
                            <div class="col-6">
                                <div class="form-group form-group-float">
									<label class="form-group-float-label is-visible">Телефони</label>
                                    <div id="phones_block">
                                        @if (isset($client->phones))
                                            @foreach ($client->phones as $phone)
                                                <div class="input-group mb-3">
                                                    <span class="input-group-prepend">
                                                        <button class="btn btn-light btn-icon legitRipple delete_phone_button"><i class="icon-cross2"></i></button>
                                                    </span>
                                                    <input type="text" name="phone" class="form-control" readonly placeholder="38 (999) 999-99-99" value="{{ $phone->phone }}">
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
									<div class="input-group mb-3">
                                        <span class="input-group-prepend">
                                            <button id="new_phone_button" class="btn btn-light btn-icon legitRipple"><i class="icon-plus3"></i></button>
                                        </span>
                                        <input type="text" name="new_phone" class="form-control" placeholder="Новий телефон">
                                    </div>
								</div>
                            </div>
                           
                            <div class="col-6">
                                <div class="form-group form-group-float is-success">
									<label class="form-group-float-label is-visible">E-mail</label>
                                    <div id="emails_block">
                                        @if (isset($client->emails))
                                            @foreach ($client->emails as $email)
                                                <div class="input-group mb-3">
                                                    <span class="input-group-prepend">
                                                        <button class="btn btn-light btn-icon legitRipple delete_email_button" type="button"><i class="icon-cross2"></i></button>
                                                    </span>
                                                    <input type="text" name="email" class="form-control" readonly placeholder="Email" value="{{ $email->email }}">
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
									<div class="input-group mb-3">
                                        <span class="input-group-prepend">
                                            <button id="new_email_button" class="btn btn-light btn-icon legitRipple"><i class="icon-plus3"></i></button>
                                        </span>
                                        <input type="text" name="new_email" class="form-control" placeholder="Новий email">
                                    </div>
								</div>
                            </div>
                            
                        </div>
                    </fieldset>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('footer')

    @if (Auth::user()->can('client_save') or Auth::user()->can('client_delete'))

        <div class="navbar navbar-expand-lg navbar-light fixed-bottom">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Панель управління
                </button>
            </div>

            <div class="navbar-collapse collapse p-1" id="navbar-footer">
                <ul class="navbar-nav ml-lg-auto">
                    @can('client_save')
                    <li class="nav-item">
                        <button id="save_client" client_id="{{ $client->id }}" class="btn btn-outline alpha-success text-success-800 legitRipple"><i class="icon-floppy-disk mr-2"></i>Зберегти</button>
                    </li>
                    @endcan
                    @can('client_delete')
                    <li class="nav-item">
                        <button id="delete_client" client_id="{{ $client->id }}" class="btn btn-outline alpha-danger text-danger-800 legitRipple"><i class="icon-bin mr-2"></i>Видали</button>
                    </li>
                    @endcan
                </ul>
            </div>
        </div>

    @endif

@endsection

@section('scripts')
<script src="{{ URL::asset('global_assets/js/plugins/forms/validation/validate.min.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/notifications/pnotify.min.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/forms/inputs/inputmask.js') }}"></script>


<script>
    var client = {!! json_encode($client) !!};
</script>

<script src="{{ URL::asset('assets/js/content/management/client.js') }}"></script>

@endsection