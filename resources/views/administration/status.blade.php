@extends('layouts.template')

@section('title')
{{ $title }}
@endsection

@section('style')

@endsection


@section('content')

<div class="row">
    <div class="col-md-4">

        <div class="card">
           <form id="status_form">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Статус</h5>
                <div class="header-elements">
                    @can('status_save')
                        <button id="save_status" class="btn btn-outline alpha-success text-success-800 legitRipple">Зберегти</button>
                    @endcan
                    @can('status_delete')
                        <button id="delete_status" class="btn btn-outline alpha-danger text-danger-800 legitRipple ml-3">Видалити</button>
                    @endcan
                </div>
            </div>
            <div class="card-body">
                    <fieldset class="mb-3">
                        <legend class="text-uppercase font-size-sm font-weight-bold">Базова інформація</legend>
                        
                        <div class="row">
                           
                            <div class="col-12">
                                <div class="form-group form-group-float is-success">
									<label class="form-group-float-label is-visible">Назва</label>
									<input type="text" class="form-control" name="name" placeholder="Назва" value="{{ optional($status)->name }}">
								</div>
                            </div>

                            <div class="col-12">
                                <div class="form-group form-group-float is-success">
									<label class="form-group-float-label is-visible">Сутність</label>
                                    <select class="form-control select" name="essence_id" data-fouc>
                                        @foreach ($essences as $essence)
                                            <option value="{{ $essence->id }}">{{ $essence->name }}</option>
                                        @endforeach
									</select>
								</div>
                            </div>
                            
                        </div>
                    </fieldset>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ URL::asset('global_assets/js/plugins/notifications/pnotify.min.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>

<script>

    var status_data = {!! json_encode($status) !!};

</script>

<script src="{{ URL::asset('assets/js/content/administration/status.js') }}"></script>

@endsection