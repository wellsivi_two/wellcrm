@extends('layouts.template')

@section('title')
{{ $title }}
@endsection

@section('style')

@endsection


@section('content')

<div class="row">
    <div class="col-md-6">

        <div class="card">
           <form id="user_form">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Користувачі</h5>
            </div>
            <div class="card-body">
                    <fieldset class="mb-3">
                        <legend class="text-uppercase font-size-sm font-weight-bold">Базова інформація</legend>
                        
                        <div class="row">
                           
                            <div class="col-6">
                                <div class="form-group form-group-float is-success">
									<label class="form-group-float-label is-visible">Ім'я</label>
									<input type="text" class="form-control" name="name" placeholder="Ім'я" value="{{ optional($user)->name }}">
								</div>
                            </div>
                            
                            <div class="col-6">
                                <div class="form-group form-group-float">
									<label class="form-group-float-label is-visible">Прізвище</label>
									<input type="text" class="form-control" name="surname" placeholder="Прізвище" value="{{ optional($user)->surname }}">
								</div>
                            </div>
                            
                            <div class="col-6">
                                <div class="form-group form-group-float">
									<label class="form-group-float-label is-visible">Пошта</label>
									<input type="text" class="form-control" name="email" placeholder="Пошта" value="{{ optional($user)->email }}">
								</div>
                            </div>
                            
                            <div class="col-6">
                                <div class="form-group form-group-float">
									<label class="form-group-float-label is-visible">Номер телефону</label>
									<input type="text" class="form-control" name="phone" placeholder="Номер телефону" value="{{ optional($user)->phone }}">
								</div>
                            </div>
                            
                            <div class="col-6">
                                <div class="form-group form-group-float">
									<label class="form-group-float-label is-visible">Пароль</label>
									<input type="password" name="password" class="form-control" placeholder="Пароль">
								</div>
                            </div>
                            
                            <div class="col-6">
                                <div class="form-group form-group-float">
									<label class="form-group-float-label is-visible">Повторіть пароль</label>
									<input type="password" name="re_password" class="form-control" placeholder="Повторіть пароль">
								</div>
                            </div>
                            
                        </div>
                    </fieldset>
                    
                    @can('user_view_roles')
                    <fieldset class="mb-3">
                        <legend class="text-uppercase font-size-sm font-weight-bold">Доступ</legend>
                        <div class="row">
                           
                            <div class="col-12">
                                <div class="form-group form-group-float">
									<label class="form-group-float-label is-visible">Ролі</label>
									<select class="form-control multiselect-select-all-filtering" disabled name="roles" multiple="multiple" data-fouc>
										@foreach ($roles as $role)
                                            @if (isset($user->roles) and $user->roles->contains($role))
                                                <option value="{{ $role->id }}" selected="selected">{{ $role->name }}</option>
                                            @else
                                                <option value="{{ $role->id }}">{{ $role->name }}</option>
                                            @endif
                                        @endforeach
									</select>
								</div>
                            </div>
                            
                        </div>
                    </fieldset>
                    @endcan
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('footer')

    @if (Auth::user()->can('user_save') or Auth::user()->can('user_delete'))

        <div class="navbar navbar-expand-lg navbar-light fixed-bottom">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Панель управління
                </button>
            </div>

            <div class="navbar-collapse collapse p-1" id="navbar-footer">
                <ul class="navbar-nav ml-lg-auto">
                    @can('user_save')
                    <li class="nav-item">
                        <button type="submit" id="save_user" user_id="{{ $user->id }}" form="user_form" class="btn btn-outline alpha-success text-success-800 legitRipple"><i class="icon-floppy-disk mr-2"></i>Зберегти</button>
                    </li>
                    @endcan
                    @can('user_delete')
                    <li class="nav-item">
                        <button id="delete_user" user_id="{{ $user->id }}" class="btn btn-outline alpha-danger text-danger-800 legitRipple"><i class="icon-bin mr-2"></i>Видали</button>
                    </li>
                    @endcan
                </ul>
            </div>
        </div>

    @endif

@endsection

@section('scripts')

<script src="{{ URL::asset('global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/forms/inputs/inputmask.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/forms/validation/validate.min.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/notifications/pnotify.min.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/media/cropper.min.js') }}"></script>

<script src="{{ URL::asset('assets/js/content/administration/user.js') }}"></script>

@can('user_edit_roles')
<script type="application/javascript">
    $('select[name="roles"]').prop('disabled', false);
</script>
@endcan

@endsection