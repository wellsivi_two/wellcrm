@extends('layouts.template')

@section('title')
{{ $title }}
@endsection

@section('style')

@endsection


@section('content')

<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header header-elements-inline">
                <h6 class="card-title">Оплати</h6>
            </div>

            <table class="table datatable-responsive-row-control" id="payments_table">
                <thead>
                    <tr>
                        <tr>
                            <th>ID</th>
                            <th>Назва</th>
                        </tr>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>

    </div>
</div>
@endsection

@section('table_navbar')
    @if (Auth::user()->can('payment_save'))
        <div class="sidebar sidebar-light sidebar-secondary sidebar-expand-md align-self-start">
            <div class="sidebar-mobile-toggler text-center">
                <a href="#" class="sidebar-mobile-secondary-toggle">
                    <i class="icon-arrow-left8"></i>
                </a>
                <span class="font-weight-semibold">Панель керування</span>
                <a href="#" class="sidebar-mobile-expand">
                    <i class="icon-screen-full"></i>
                    <i class="icon-screen-normal"></i>
                </a>
            </div>
            <div class="sidebar-content">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h6 class="card-title">Панель керування</h6>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>

                    <div class="card-body p-0">
                        <ul class="nav nav-sidebar" data-nav-type="accordion">
                            <li class="nav-item-header pt-0 mt-0">Ініціатива</li>
                            @can('store_save')
                            <li class="nav-item">
                                <a href="/payment/0" class="nav-link"><i class="icon-googleplus5"></i> Створити оплату</a>
                            </li>
                            @endcan
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection


@section('scripts')

<script src="{{ URL::asset('global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>

<script src="{{ URL::asset('assets/js/content/administration/payments.js') }}"></script>

@endsection