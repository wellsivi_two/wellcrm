@extends('layouts.template')

@section('title')
{{ $title }}
@endsection

@section('style')

@endsection


@section('content')

<div class="row">
    <div class="col-md-4">

        <div class="card">
           <form id="payment_form">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Оплата</h5>
                <div class="header-elements">
                    @can('payment_save')
                        <button id="save_payment" class="btn btn-outline alpha-success text-success-800 legitRipple">Зберегти</button>
                    @endcan
                    @can('payment_delete')
                        <button id="delete_payment" class="btn btn-outline alpha-danger text-danger-800 legitRipple ml-3">Видалити</button>
                    @endcan
                </div>
            </div>
            <div class="card-body">
                    <fieldset class="mb-3">
                        <legend class="text-uppercase font-size-sm font-weight-bold">Базова інформація</legend>
                        
                        <div class="row">
                           
                            <div class="col-12">
                                <div class="form-group form-group-float is-success">
									<label class="form-group-float-label is-visible">Назва</label>
									<input type="text" class="form-control" name="name" placeholder="Назва" value="{{ optional($payment)->name }}">
								</div>
                            </div>
                            
                        </div>
                    </fieldset>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ URL::asset('global_assets/js/plugins/notifications/pnotify.min.js') }}"></script>

<script>

    var payment = {!! json_encode($payment) !!};

</script>

<script src="{{ URL::asset('assets/js/content/administration/payment.js') }}"></script>

@endsection