@extends('layouts.template')

@section('title')
{{ $title }}
@endsection

@section('style')

@endsection


@section('content')

<div class="row">
    <div class="col-md-6">
        <div class="card">
           <form id="role_form">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Роль</h5>
                </div>
                <div class="card-body">
                    <fieldset class="mb-3">
                        <legend class="text-uppercase font-size-sm font-weight-bold">Базова інформація</legend>
                        
                        <div class="row">
                           
                            <div class="col-sm-6">
                                <div class="form-group form-group-float is-success">
									<label class="form-group-float-label is-visible">Назва <span class="text-danger">*</span></label>
									<input type="text" class="form-control" name="name" placeholder="Назва" value="{{ optional($role)->name }}">
								</div>
                            </div>
                            
                            <div class="col-sm-6">
                                <div class="form-group form-group-float">
									<label class="form-group-float-label is-visible">Керуючий <span class="text-danger">*</span></label>
									<select class="form-control select-search" name="responsible" data-fouc>
                                        @foreach ($users as $user)
                                            @if (isset($role->users) and $role->users->contains($user))

                                                @if ($user->id == $role->responsible)
                                                    <option selected value="{{ $user->id }}">{{ $user->name }} {{ $user->surname }}</option>
                                                @else
                                                    <option value="{{ $user->id }}">{{ $user->name }} {{ $user->surname }}</option>
                                                @endif

                                            @endif
                                        @endforeach
									</select>
									<span class="form-text text-muted font-size-xs">Вибір можливий тільки серед учасників ролі</span>
								</div>
                            </div>
                            
                        </div>
                    </fieldset>
                    
                </div>
            </form>
        </div>
        
        @can('role_view_members')
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Учасники</h5>
                @can('role_edit_members')
                <div class="header-elements">
                    <div class="form-group-feedback form-group-feedback-left">
                        <input type="search" class="form-control wmin-300" placeholder="Пошук користувача для добавлення" id="user_search">
                        <div class="form-control-feedback">
                            <i class="icon-search4 font-size-base text-muted"></i>
                        </div>
                    </div>
                </div>
                @endcan
            </div>
            
            <table class="table" id="users">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Користувач</th>
                        <th>Дії</th>
                    </tr>
                </thead>
                <tbody>
                   @foreach ($users as $user)
                        @if (isset($role->users) and $role->users->contains($user))
                            <tr id="{{ $user->id }}">
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }} {{ $user->surname }}</td>
                                <td>
                                    @can('role_edit_members')
                                    <div class="list-icons">
                                        <button class="btn list-icons-item delete_user"><i class="icon-cross2"></i></button>
                                    </div>
                                    @endcan
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
        @endcan
    </div>
    <div class="col-md-6">
        @can('role_view_permissions')
        <div class="card">
           <form id="role_permissions">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Доступ</h5>
                </div>
                <div class="card-body">
                    <fieldset class="mb-3">
                        <legend class="text-uppercase font-size-sm font-weight-bold">Користувачі</legend>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if($permission->element == 'user') 
                                   <div class="col-sm-6">
                                        <div class="form-group form-group-float is-success">
                                           
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    
                                                     @if (isset($role->permissions) and $role->permissions->contains($permission))
                                                        <input disabled type="checkbox" id="{{ $permission->element . '_' .  $permission->action }}" name="{{ $permission->id }}" class="form-check-input-styled" checked data-fouc>
                                                    @else
                                                        <input disabled type="checkbox" id="{{ $permission->element . '_' .  $permission->action }}" name="{{ $permission->id }}" class="form-check-input-styled" data-fouc>
                                                    @endif
                                                    
                                                    {{ $permission->name }}
                                                </label>
                                            </div>   
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </fieldset>
                    <fieldset class="mb-3">
                        <legend class="text-uppercase font-size-sm font-weight-bold">Ролі</legend>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if($permission->element == 'role') 
                                   <div class="col-sm-6">
                                        <div class="form-group form-group-float is-success">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    
                                                     @if (isset($role->permissions) and $role->permissions->contains($permission))
                                                        <input disabled type="checkbox" id="{{ $permission->element . '_' .  $permission->action }}" name="{{ $permission->id }}" class="form-check-input-styled" checked data-fouc>
                                                    @else
                                                        <input disabled type="checkbox" id="{{ $permission->element . '_' .  $permission->action }}" name="{{ $permission->id }}" class="form-check-input-styled" data-fouc>
                                                    @endif
                                                    
                                                    {{ $permission->name }}
                                                </label>
                                            </div>   
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </fieldset>
                    <fieldset class="mb-3">
                        <legend class="text-uppercase font-size-sm font-weight-bold">Продукти</legend>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if($permission->element == 'product') 
                                   <div class="col-sm-6">
                                        <div class="form-group form-group-float is-success">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    
                                                     @if (isset($role->permissions) and $role->permissions->contains($permission))
                                                        <input disabled type="checkbox" id="{{ $permission->element . '_' .  $permission->action }}" name="{{ $permission->id }}" class="form-check-input-styled" checked data-fouc>
                                                    @else
                                                        <input disabled type="checkbox" id="{{ $permission->element . '_' .  $permission->action }}" name="{{ $permission->id }}" class="form-check-input-styled" data-fouc>
                                                    @endif
                                                    
                                                    {{ $permission->name }}
                                                </label>
                                            </div>   
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </fieldset>
                    <fieldset class="mb-3">
                        <legend class="text-uppercase font-size-sm font-weight-bold">Клієнти</legend>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if($permission->element == 'client') 
                                   <div class="col-sm-6">
                                        <div class="form-group form-group-float is-success">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    
                                                     @if (isset($role->permissions) and $role->permissions->contains($permission))
                                                        <input disabled type="checkbox" id="{{ $permission->element . '_' .  $permission->action }}" name="{{ $permission->id }}" class="form-check-input-styled" checked data-fouc>
                                                    @else
                                                        <input disabled type="checkbox" id="{{ $permission->element . '_' .  $permission->action }}" name="{{ $permission->id }}" class="form-check-input-styled" data-fouc>
                                                    @endif
                                                    
                                                    {{ $permission->name }}
                                                </label>
                                            </div>   
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </fieldset>
                    <fieldset class="mb-3">
                        <legend class="text-uppercase font-size-sm font-weight-bold">Замовлення</legend>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if($permission->element == 'order') 
                                   <div class="col-sm-6">
                                        <div class="form-group form-group-float is-success">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    
                                                     @if (isset($role->permissions) and $role->permissions->contains($permission))
                                                        <input disabled type="checkbox" id="{{ $permission->element . '_' .  $permission->action }}" name="{{ $permission->id }}" class="form-check-input-styled" checked data-fouc>
                                                    @else
                                                        <input disabled type="checkbox" id="{{ $permission->element . '_' .  $permission->action }}" name="{{ $permission->id }}" class="form-check-input-styled" data-fouc>
                                                    @endif
                                                    
                                                    {{ $permission->name }}
                                                </label>
                                            </div>   
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </fieldset>
                    <fieldset class="mb-3">
                        <legend class="text-uppercase font-size-sm font-weight-bold">Магазини</legend>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if($permission->element == 'store') 
                                   <div class="col-sm-6">
                                        <div class="form-group form-group-float is-success">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    
                                                     @if (isset($role->permissions) and $role->permissions->contains($permission))
                                                        <input disabled type="checkbox" id="{{ $permission->element . '_' .  $permission->action }}" name="{{ $permission->id }}" class="form-check-input-styled" checked data-fouc>
                                                    @else
                                                        <input disabled type="checkbox" id="{{ $permission->element . '_' .  $permission->action }}" name="{{ $permission->id }}" class="form-check-input-styled" data-fouc>
                                                    @endif
                                                    
                                                    {{ $permission->name }}
                                                </label>
                                            </div>   
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </fieldset>
                    <fieldset class="mb-3">
                        <legend class="text-uppercase font-size-sm font-weight-bold">Статуси</legend>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if($permission->element == 'status') 
                                   <div class="col-sm-6">
                                        <div class="form-group form-group-float is-success">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    
                                                     @if (isset($role->permissions) and $role->permissions->contains($permission))
                                                        <input disabled type="checkbox" id="{{ $permission->element . '_' .  $permission->action }}" name="{{ $permission->id }}" class="form-check-input-styled" checked data-fouc>
                                                    @else
                                                        <input disabled type="checkbox" id="{{ $permission->element . '_' .  $permission->action }}" name="{{ $permission->id }}" class="form-check-input-styled" data-fouc>
                                                    @endif
                                                    
                                                    {{ $permission->name }}
                                                </label>
                                            </div>   
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </fieldset>
                    <fieldset class="mb-3">
                        <legend class="text-uppercase font-size-sm font-weight-bold">Доставки</legend>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if($permission->element == 'delivery') 
                                   <div class="col-sm-6">
                                        <div class="form-group form-group-float is-success">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    
                                                     @if (isset($role->permissions) and $role->permissions->contains($permission))
                                                        <input disabled type="checkbox" id="{{ $permission->element . '_' .  $permission->action }}" name="{{ $permission->id }}" class="form-check-input-styled" checked data-fouc>
                                                    @else
                                                        <input disabled type="checkbox" id="{{ $permission->element . '_' .  $permission->action }}" name="{{ $permission->id }}" class="form-check-input-styled" data-fouc>
                                                    @endif
                                                    
                                                    {{ $permission->name }}
                                                </label>
                                            </div>   
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </fieldset>
                    <fieldset class="mb-3">
                        <legend class="text-uppercase font-size-sm font-weight-bold">Оплати</legend>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if($permission->element == 'payment') 
                                   <div class="col-sm-6">
                                        <div class="form-group form-group-float is-success">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    
                                                     @if (isset($role->permissions) and $role->permissions->contains($permission))
                                                        <input disabled type="checkbox" id="{{ $permission->element . '_' .  $permission->action }}" name="{{ $permission->id }}" class="form-check-input-styled" checked data-fouc>
                                                    @else
                                                        <input disabled type="checkbox" id="{{ $permission->element . '_' .  $permission->action }}" name="{{ $permission->id }}" class="form-check-input-styled" data-fouc>
                                                    @endif
                                                    
                                                    {{ $permission->name }}
                                                </label>
                                            </div>   
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </fieldset>
                </div>
            </form>
        </div>
        @endcan
    </div>	
</div>

<style type="text/css">
    
    .selectable-demo-list{
        min-height: 100%;
    }
    
</style>

@endsection

@section('footer')

    @if (Auth::user()->can('role_save') or Auth::user()->can('role_delete'))

        <div class="navbar navbar-expand-lg navbar-light fixed-bottom">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Панель управління
                </button>
            </div>

            <div class="navbar-collapse collapse p-1" id="navbar-footer">
                <ul class="navbar-nav ml-lg-auto">
                    @can('role_save')
                    <li class="nav-item">
                        <button type="submit" id="save_role" role_id="{{ $role->id }}" form="role_form" class="btn btn-outline alpha-success text-success-800 legitRipple"><i class="icon-floppy-disk mr-2"></i>Зберегти</button>
                    </li>
                    @endcan
                    @can('role_delete')
                    <li class="nav-item">
                        <button id="delete_role" role_id="{{ $role->id }}" class="btn btn-outline alpha-danger text-danger-800 legitRipple"><i class="icon-bin mr-2"></i>Видали</button>
                    </li>
                    @endcan
                </ul>
            </div>
        </div>

    @endif

@endsection

@section('scripts')

<script src="{{ URL::asset('global_assets/js/plugins/forms/validation/validate.min.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/notifications/pnotify.min.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/extensions/jquery_ui/widgets.min.js') }}"></script>

<script src="{{ URL::asset('assets/js/content/administration/role.js') }}"></script>


@can('role_edit_permissions')
<script type="application/javascript">
    $('#role_permissions input[type="checkbox"]').prop('disabled', false).uniform('refresh');
</script>
@endcan

@endsection