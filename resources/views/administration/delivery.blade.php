@extends('layouts.template')

@section('title')
{{ $title }}
@endsection

@section('style')

@endsection


@section('content')

<div class="row">
    <div class="col-md-4">

        <div class="card">
           <form id="delivery_form">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Доставка</h5>
                <div class="header-elements">
                    @can('delivery_save')
                        <button id="save_delivery" class="btn btn-outline alpha-success text-success-800 legitRipple">Зберегти</button>
                    @endcan
                    @can('delivery_delete')
                        <button id="delete_delivery" class="btn btn-outline alpha-danger text-danger-800 legitRipple ml-3">Видалити</button>
                    @endcan
                </div>
            </div>
            <div class="card-body">
                    <fieldset class="mb-3">
                        <legend class="text-uppercase font-size-sm font-weight-bold">Базова інформація</legend>
                        
                        <div class="row">
                           
                            <div class="col-12">
                                <div class="form-group form-group-float is-success">
									<label class="form-group-float-label is-visible">Назва</label>
									<input type="text" class="form-control" name="name" placeholder="Назва" value="{{ optional($delivery)->name }}">
								</div>
                            </div>

                            <div class="col-12">
                                <div class="form-group form-group-float is-success">
									<label class="form-group-float-label is-visible">Інтеграція</label>
									<select class="form-control select" name="integration_id" data-fouc>
                                        <option value="0">Не вибрано</option>
                                        @foreach ($integrations as $integration)
                                            <option value="{{ $integration->id }}">{{ $integration->description }}</option>
                                        @endforeach
                                    </select>
								</div>
                            </div>
                            
                        </div>
                    </fieldset>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ URL::asset('global_assets/js/plugins/notifications/pnotify.min.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>

<script>

    var delivery = {!! json_encode($delivery) !!};

</script>

<script src="{{ URL::asset('assets/js/content/administration/delivery.js') }}"></script>

@endsection