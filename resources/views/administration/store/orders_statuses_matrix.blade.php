<div class="card">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">Налаштування статусів замовлення</h5>
    </div>
    <table class="table datatable-responsive-row-control" id="orders_statuses_table">
        <thead>
            <tr>
                <tr>
                    <th>Статус</th>
                    <th>Початковий</th>
                    <th>Статуси перехода</th>
                </tr>
            </tr>
        </thead>
        <tbody>
            @foreach ($statuses as $status)
                <tr>
                    <td>{{ $status->name }}</td>
                    <td>

                        @if ($status->start_status == true)
                            <input type="radio" name="start_order_status_id" value="{{ $status->id }}" class="form-input-styled" data-fouc checked>
                        @else
                            <input type="radio" name="start_order_status_id" value="{{ $status->id }}" class="form-input-styled" data-fouc>
                        @endif

                    </td>
                    <td>
                        <select class="form-control multiselect-select-all-filtering status_matrix" name="{{ $status->id }}" multiple="multiple" data-fouc>
                            @foreach ($statuses as $status_matrix)
                                @if (in_array($status_matrix->id, $status->selected))
                                    <option value="{{ $status_matrix->id }}" selected="selected">{{ $status_matrix->name }}</option>
                                @else
                                    <option value="{{ $status_matrix->id }}">{{ $status_matrix->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

<script src="{{ URL::asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>