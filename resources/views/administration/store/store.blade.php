@extends('layouts.template')

@section('title')
{{ $title }}
@endsection

@section('style')

@endsection


@section('content')

<div class="row">
    <div class="col-md-12">

        <div class="card">
           <form id="store_form">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Магазин</h5>
                <div class="header-elements">
                    @can('store_save')
                        <button id="save_store" class="btn btn-outline alpha-success text-success-800 legitRipple">Зберегти</button>
                    @endcan
                    @can('store_delete')
                        <button id="delete_store" class="btn btn-outline alpha-danger text-danger-800 legitRipple ml-3">Видалити</button>
                    @endcan
                </div>
            </div>
            <div class="card-body">
                    <fieldset class="mb-3">
                        <legend class="text-uppercase font-size-sm font-weight-bold">Базова інформація</legend>
                        
                        <div class="row">
                           
                            <div class="col-3">
                                <div class="form-group form-group-float is-success">
									<label class="form-group-float-label is-visible">Назва</label>
									<input type="text" class="form-control" name="name" placeholder="Назва" value="{{ optional($store)->name }}">
								</div>
                            </div>

                            <div class="col-3">
                                <div class="form-group form-group-float">
									<label class="form-group-float-label is-visible">Статуси</label>
									<select class="form-control multiselect-select-all-filtering" name="statuses" multiple="multiple" data-fouc>
										@foreach ($statuses as $status)
                                            @if (isset($store->statuses) and $store->statuses->contains($status))
                                                <option value="{{ $status->id }}" selected="selected">{{ $status->name }}</option>
                                            @else
                                                <option value="{{ $status->id }}">{{ $status->name }}</option>
                                            @endif
                                        @endforeach
									</select>
								</div>
                            </div>

                            <div class="col-3">
                                <div class="form-group form-group-float">
									<label class="form-group-float-label is-visible">Доставки</label>
									<select class="form-control multiselect-select-all-filtering" name="deliveries" multiple="multiple" data-fouc>
										@foreach ($deliveries as $delivery)
                                            @if (isset($store->deliveries) and $store->deliveries->contains($delivery))
                                                <option value="{{ $delivery->id }}" selected="selected">{{ $delivery->name }}</option>
                                            @else
                                                <option value="{{ $delivery->id }}">{{ $delivery->name }}</option>
                                            @endif
                                        @endforeach
									</select>
								</div>
                            </div>

                            <div class="col-3">
                                <div class="form-group form-group-float">
									<label class="form-group-float-label is-visible">Оплати</label>
									<select class="form-control multiselect-select-all-filtering" name="payments" multiple="multiple" data-fouc>
										@foreach ($payments as $payment)
                                            @if (isset($store->payments) and $store->payments->contains($payment))
                                                <option value="{{ $payment->id }}" selected="selected">{{ $payment->name }}</option>
                                            @else
                                                <option value="{{ $payment->id }}">{{ $payment->name }}</option>
                                            @endif
                                        @endforeach
									</select>
								</div>
                            </div>
                            
                        </div>
                    </fieldset>
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-6" id="orders_statuses_matrix">
        
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ URL::asset('global_assets/js/plugins/notifications/pnotify.min.js') }}"></script>
<script src="{{ URL::asset('global_assets/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>

<script>
    var store = {!! json_encode($store) !!};
</script>

<script src="{{ URL::asset('assets/js/content/administration/store.js') }}"></script>

@endsection