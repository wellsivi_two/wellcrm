<!-- Main navbar -->
	<div class="navbar navbar-expand-md navbar-dark bg-indigo">
		<div class="navbar-brand wmin-0 mr-5">
			<a href="index.html" class="d-inline-block">
				<img src="../../../../global_assets/images/logo_light.png" alt="">
			</a>
		</div>

		<div class="d-md-none">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
				<i class="icon-tree5"></i>
			</button>
			<button class="navbar-toggler sidebar-mobile-secondary-toggle" type="button">
				<i class="icon-more"></i>
			</button>
		</div>

		<div class="collapse navbar-collapse" id="navbar-mobile">

			<ul class="navbar-nav ml-md-3 ml-md-auto">
				<li class="nav-item dropdown dropdown-user">
					<a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
						<span>{{ Auth::user()->name . ' ' .  Auth::user()->surname}}</span>
					</a>

					<div class="dropdown-menu dropdown-menu-right">
						<a id="logout" class="dropdown-item"><i class="icon-switch2"></i> Вихід</a>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->