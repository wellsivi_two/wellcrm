<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>@yield('title')</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('global_assets/css/icons/icomoon/styles.min.css') }}" rel="stylesheet" type="text/css">
    

    <link href="{{ URL::asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('assets/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('assets/css/layout.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('assets/css/components.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ URL::asset('assets/css/colors.min.css') }}" rel="stylesheet" type="text/css">
	
	<link href="{{ URL::asset('assets/css/style.css') }}" rel="stylesheet" type="text/css">

	@yield('styles')
</head>

<body>

	@yield('body')
	
	<!-- Core JS files -->
	<script src="{{ URL::asset('global_assets/js/main/jquery.min.js') }}"></script>
	<script src="{{ URL::asset('global_assets/js/main/bootstrap.bundle.min.js') }}"></script>
	<script src="{{ URL::asset('global_assets/js/plugins/loaders/blockui.min.js') }}"></script>
	<script src="{{ URL::asset('global_assets/js/plugins/ui/slinky.min.js') }}"></script>
	<script src="{{ URL::asset('global_assets/js/plugins/ui/ripple.min.js') }}"></script>

	<!-- Theme JS files -->
	<script src="{{ URL::asset('global_assets/js/plugins/visualization/d3/d3.min.js') }}"></script>
	<script src="{{ URL::asset('global_assets/js/plugins/visualization/d3/d3_tooltip.js') }}"></script>
	<script src="{{ URL::asset('global_assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
	<script src="{{ URL::asset('global_assets/js/plugins/ui/moment/moment.min.js') }}"></script>
	<script src="{{ URL::asset('global_assets/js/plugins/visualization/d3/d3.min.js') }}"></script>
	<script src="{{ URL::asset('global_assets/js/plugins/pickers/daterangepicker.js') }}"></script>
	<script src="{{ URL::asset('assets/js/app.js') }}"></script>

	<script src="{{ URL::asset('assets/js/main.js') }}"></script>

	@yield('scripts')

</body>
</html>
