<div class="card" id="novaposhta_card">
    <div class="card-header bg-white header-elements-sm-inline">
        <h6 class="card-title">Нова Пошта</h6>
        <div class="header-elements">

            @if (isset($novaposhta_ttn) and $novaposhta_ttn->created === 0)
                <button id="create_ttn" novaposhta_id="{{ optional($novaposhta_ttn)->id }}" class="btn btn-outline alpha-success text-success-800 legitRipple">Створити ТТН</button>
            @else
                <button id="delete_ttn" novaposhta_id="{{ optional($novaposhta_ttn)->id }}" class="btn btn-outline alpha-danger text-danger-800 legitRipple">Видалити ТТН</button>
            @endif

        </div>
    </div>

    <div class="card-body">

        <form id="novaposhta_form">

            @if (isset($novaposhta_ttn) and $novaposhta_ttn->created === 1)

                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">Дані посилки</legend>
                    
                    <div class="row">
                        
                        <div class="col-4">
                            <div class="form-group form-group-float">
                                <label class="form-group-float-label is-visible">Номер ТТН</label>
                                <input type="text" name="number" readonly class="form-control" placeholder="Опис відправлення" value="{{ optional($novaposhta_ttn)->number }}">
                            </div>
                        </div>

                        <div class="col-4">
                            <div class="form-group form-group-float">
                                <label class="form-group-float-label is-visible">Статус</label>
                                <input type="text" name="status_name" readonly class="form-control" placeholder="Опис відправлення" value="{{ optional($novaposhta_ttn)->status_name }}">
                            </div>
                        </div>

                        <div class="col-4">
                            <div class="form-group form-group-float">
                                <label class="form-group-float-label is-visible">Дата останнього оновлення</label>
                                <input type="text" name="updated_at" readonly class="form-control" placeholder="Опис відправлення" value="{{ optional($novaposhta_ttn)->updated_at }}">
                            </div>
                        </div>
                        
                    </div>
                </fieldset>

            @endif

            <fieldset class="mb-3">
                <legend class="text-uppercase font-size-sm font-weight-bold">Параметри відправлення</legend>

                <div class="row">
                    <div class="col-6">
                        <div class="form-group form-group-float">
                            <label class="form-group-float-label is-visible">Опис відправлення</label>
                            <input type="text" name="delivery_description" Ref="{{ optional($novaposhta_ttn)->delivery_description_Ref }}" class="form-control" placeholder="Опис відправлення" value="{{ optional($novaposhta_ttn)->delivery_description }}">
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group form-group-float">
                            <label class="form-group-float-label is-visible">Оголошена вартість</label>
                            <input type="text" name="cost" class="form-control" placeholder="Оголошена вартість" value="{{ optional($novaposhta_ttn)->cost }}">
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group form-group-float">
                            <label class="form-group-float-label is-visible">Вага (кг.)</label>
                            <input type="number" name="weight" class="form-control" placeholder="Вага (кг.)" value="{{ optional($novaposhta_ttn)->weight }}">
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group form-group-float">
                            <label class="form-group-float-label is-visible">Довжина (см.)</label>
                            <input type="number" name="length" class="form-control" placeholder="Довжина (см.)" value="{{ optional($novaposhta_ttn)->length }}">
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group form-group-float">
                            <label class="form-group-float-label is-visible">Ширина (см.)</label>
                            <input type="number" name="width" class="form-control" placeholder="Ширина (см.)" value="{{ optional($novaposhta_ttn)->width }}">
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group form-group-float">
                            <label class="form-group-float-label is-visible">Висота (см.)</label>
                            <input type="number" name="height" class="form-control" placeholder="Висота (см.)" value="{{ optional($novaposhta_ttn)->height }}">
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group form-group-float">
                            <label class="form-group-float-label is-visible">Об’ємна вага (кг.)</label>
                            <input type="text" name="volumetric_weight" readonly="" class="form-control" placeholder="Висота" value="{{ optional($novaposhta_ttn)->volumetric_weight }}">
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="form-group form-group-float">
                            <label class="form-group-float-label is-visible">Тип доставки</label>
                            <select class="form-control select" name="delivery_service" data-fouc>
                                <option value="0">Не вибрано</option>

                                @if (isset($novaposhta_services))
                                    @foreach ($novaposhta_services as $novaposhta_service)
                                        @if (isset($novaposhta_ttn->delivery_service) and $novaposhta_ttn->delivery_service == $novaposhta_service->Ref)
                                            <option value="{{ $novaposhta_service->Ref }}" selected>{{ $novaposhta_service->Description }}</option>
                                        @else
                                            <option value="{{ $novaposhta_service->Ref }}">{{ $novaposhta_service->Description }}</option>
                                        @endif
                                    @endforeach
                                @endif

                            </select>
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="form-group form-group-float">
                            <label class="form-group-float-label is-visible">Тип вантажу</label>
                            <select class="form-control select" name="delivery_cargo" data-fouc>
                                <option value="0">Не вибрано</option>

                                @if (isset($novaposhta_delivery_cargos))
                                    @foreach ($novaposhta_delivery_cargos as $novaposhta_delivery_cargo)
                                        @if (isset($novaposhta_ttn->delivery_cargo) and $novaposhta_ttn->delivery_cargo == $novaposhta_delivery_cargo->Ref)
                                            <option value="{{ $novaposhta_delivery_cargo->Ref }}" selected>{{ $novaposhta_delivery_cargo->Description }}</option>
                                        @else
                                            <option value="{{ $novaposhta_delivery_cargo->Ref }}">{{ $novaposhta_delivery_cargo->Description }}</option>
                                        @endif
                                    @endforeach
                                @endif

                            </select>
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="form-group form-group-float">
                            <label class="form-group-float-label is-visible">Платник за доставку</label>
                            <select class="form-control select" name="delivery_payer" data-fouc>
                                <option value="0">Не вибрано</option>

                                @if (isset($novaposhta_delivery_payers))
                                    @foreach ($novaposhta_delivery_payers as $novaposhta_delivery_payer)
                                        @if (isset($novaposhta_ttn->delivery_payer) and $novaposhta_ttn->delivery_payer == $novaposhta_delivery_payer->Ref)
                                            <option value="{{ $novaposhta_delivery_payer->Ref }}" selected>{{ $novaposhta_delivery_payer->Description }}</option>
                                        @else
                                            <option value="{{ $novaposhta_delivery_payer->Ref }}">{{ $novaposhta_delivery_payer->Description }}</option>
                                        @endif
                                    @endforeach
                                @endif

                            </select>
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="form-group form-group-float">
                            <label class="form-group-float-label is-visible">Форма оплати за доставку</label>
                            <select class="form-control select" name="delivery_payment" data-fouc>
                                <option value="0">Не вибрано</option>

                                @if (isset($novaposhta_payments))
                                    @foreach ($novaposhta_payments as $novaposhta_payment)
                                        @if (isset($novaposhta_ttn->delivery_payment) and $novaposhta_ttn->delivery_payment == $novaposhta_payment->Ref)
                                            <option value="{{ $novaposhta_payment->Ref }}" selected>{{ $novaposhta_payment->Description }}</option>
                                        @else
                                            <option value="{{ $novaposhta_payment->Ref }}">{{ $novaposhta_payment->Description }}</option>
                                        @endif
                                    @endforeach
                                @endif

                            </select>
                        </div>
                    </div>

                </div>
            </fieldset>
            
            <fieldset class="mb-3">
                <legend class="text-uppercase font-size-sm font-weight-bold">Відправник</legend>
                
                <div class="row">
                    
                    <div class="col-12">
                        <div class="form-group form-group-float is-success">
                            <label class="form-group-float-label is-visible">Контрагент</label>
                            <select class="form-control select" name="novaposhta_account_id" data-fouc>
                                @if (isset($novaposhta_accounts))
                                    @foreach ($novaposhta_accounts as $novaposhta_account)
                                        @if (isset($novaposhta_ttn->novaposhta_account_id) and $novaposhta_ttn->novaposhta_account_id == $novaposhta_account->id)
                                            <option value="{{ $novaposhta_account->id }}" selected>{{ $novaposhta_account->Description . ' (' .  $novaposhta_account->Phones . ')' }}</option>
                                        @else
                                            <option value="{{ $novaposhta_account->id }}">{{ $novaposhta_account->Description . ' (' .  $novaposhta_account->Phones . ')' }}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    
                </div>
            </fieldset>

            <fieldset class="mb-3">
                <legend class="text-uppercase font-size-sm font-weight-bold">Одержувач</legend>
                
                <div class="row">
                    
                    <div class="col-3">
                        <div class="form-group form-group-float is-success">
                            <label class="form-group-float-label is-visible">Телефон</label>
                            <input type="text" name="phone" class="form-control" placeholder="Телефон" value="{{ optional($novaposhta_ttn)->phone }}">
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="form-group form-group-float is-success">
                            <label class="form-group-float-label is-visible">Прізвище</label>
                            <input type="text" name="last_name" class="form-control" placeholder="Прізвище" value="{{ optional($novaposhta_ttn)->last_name }}">
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="form-group form-group-float is-success">
                            <label class="form-group-float-label is-visible">Ім'я</label>
                            <input type="text" name="first_name" class="form-control" placeholder="Ім'я" value="{{ optional($novaposhta_ttn)->first_name }}">
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="form-group form-group-float is-success">
                            <label class="form-group-float-label is-visible">По батькові</label>
                            <input type="text" name="middle_name" class="form-control" placeholder="По батькові" value="{{ optional($novaposhta_ttn)->middle_name }}">
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group form-group-float is-success">
                            <label class="form-group-float-label is-visible">Область</label>
                            <input type="text" name="area" Ref="{{ optional($novaposhta_ttn)->area_Ref }}" class="form-control" placeholder="Область" value="{{ optional($novaposhta_ttn)->area }}">
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group form-group-float is-success">
                            <label class="form-group-float-label is-visible">Місто</label>
                            <input type="text" name="city" Ref="{{ optional($novaposhta_ttn)->city_Ref }}" class="form-control" placeholder="Місто" value="{{ optional($novaposhta_ttn)->city }}">
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group form-group-float is-success">
                            <label class="form-group-float-label is-visible">Відділення</label>
                            <input type="text" name="warehouse" Ref="{{ optional($novaposhta_ttn)->city_Ref }}" class="form-control" placeholder="Відділення" value="{{ optional($novaposhta_ttn)->warehouse }}">
                        </div>
                    </div>
                    
                </div>
            </fieldset>

            <fieldset class="mb-3">
                <legend class="text-uppercase font-size-sm font-weight-bold">Додаткові послуги</legend>
                
                <div class="row">

                    <div class="col-4">
                        <div class="form-group form-group-float">
                            <label class="form-group-float-label is-visible">Зворотня доставка</label>
                            <select class="form-control select" name="redelivery_cargo" data-fouc>
                                <option value="0">Не вибрано</option>

                                @if (isset($novaposhta_redelivery_cargos))
                                    @foreach ($novaposhta_redelivery_cargos as $novaposhta_redelivery_cargo)
                                        @if (isset($novaposhta_ttn->redelivery_cargo) and $novaposhta_ttn->redelivery_cargo == $novaposhta_redelivery_cargo->Ref)
                                            <option value="{{ $novaposhta_redelivery_cargo->Ref }}" selected>{{ $novaposhta_redelivery_cargo->Description }}</option>
                                        @else
                                            <option value="{{ $novaposhta_redelivery_cargo->Ref }}">{{ $novaposhta_redelivery_cargo->Description }}</option>
                                        @endif
                                    @endforeach
                                @endif

                            </select>
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group form-group-float">
                            <label class="form-group-float-label is-visible">Додаткове поле</label>
                            <input type="text" name="redelivery_additional_string" disabled class="form-control" placeholder="Сума або опис" value="{{ optional($novaposhta_ttn)->redelivery_additional_string }}">
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group form-group-float">
                            <label class="form-group-float-label is-visible">Платник</label>
                            <select class="form-control select" disabled name="redelivery_payer" data-fouc>
                                <option value="0">Не вибрано</option>

                                @if (isset($novaposhta_redelivery_payers))
                                    @foreach ($novaposhta_redelivery_payers as $novaposhta_redelivery_payer)
                                        @if (isset($novaposhta_ttn->redelivery_payer) and $novaposhta_ttn->redelivery_payer == $novaposhta_redelivery_payer->Ref)
                                            <option value="{{ $novaposhta_redelivery_payer->Ref }}" selected>{{ $novaposhta_redelivery_payer->Description }}</option>
                                        @else
                                            <option value="{{ $novaposhta_redelivery_payer->Ref }}">{{ $novaposhta_redelivery_payer->Description }}</option>
                                        @endif
                                    @endforeach
                                @endif

                            </select>
                        </div>
                    </div>
                    
                </div>
            </fieldset>

        </form>

    </div>
</div>

<script src="{{ URL::asset('global_assets/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
<script src="{{ URL::asset('assets/js/content/layouts/novaposhta_form.js') }}"></script>