<!-- Secondary navbar -->
	<div class="navbar navbar-expand-md navbar-light">
		<div class="text-center d-md-none w-100">
			<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-navigation">
				<i class="icon-unfold mr-2"></i>
				Навігація
			</button>
		</div>

		<div class="navbar-collapse collapse" id="navbar-navigation">
			<ul class="navbar-nav navbar-nav-highlight">
				<li class="nav-item">
					<a href="/" class="navbar-nav-link active">
						<i class="icon-home2 mr-2"></i>
						Головна
					</a>
				</li>
				
				@if (Auth::user()->can('user_view') 
				or 
					Auth::user()->can('role_view') 
				or 
					Auth::user()->can('store_view') 
				or 
					Auth::user()->can('status_view') 
				or 
					Auth::user()->can('delivery_view')
				or 
					Auth::user()->can('payment_view')
				)
				
				    <li class="nav-item dropdown">
					    <a href="#" class="navbar-nav-link dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="false">
                            <i class="icon-briefcase mr-2"></i>
                            Адміністрування
                        </a>

                        <div class="dropdown-menu">
                            @can('user_view')
                                <a href="/users" class="dropdown-item">Користувачі</a>
                            @endcan
                            @can('role_view')
                                <a href="/roles" class="dropdown-item">Ролі</a>
                            @endcan
							@can('store_view')
                                <a href="/stores" class="dropdown-item">Магазини</a>
                            @endcan
							@can('status_view')
                                <a href="/statuses" class="dropdown-item">Статуси</a>
                            @endcan
							@can('delivery_view')
                                <a href="/deliveries" class="dropdown-item">Доставки</a>
                            @endcan
							@can('payment_view')
                                <a href="/payments" class="dropdown-item">Оплати</a>
                            @endcan
                        </div>
                    </li>
				
				@endif
				
				@if (Auth::user()->can('product_view'))
				
				<li class="nav-item dropdown">
					<a href="#" class="navbar-nav-link dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="false">
						<i class="icon-box mr-2"></i>
						Склад
					</a>

					<div class="dropdown-menu">
						@can('product_view')
							<a href="/products" class="dropdown-item">Товари</a>
						@endcan
					</div>
				</li>
				
				@endif
				
				@if (Auth::user()->can('client_view') or Auth::user()->can('order_view'))
				
				<li class="nav-item dropdown">
					<a href="#" class="navbar-nav-link dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="false">
						<i class="icon-box mr-2"></i>
						Менеджмент
					</a>

					<div class="dropdown-menu">
						@can('client_view')
							<a href="/clients" class="dropdown-item">Клієнти</a>
						@endcan
						@can('order_view')
							<a href="/orders" class="dropdown-item">Замовлення</a>
						@endcan
					</div>
				</li>
				
				@endif
				
			</ul>

			<ul class="navbar-nav navbar-nav-highlight ml-md-auto">
				@if (Auth::user()->can('integration_view'))
				
					<li class="nav-item dropdown">
						<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
							<i class="icon-store mr-2"></i>
							Магазин
						</a>

						<div class="dropdown-menu dropdown-menu-right">
							@can('integration_view')
								<a href="/integrations" class="dropdown-item">Інтеграції</a>
							@endcan
						</div>
					</li>
				
				@endif
			</ul>
			
		</div>
	</div>
	<!-- /secondary navbar -->