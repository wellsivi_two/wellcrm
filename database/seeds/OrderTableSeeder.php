<?php

use Illuminate\Database\Seeder;

use App\Order;
use App\OrderProduct;

class OrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Order::truncate();

        Order::create([
            'client_id' => '1',
            'store_id' => '1',
            'delivery_id' => '1',
            'payment_id' => '1',
            'status_id' => '1',
            'discount' => 10,
            'total' => 100,
        ]);

        OrderProduct::truncate();

        OrderProduct::create([
            'order_id' => '1',
            'product_id' => '1',
            'price' => '100',
            'discount' => '0',
            'amount' => '1',
            'sum' => '100'
        ]);
    }
}
