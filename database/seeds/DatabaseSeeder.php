<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        
        Eloquent::unguard();

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->call(PermissionTableSeeder::class);

        $this->call(RoleTableSeeder::class);

        $this->call(UserTableSeeder::class);
        
        $this->call(ProductSeeder::class);

        $this->call(ClientSeeder::class);

        $this->call(ClientPhoneSeeder::class);

        $this->call(ClientEmailSeeder::class);

        $this->call(StoreTableSeeder::class);

        $this->call(EssenceTableSeeder::class);

        $this->call(StatusTableSeeder::class);

        $this->call(DeliveryTableSeeder::class);

        $this->call(PaymentTableSeeder::class);

        $this->call(IntegrationTypesTableSeeder::class);

        $this->call(IntegrationTableSeeder::class);

        $this->call(OrderTableSeeder::class);
        
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }
}
