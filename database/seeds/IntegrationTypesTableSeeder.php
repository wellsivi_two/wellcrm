<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class IntegrationTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::connection('shared_bd')->table('crm_integration_types')->insert([
            [
                'ref' => 'delivery',
                'description' => 'Доставка'
            ]
        ]);

    }
}
