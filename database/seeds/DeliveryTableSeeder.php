<?php

use Illuminate\Database\Seeder;

use App\Delivery;

class DeliveryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        Delivery::truncate();

        Delivery::create([
            'name' => 'Самовивіз з Києва'
        ]);

        Delivery::create([
            'name' => 'НоваПошта',
            'integration_id' => 1
        ]);

    }
}
