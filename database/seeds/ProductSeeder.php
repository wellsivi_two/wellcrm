<?php

use Illuminate\Database\Seeder;

use App\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::truncate();

        Product::create([
            'name' => 'Гель для волосся',
            'price' => '250',
            'cost' => '100',
            'store_id' => 1,
            'article' => 'AT13425'
        ]);

        Product::create([
            'name' => 'Масло для жіночого волосся',
            'price' => '370',
            'cost' => '100',
            'store_id' => 1,
            'article' => 'AT78678'
        ]);
    }
}
