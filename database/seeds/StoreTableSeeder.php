<?php

use Illuminate\Database\Seeder;

use App\Store;

use App\StoreStatus;
use App\StoreDelivery;
use App\StorePayment;

class StoreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        Store::truncate();

        Store::create([
            'name' => 'Medi',
        ]);

        StoreStatus::truncate();

        StoreStatus::create([
            'store_id' => 1,
            'status_id' => 1
        ]);

        StoreDelivery::truncate();

        StoreDelivery::create([
            'store_id' => 1,
            'delivery_id' => 1
        ]);

        StorePayment::truncate();

        StorePayment::create([
            'store_id' => 1,
            'payment_id' => 1
        ]);

    }
}
