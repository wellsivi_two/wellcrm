<?php

use Illuminate\Database\Seeder;

use App\Payment;

class PaymentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        Payment::truncate();

        Payment::create([
            'name' => 'На карту ПБ'
        ]);

    }
}
