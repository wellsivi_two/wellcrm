<?php

use Illuminate\Database\Seeder;

use App\Essence;

class EssenceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        Essence::truncate();

        Essence::create([
            'name' => 'Заказ'
        ]);

    }
}
