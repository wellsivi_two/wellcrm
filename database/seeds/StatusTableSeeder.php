<?php

use Illuminate\Database\Seeder;

use App\Status;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Status::truncate();

        Status::create([
            'name' => 'Новий',
            'essence_id' => 1
        ]);

        Status::create([
            'name' => 'Обробляється',
            'essence_id' => 1
        ]);

        Status::create([
            'name' => 'Підтверджений',
            'essence_id' => 1
        ]);

        Status::create([
            'name' => 'Готовиться до відгрузки',
            'essence_id' => 1
        ]);
        
        Status::create([
            'name' => 'Відгружений',
            'essence_id' => 1
        ]);

        Status::create([
            'name' => 'Завершений',
            'essence_id' => 1
        ]);

        Status::create([
            'name' => 'Повернений',
            'essence_id' => 1
        ]);

    }
}
