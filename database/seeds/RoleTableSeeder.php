<?php

use Illuminate\Database\Seeder;

use App\Role;
use App\Permission;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::truncate();

        $permissions = Permission::whereIn('element', [
            'user', 'role', 'client', 'product', 'service', 'product', 'order', 'store', 'status', 'delivery', 'payment', 'integration'
        ])->pluck('id')->all();

        $role = Role::create([
            'name' => 'Власник',
            'responsible' => 2
        ]);

        $role->permissions()->attach($permissions);

        $role = Role::create([
            'name' => 'Менеджер',
            'responsible' => 2
        ]);
    }
}
