<?php

use Illuminate\Database\Seeder;

use App\ClientPhone;

class ClientPhoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ClientPhone::truncate();

        ClientPhone::create([
            'client_id' => '1',
            'phone' => '380673431700',
        ]);

        ClientPhone::create([
            'client_id' => '1',
            'phone' => '380665910945',
        ]);
    }
}
