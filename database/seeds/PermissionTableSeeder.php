<?php

use Illuminate\Database\Seeder;

use App\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::truncate();

        Permission::create([
            'name' => 'Перегляд користувачів',
            'element' => 'user',
            'action' => 'view',
        ]);

        Permission::create([
            'name' => 'Збереження користувачів',
            'element' => 'user',
            'action' => 'save',
        ]);
        
        Permission::create([
            'name' => 'Перегляд ролей',
            'element' => 'user',
            'action' => 'view_roles',
        ]);

        Permission::create([
            'name' => 'Редагування ролей',
            'element' => 'user',
            'action' => 'edit_roles',
        ]);
        
        Permission::create([
            'name' => 'Видалення користувачів',
            'element' => 'user',
            'action' => 'delete',
        ]);
        
        // Role

        Permission::create([
            'name' => 'Перегляд ролей',
            'element' => 'role',
            'action' => 'view',
        ]);

        Permission::create([
            'name' => 'Збереження ролей',
            'element' => 'role',
            'action' => 'save',
        ]);
        
        Permission::create([
            'name' => 'Перегляд учасників',
            'element' => 'role',
            'action' => 'view_members',
        ]);

        Permission::create([
            'name' => 'Редагування учасників',
            'element' => 'role',
            'action' => 'edit_members',
        ]);

        Permission::create([
            'name' => 'Редагування прав',
            'element' => 'role',
            'action' => 'edit_permissions',
        ]);
        
        Permission::create([
            'name' => 'Перегляд прав',
            'element' => 'role',
            'action' => 'view_permissions',
        ]);
        
        Permission::create([
            'name' => 'Видалення ролей',
            'element' => 'role',
            'action' => 'delete',
        ]);
        
        // Client

        Permission::create([
            'name' => 'Перегляд клієнтів',
            'element' => 'client',
            'action' => 'view',
        ]);

        Permission::create([
            'name' => 'Зберігання клієнтів',
            'element' => 'client',
            'action' => 'save',
        ]);

        Permission::create([
            'name' => 'Видалення клієнтів',
            'element' => 'client',
            'action' => 'delete',
        ]);

        // Product

        Permission::create([
            'name' => 'Перегляд продуктів',
            'element' => 'product',
            'action' => 'view',
        ]);

        Permission::create([
            'name' => 'Зберігання продуктів',
            'element' => 'product',
            'action' => 'save',
        ]);

        Permission::create([
            'name' => 'Видалення продуктів',
            'element' => 'product',
            'action' => 'delete',
        ]);

        // Order

        Permission::create([
            'name' => 'Перегляд замовлень',
            'element' => 'order',
            'action' => 'view',
        ]);

        Permission::create([
            'name' => 'Збереження замовлень',
            'element' => 'order',
            'action' => 'save',
        ]);

        Permission::create([
            'name' => 'Видалення замовлень',
            'element' => 'order',
            'action' => 'delete',
        ]);

        Permission::create([
            'name' => 'Добавлення продуктів',
            'element' => 'order',
            'action' => 'add_product',
        ]);

        // Store

        Permission::create([
            'name' => 'Перегляд магазинів',
            'element' => 'store',
            'action' => 'view',
        ]);

        Permission::create([
            'name' => 'Збереження магазинів',
            'element' => 'store',
            'action' => 'save',
        ]);

        Permission::create([
            'name' => 'Видалення магазинів',
            'element' => 'store',
            'action' => 'delete',
        ]);

        // Status

        Permission::create([
            'name' => 'Перегляд статусів',
            'element' => 'status',
            'action' => 'view',
        ]);

        Permission::create([
            'name' => 'Збереження статусів',
            'element' => 'status',
            'action' => 'save',
        ]);

        Permission::create([
            'name' => 'Видалення статусів',
            'element' => 'status',
            'action' => 'delete',
        ]);

        // Delivery

        Permission::create([
            'name' => 'Перегляд доставок',
            'element' => 'delivery',
            'action' => 'view',
        ]);

        Permission::create([
            'name' => 'Збереження доставок',
            'element' => 'delivery',
            'action' => 'save',
        ]);

        Permission::create([
            'name' => 'Видалення доставок',
            'element' => 'delivery',
            'action' => 'delete',
        ]);

        // Payment

        Permission::create([
            'name' => 'Перегляд оплат',
            'element' => 'payment',
            'action' => 'view',
        ]);

        Permission::create([
            'name' => 'Збереження оплат',
            'element' => 'payment',
            'action' => 'save',
        ]);

        Permission::create([
            'name' => 'Видалення оплат',
            'element' => 'payment',
            'action' => 'delete',
        ]);

        // Integration

        Permission::create([
            'name' => 'Перегляд інтеграцій',
            'element' => 'integration',
            'action' => 'view',
        ]);

        Permission::create([
            'name' => 'Дезактивація інтеграцій',
            'element' => 'integration',
            'action' => 'save',
        ]);

        Permission::create([
            'name' => 'Активація інтеграцій',
            'element' => 'integration',
            'action' => 'activation',
        ]);

    }
}
