<?php

use Illuminate\Database\Seeder;

use App\Client;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Client::truncate();

        Client::create([
            'name' => 'Анастасія',
            'surname' => 'Голуб',
            'sex' => 'WOMAN'
        ]);
    }
}
