<?php

use Illuminate\Database\Seeder;

use App\Role;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $role = Role::where('name', 'Власник')->first();

        $user = User::create([
            'name'      =>  'Владислав',
            'surname'   =>  'Залуцький',
            'phone'     =>  '380673431700',
            'email'     =>  'wellsivi@gmail.com',
            'password'  =>  bcrypt('web1234')
        ]);

        $user->roles()->attach($role);

        $user = User::create([
            'name'      =>  'Анастасія',
            'surname'   =>  'Голуб',
            'phone'     =>  '380995910945',
            'email'     =>  'nastya@gmail.com',
            'password'  =>  bcrypt('web1234')
        ]);

        $user->roles()->attach($role);
    }
}
