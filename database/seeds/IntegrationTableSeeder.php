<?php

use Illuminate\Database\Seeder;

use App\Integration;

class IntegrationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        Integration::truncate();

        Integration::create([

            'ref' => 'novaposhta',
            'description' => 'Нова пошта',

            'type_ref' => 'delivery',

            'title' => 'Інтеграція з Новою поштою',
            'price' => 100,
            'active' => 1

        ]);

    }
}
