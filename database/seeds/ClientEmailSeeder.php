<?php

use Illuminate\Database\Seeder;

use App\ClientEmail;

class ClientEmailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ClientEmail::truncate();

        ClientEmail::create([
            'client_id' => '1',
            'email' => 'wellsivi@gmail.com'
        ]);
        
    }
}
