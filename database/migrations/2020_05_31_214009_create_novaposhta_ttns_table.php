<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNovaposhtaTtnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('novaposhta_ttns', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->string('model');
            $table->bigInteger('model_id')->unsigned();

            $table->bigInteger('novaposhta_account_id')->unsigned()->default(0);
            $table->foreign('novaposhta_account_id')->references('id')->on('novaposhta_accounts');

            $table->decimal('cost', 8, 2)->default(0);
            $table->decimal('weight', 8, 2)->default(0);

            $table->decimal('length', 8, 2)->default(0);
            $table->decimal('width', 8, 2)->default(0);
            $table->decimal('height', 8, 2)->default(0);

            $table->decimal('volumetric_weight', 8, 2)->default(0);

            $table->string('last_name', 20)->default('');
            $table->string('first_name', 20)->default('');
            $table->string('middle_name', 20)->default('');
            $table->string('phone', 20)->default('');

            $table->string('delivery_service', 30)->default('');
            $table->string('delivery_payer', 30)->default('');
            $table->string('delivery_payment', 30)->default('');
            $table->string('delivery_cargo', 100)->default('');

            $table->string('delivery_description', 100)->default('');
            $table->string('delivery_description_Ref', 100)->default('');

            $table->string('redelivery_cargo', 100)->default('');
            $table->string('redelivery_additional_string', 100)->default('');
            $table->string('redelivery_payer', 100)->default('');

            $table->string('area', 100)->default('');
            $table->string('area_Ref', 100)->default('');

            $table->string('city', 100)->default('');
            $table->string('city_Ref', 100)->default('');

            $table->string('warehouse', 100)->default('');
            $table->string('warehouse_Ref', 100)->default('');

            $table->bigInteger('created')->default(0);
            
            $table->string('number', 100)->default('');
            $table->string('ref', 100)->default('');

            $table->bigInteger('status_id')->default(0);
            $table->string('status_name')->default('');

            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('novaposhta_ttns');
    }
}
