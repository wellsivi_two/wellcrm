<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNovaposhtaCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('shared_bd')->create('novaposhta_cities', function (Blueprint $table) {

            $table->integer('CityID', false, true)->length(10);
            $table->string('Description', 200);
            $table->string('Ref', 50);
            $table->string('SettlementTypeDescription', 50)->default('невідомо');
            $table->string('SettlementType', 50);
            $table->string('Area', 50);

            $table->tinyInteger('Delivery1', false);
            $table->tinyInteger('Delivery2', false);
            $table->tinyInteger('Delivery3', false);
            $table->tinyInteger('Delivery4', false);
            $table->tinyInteger('Delivery5', false);
            $table->tinyInteger('Delivery6', false);
            $table->tinyInteger('Delivery7', false);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('shared_bd')->dropIfExists('novaposhta_cities');
    }
}
