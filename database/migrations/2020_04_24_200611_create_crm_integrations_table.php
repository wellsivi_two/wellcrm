<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Query\Expression;

class CreateCrmIntegrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::connection('shared_bd')->create('crm_integrations', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->string('ref');
            $table->string('description');

            $table->string('type_ref');

            $table->string('title');
            $table->decimal('price', 8, 2);
            $table->integer('active');

            $table->json('inputs')->default(new Expression('(JSON_ARRAY())'));


            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('shared_bd')->dropIfExists('crm_integrations');
    }
}
