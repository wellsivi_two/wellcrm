<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateOrdersTalbe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {

            $table->bigInteger('store_id')->unsigned()->after('client_id');
            $table->foreign('store_id')->references('id')->on('stores');

            $table->foreign('status_id')->references('id')->on('statuses');

            $table->foreign('delivery_id')->references('id')->on('deliveries');

            $table->foreign('payment_id')->references('id')->on('payments');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {

            $table->dropForeign(['store_id']);
            $table->dropColumn('store_id');

            $table->dropForeign(['status_id']);
            $table->dropColumn('status_id');

            $table->dropForeign(['delivery_id']);
            $table->dropColumn('delivery_id');

            $table->dropForeign(['payment_id']);
            $table->dropColumn('payment_id');

        });
    }
}
