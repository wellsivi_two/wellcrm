<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmIntegrationTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('shared_bd')->create('crm_integration_types', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('description');
            $table->string('ref');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('shared_bd')->dropIfExists('crm_integration_types');
    }
}
