<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Query\Expression;

class UpdateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stores', function (Blueprint $table) {

            $table->json('orders_status_matrix')->after('name')->default(new Expression('(JSON_ARRAY())'));
            $table->bigInteger('start_order_status_id')->after('orders_status_matrix')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stores', function (Blueprint $table) {

            $table->dropColumn('orders_status_matrix');
            $table->dropColumn('start_order_status_id');

        });
    }
}
