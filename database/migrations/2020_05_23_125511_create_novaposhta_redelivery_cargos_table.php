<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNovaposhtaRedeliveryCargosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('shared_bd')->create('novaposhta_redelivery_cargos', function (Blueprint $table) {
            
            $table->string('Description', 250);
            $table->string('Ref', 250);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('shared_bd')->dropIfExists('novaposhta_redelivery_cargos');
    }
}
