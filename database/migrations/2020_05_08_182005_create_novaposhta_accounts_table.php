<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNovaposhtaAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('novaposhta_accounts', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->string('Key');
            $table->string('sender_Ref');
            $table->string('contact_Ref');

            $table->string('Description');
            $table->bigInteger('Phones')->length(12);
            $table->integer('month_limit')->length(11);
            $table->integer('active')->length(1);

            $table->string('city', 100)->default('');
            $table->string('city_Ref', 100)->default('');

            $table->string('warehouse', 100)->default('');
            $table->string('warehouse_Ref', 100)->default('');

            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('novaposhta_accounts');
    }
}
