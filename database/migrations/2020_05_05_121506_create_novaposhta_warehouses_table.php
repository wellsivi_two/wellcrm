<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNovaposhtaWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('shared_bd')->create('novaposhta_warehouses', function (Blueprint $table) {
            
            $table->integer('SiteKey', false, true)->length(10);

            $table->string('Description', 250);
            $table->string('ShortAddress', 250);

            $table->string('TypeOfWarehouse', 250);
            $table->string('Ref', 250);

            $table->integer('Number', false, true)->length(11);

            $table->string('CityRef', 250);
            $table->string('CityDescription', 250);

            $table->string('WarehouseStatus', 250);


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('shared_bd')->dropIfExists('novaposhta_warehouses');
    }
}
